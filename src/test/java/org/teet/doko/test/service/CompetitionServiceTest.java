package org.teet.doko.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.teet.doko.DokoApp;
import org.teet.doko.domain.Community;
import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitioncycle;
import org.teet.doko.domain.Competitionlocation;
import org.teet.doko.domain.Competitionlocationcycle;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.domain.Competitionplayercycle;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.service.CommunityService;
import org.teet.doko.domain.service.CommunityStatisticService;
import org.teet.doko.domain.service.CommunityplayerService;
import org.teet.doko.domain.service.CompetitionService;
import org.teet.doko.domain.service.CompetitioncycleService;
import org.teet.doko.domain.service.CompetitionlocationService;
import org.teet.doko.domain.service.CompetitionlocationcycleService;
import org.teet.doko.domain.service.CompetitionplayerService;
import org.teet.doko.domain.service.CompetitionplayercycleService;
import org.teet.doko.domain.service.PlayerService;
import org.teet.doko.gateway.v1.SimpleCommunity;
import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleLocation;
import org.teet.doko.gateway.v1.SimpleLocationCycleplayer;
import org.teet.doko.gateway.v1.SimpleMember;
import org.teet.doko.gateway.v1.SimplePlayer;
import org.teet.doko.gateway.v1.SimplePlayercoin;
import org.teet.doko.gateway.v1.SimplePlayercoingroup;
import org.teet.doko.util.PojoUtil;

@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.teet.doko.domain")
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {DokoApp.class})
public class CompetitionServiceTest {
	
	@Autowired
	private CompetitionService competitionService;
	
	 @Autowired
	 private CommunityService communityService;
	
	 @Autowired
	 private CommunityStatisticService communityStatisticService;
	 
	 @Autowired
	 private PlayerService playerService;
	 
	 @Autowired
	 private CommunityplayerService communitylayerService;
	 
	 @Autowired
	 private CompetitionlocationService competitionlocationService;
	 
	 @Autowired
	 private CompetitionlocationcycleService competitionlocationcycleService;
	 
	 @Autowired
	 private CompetitionplayerService competitionplayerService;
	 
	 @Autowired
	 private CompetitioncycleService competitioncycleService;
	 
	 @Autowired
	 CompetitionplayercycleService competitionplayercycleService;
	 
	 private long competitionId;
	 private long communityId;
	 
	 @Before
	 public void createCompetition() throws Exception {
		 
		 	
		 	
		 	//Spielort
		 	//
	        
	        
	       
	        
	        
	        
	        
	        
	        
	        
	 }
	 @Test
	 @Transactional(propagation = Propagation.REQUIRES_NEW)
	 public void test() throws Exception {
		 Player u1 =new Player();
		 u1.setUsername("guest");
	        playerService.add(u1);
		 
		 SimpleCommunity c1 = new SimpleCommunity();
	        c1.setTitle("Test");
	        c1.setOwnwer(u1.getUsername());
	        Community cEntity=communityService.add(c1);
	        communityId=cEntity.getId();
	        SimpleCompetition sC=new SimpleCompetition();
	        sC.setTitle("Turnier 1");
	        sC.setCommunityId(cEntity.getId());
	        sC.setDate(new java.util.Date());
	        Competition c=competitionService.add(sC);
	       // Competition c=competitionService.findById(comp1.getId());
	        
	        competitionId=c.getId();
	        
	        //Spieler hinzufügen
	        List<Player> playerList=new ArrayList<Player>();
		 	for(Player p : EntityHelper.getPlayer()) {
		 		playerService.add(p);
		 		playerList.add(p);
		 	}
		 	
		 	Set<Communityplayer> cpList=EntityHelper.getCommunityplayer(playerList, cEntity);
		 	
		 	
		 	Set<SimpleMember> memberset= PojoUtil.toSimpleMember(cpList);
		 	
		 	//Spieler der Community hinzugefügt
		 	for(Communityplayer p : cpList) {
		 		communitylayerService.add(p,null);
		 	}
		 	
		 	//Spieler dem Turnier hinzugefügt
		 	competitionService.addCompetitionplayer(memberset,  c.getId());
		 	
		 SimpleLocation location1=new SimpleLocation();
		 location1.setCompetitionId(competitionId);
		 location1.setName("Tisch 1");
		 
		 SimpleLocation location2=new SimpleLocation();
		 location2.setCompetitionId(competitionId);
		 location2.setName("Tisch 2");
		 	
		 Competitionlocation comp1Location=competitionlocationService.add(location1);
		 assertNotNull(comp1Location);
		 assertNotNull(comp1Location.getCompetition());
		 assertEquals(comp1Location.getCompetition().getId(),competitionId);
		 Competitionlocation comp2Location=competitionlocationService.add(location2);
		 assertNotNull(comp2Location);
		 
		 

		 Set<Competitionplayer> compPlayerList=competitionplayerService.findByCompetitionId(competitionId);

		 System.out.println("Locationid:"+comp2Location.getId());
		 
		 competitionService.findAll();
		 Competitioncycle cycle1= competitionService.addCompetitioncycleForAllLocation(competitionId);
		 
		 Competitioncycle cycle2= competitionService.addCompetitioncycleForAllLocation(competitionId);
		 assertNotNull(cycle2);
		 
		 Set<SimpleLocationCycleplayer> setCyclePlayer1=new HashSet<SimpleLocationCycleplayer>();  
		 
		 System.out.println("Player:"+compPlayerList.size());
		 
		 
		 System.out.println("Search location from Round :"+cycle1.getId());
		 //Aus der Runde die Spielorte auslesen
		 //Ergibt null
		 //Set<SimpleLocationCycle> loccycleSet=competitioncycleService.getCompetitionlocationByRoundId(cycle1.getId());
		 Collection<Competitionlocationcycle> loccycleSet=competitionlocationcycleService.findByCompetitioncycle(cycle1);
		 System.out.println("Locationcycle:"+loccycleSet.size());
		 Competitionlocationcycle loccycle1 =loccycleSet.iterator().next();
		 
		 
		 
		//Einen Spielort nehmn
		 
		 
		 for(Competitionplayer cp :  compPlayerList) {
			 
			 SimpleLocationCycleplayer p1=new SimpleLocationCycleplayer();
			 
			 //p1.setId(cp.getId());
			 p1.setPlayerId(cp.getPlayer().getId());
			 p1.setUsername(cp.getPlayer().getUsername());
			 p1.setCompetitionId(competitionId);
			 setCyclePlayer1.add(p1);
			 
		 };	
		 
		 
		 assertEquals(5,setCyclePlayer1.size());
		 
	     competitionlocationcycleService.addPlayer2Loctioncycle(setCyclePlayer1,loccycle1.getId());
	     
	     Collection<Competitionplayercycle>  cyclePlayerSet=competitionplayercycleService.findByLocationcycle(loccycle1);
	     
	     //System.out.println(cyclePlayerSet.size());
	     assertEquals(5,cyclePlayerSet.size());
	     Competitionplayercycle cycleplayer=cyclePlayerSet.iterator().next();
	     
	     competitionplayercycleService.delete(cycleplayer);
	     
	     
	     //deletePlayerFromLocationcycle
	     Collection<Competitionplayercycle>  cyclePlayerSet2=competitionplayercycleService.findByLocationcycle(loccycle1);
	     assertEquals(4,cyclePlayerSet2.size());
	     /*
	     SimpleMatch tesMatch1=new SimpleMatch();
	     tesMatch1.setCoin(4);
	     tesMatch1.setCycleId(loccycle1.getId());
	     
	     Set<SimplePlayercoin> coins=new HashSet<SimplePlayercoin>();
	     for(Competitionplayercycle plcyc:cyclePlayerSet2) {
	    	 SimplePlayercoin scoin=new SimplePlayercoin();
	    	 scoin.setCoin(2);
	    	 scoin.setUserId(plcyc.getPlayer().getId());
	    	 scoin.setCyclesequence(1);
	    	 coins.add(scoin);
	     }
	     tesMatch1.setPlayercoin(coins);
	     competitionlocationcycleService.addMatchForCycle(tesMatch1,loccycle1.getId());
	     */
	     
	     Collection<SimplePlayer> playerListSearch=communityService.findBySearchtext("Tors");
	     assertEquals(1,playerListSearch.size());
	     
	     Collection<SimplePlayer> playerListSearch2=communityService.findBySearchtext("AA");
	     assertEquals(0,playerListSearch2.size());
	     
	     List<SimplePlayercoin> coinsSum=communityStatisticService.getPlayercoinSumByCompetition(competitionId);
	     
	     System.out.println("TEILNEHMER:"+coinsSum.size());
	     
	     List<SimplePlayercoingroup> coinsSumCycle=communityStatisticService.getPlayercoinSumByCompetitionAndCycle(competitionId);
	     /*	 
	     
	     communityStatisticService.getSimplePlayercoinByCompetitionId(c.getId());
	        
	        communityStatisticService.getSimplePlayercoinByCommunityId(cEntity.getId());
	*/	 
	 }
}
