package org.teet.doko.test.service;





import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.teet.doko.DokoApp;
import org.teet.doko.domain.Community;
import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.service.CommunityService;
import org.teet.doko.domain.service.CommunityplayerService;
import org.teet.doko.domain.service.PlayerService;

@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.teet.doko.domain")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DokoApp.class})

@ActiveProfiles("test")

public class CommunityplayerServiceTest {
	private Community c1;
	private Community c2;
	
	private Player p1;
	private Player p2;
	private Player p3;
	
    @Autowired
    private CommunityplayerService communityplayerService;
    
    @Autowired
    private CommunityService communityService;
    
    @Autowired
    private PlayerService playerService;
    
    @Before
    public void createData() throws Exception{
        // given
         
        
         p1 =new Player();
        p1.setUsername("Player1");
         p2 =new Player();
        p2.setUsername("Player2");
         p3 =new Player();
        p2.setUsername("Player3");
        
        
        playerService.add(p1);
        playerService.add(p2);
        playerService.add(p3);
        
        c1 = new Community();
        c1.setTitle("Community1");
        c1.setOwner(p1);
        
         c2 = new Community();
        c2.setTitle("Community2");
        c2.setOwner(p2);
        communityService.add(c1);
        communityService.add(c2);
        
    
    }
    @Test
    public void createCommunityplayer() throws Exception {
        // when
    	 Communityplayer cp1=new Communityplayer();
    	 cp1.setCommunity(c1);
    	 cp1.setPlayer(p1);
    	 communityplayerService.add(cp1,null);
    	 
    	 List<Community> cR= communityplayerService.findByPlayer(p1);
        assertEquals(cR.size(),1);
        
        // then
      
    }
}
