package org.teet.doko.test.service;





import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.teet.doko.DokoApp;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.service.PlayerService;

@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.teet.doko.domain")
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {DokoApp.class})



public class PlayerServiceTest {
	
	
    @Autowired
    private PlayerService communityService;
    
    @Test
    public void whenFindByName_thenReturnEmployee() throws Exception {
        // given
        Player c1 = new Player();
        c1.setUsername("Test");
        
        communityService.add(c1);
        // when
        Player found = communityService.findByUsername(c1.getUsername());
        
        
        assertEquals(found.getUsername(),c1.getUsername());
        
        // then
      
    }
}
