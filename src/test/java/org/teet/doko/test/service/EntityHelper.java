package org.teet.doko.test.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.teet.doko.domain.Community;
import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.domain.Player;

public class EntityHelper {
	public static  Set<Community> getCommunity(){
		Set<Community> cList=new HashSet<Community>();
			Community c1=new Community();
			c1.setTitle("Gruppe Bremen");
			c1.setDescription("Erste Gruppe");
			
			Community c2=new Community();
			c2.setTitle("Gruppe Hannover");
			c2.setDescription("Zweite Gruppe");
			cList.add(c1);
			cList.add(c2);
		return cList;
	}
	
	public static  List<Player> getPlayer(){
		List<Player> pLs=new ArrayList<Player>();
		Player player1 = new Player();
	 	player1.setFirstname("Torsten");
	 	player1.setLastname("P.");
	 	player1.setUsername("torsten");
	 	pLs.add(player1);
	 	
	 	Player player2 = new Player();
	 	player2.setFirstname("Elke");
	 	player2.setLastname("P.");
	 	player2.setUsername("elke");
	 	
	 	pLs.add(player2);
	 	
	 	Player player3 = new Player();
	 	player3.setFirstname("Till");
	 	player3.setLastname("P.");
	 	player3.setUsername("till");
	 	
	 	pLs.add(player3);
	 	
	 	Player player4 = new Player();
	 	player4.setFirstname("Emil");
	 	player4.setLastname("P.");
	 	player4.setUsername("emil");
	 	
	 	pLs.add(player4);
	 	
	 	Player player5 = new Player();
	 	player5.setFirstname("Micha");
	 	player5.setLastname("M.");
	 	player5.setUsername("micha");
	 	
	 	pLs.add(player5);
		return pLs;
	}
	
	public static Set<Communityplayer> getCommunityplayer(List<Player> playerList, Community community){
		Set<Communityplayer> pList=new HashSet<Communityplayer>();;
		for(Player p : playerList) {
	 		Communityplayer cp=new Communityplayer();
	 		cp.setCommunity(community);
	 		cp.setPlayer(p);
	 		pList.add(cp);
	 		
	 	}
		return pList;
	}
	
	public static List<Competitionplayer> getCompetitionplayer(List<Player> playerList, Competition competition){
		List<Competitionplayer> pList=new ArrayList<Competitionplayer>();
		for(Player p : playerList) {
			Competitionplayer cp=new Competitionplayer();
	 		cp.setCompetition(competition);
	 		cp.setPlayer(p);
	 		pList.add(cp);
	 		
	 	}
		return pList;
	}
}
