package org.teet.doko.test.service;





import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.teet.doko.DokoApp;
import org.teet.doko.domain.Community;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitiongamerule;
import org.teet.doko.domain.Competitionlocation;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.domain.Game;
import org.teet.doko.domain.Gamerule;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.service.CommunityService;
import org.teet.doko.domain.service.GameruleService;
import org.teet.doko.domain.service.PlayerService;
import org.teet.doko.gateway.v1.SimpleCommunity;
import org.teet.doko.gateway.v1.SimpleCommunityInfo;

@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.teet.doko.domain")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DokoApp.class})



public class CommunityServiceTest {
	
	
    @Autowired
    private CommunityService communityService;
    
    @Autowired
    private GameruleService gameruleService;
    
    @Autowired
    private PlayerService playerService;
    
    @Test
    public void createCommunityTest() throws Exception {
        
    	Player p1 =new Player();
        p1.setUsername("user1");
        playerService.add(p1);
    	
    	Game g1 = new Game();
    	//g1.setId(1L);
    	g1.setTitle("Doppelkopf");
    	
    	Gamerule gr1 = new Gamerule();
    	//gr1.setId(1L);
    	gr1.setTitle("Turnier 1");
    	gr1.setGame(g1);
    	
    	gameruleService.add(gr1);
    	
    	
    	
    	
    	// given
        SimpleCommunity sc1 = new SimpleCommunity();
        sc1.setTitle("Test");
        sc1.setOwnwer(p1.getUsername());
        
        communityService.add(sc1);
        
        Community c1=communityService.findByTitle(sc1.getTitle());
        
        Competition cp1=new Competition();
        cp1.setCommunity(c1);
        cp1.setDate(new java.util.Date());
        cp1.setTitle("Testturnier");
        
        Set<Competitiongamerule> cpgrLs= new HashSet<Competitiongamerule>();
        Competitiongamerule cgr1=new Competitiongamerule();
        cgr1.setCompetition(cp1);
        cgr1.setGamerule(gr1);
        cpgrLs.add(cgr1);
        cp1.setGamerule(cpgrLs);
        
        Competitionplayer cpp1=new Competitionplayer();
        cpp1.setCompetition(cp1);
        cpp1.setPlayer(p1);
        
        Set<Competitionplayer> cppLs= new HashSet<Competitionplayer>();
        cppLs.add(cpp1);
        cp1.setPlayer(cppLs);
        
        Set<Competition> cpLs= new HashSet<Competition>();
        cpLs.add(cp1);
        c1.setCompetition(cpLs);
        
        
        Set<Competitionlocation> cplLs= new HashSet<Competitionlocation>();
        Competitionlocation cpl1=new Competitionlocation();
        cpl1.setCompetition(cp1);
        cpl1.setName("Tisch1");
        cplLs.add(cpl1);
        cp1.setLocation(cplLs);
        communityService.add(c1);
        // when
        Community found = communityService.findByTitle(c1.getTitle());
        
        
        //SimpleCommunityInfo scUi= communityService.findSimpleinfoById(c1.getId());
        
        assertEquals(found.getTitle(),c1.getTitle());
        //assertEquals(scUi.getMember().size(),1);
        
      //  assertEquals(found.getCompetition().size(),c1.getCompetition().size());
        
        //assertEquals(found.getCompetition().get(0).getPlayer().size(),c1.getCompetition().get(0).getPlayer().size());
        
        //assertEquals(found.getCompetition().get(0).getLocation().size(),c1.getCompetition().get(0).getLocation().size());
        
        // then
      
    }
}
