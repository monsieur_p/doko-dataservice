package org.teet.doko.resources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.teet.doko.domain.Imagestore;
import org.teet.doko.domain.ImagestoreRepository;
import org.teet.doko.gateway.v1.SimpleImageResponse;

@RestController
@RequestMapping("/image/v1")
public class ImageUploadController {
	@Autowired
	ImagestoreRepository imagestoreRepository;
	
	@PostMapping("/upload")
	public SimpleImageResponse uplaodImage(@RequestParam("imageFile") MultipartFile file)  {
		SimpleImageResponse resp= new SimpleImageResponse();
		try {
	        System.out.println("Original Image Byte Size - " + file.getBytes().length);
	        
	        Imagestore img = new Imagestore(file.getOriginalFilename(), file.getContentType(),
	
	                compressBytes(file.getBytes()));
	
	        imagestoreRepository.save(img);
	        resp.setStatus(200);
	        resp.setId(img.getId());
	        resp.setType(img.getType());
	        resp.setName(img.getName());
	        }catch(IOException e) {
	        	resp.setStatus(500);
	        	resp.setMessage(e.getMessage());
	        	e.printStackTrace();
	        }catch(Exception e) {
	        	resp.setStatus(501);
	        	resp.setMessage(e.getMessage());
	        	e.printStackTrace();
	        }
	        return resp;
	
	 }
	 @GetMapping(path = { "/get/{id}" })
	 
	     public Imagestore getImage(@PathVariable("id") long id) throws IOException {
	 
	         final Optional<Imagestore> retrievedImage = imagestoreRepository.findById(id);
	 
	         Imagestore img = new Imagestore(retrievedImage.get().getName(), retrievedImage.get().getType(),
	 
	                 decompressBytes(retrievedImage.get().getPicByte()));
	 
	         return img;
	 
	   }
	// compress the image bytes before storing it in the database
	    public static byte[] compressBytes(byte[] data) {
	        Deflater deflater = new Deflater();
	        deflater.setInput(data);
	        deflater.finish();
	        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
	        byte[] buffer = new byte[1024];
	        while (!deflater.finished()) {
	            int count = deflater.deflate(buffer);
	            outputStream.write(buffer, 0, count);
	        }
	        try {
	            outputStream.close();
	        } catch (IOException e) {
	        }
	        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
	        return outputStream.toByteArray();
	    }
	    
	 // uncompress the image bytes before returning it to the angular application
	    public static byte[] decompressBytes(byte[] data) {
	            Inflater inflater = new Inflater();
	            inflater.setInput(data);
	            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
	            byte[] buffer = new byte[1024];
	            try {
	                while (!inflater.finished()) {
	                    int count = inflater.inflate(buffer);
	                    outputStream.write(buffer, 0, count);
	                }
	                outputStream.close();
	            } catch (IOException ioe) {
	            } catch (DataFormatException e) {
	            }
	            return outputStream.toByteArray();
	        }
}
