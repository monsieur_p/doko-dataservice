package org.teet.doko.resources;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.teet.doko.domain.Communitygamerule;
import org.teet.doko.domain.Gameitem;
import org.teet.doko.domain.Gameitemcategory;
import org.teet.doko.domain.Gameitemtype;
import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.domain.service.CommunityService;
import org.teet.doko.domain.service.GameService;
import org.teet.doko.domain.service.GameruleService;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGame;
import org.teet.doko.gateway.v1.SimpleGameitem;
import org.teet.doko.gateway.v1.SimpleGameitemcategory;
import org.teet.doko.gateway.v1.SimpleGameitemtype;
import org.teet.doko.gateway.v1.SimpleGamerule;
import org.teet.doko.gateway.v1.SimpleGamerulevariant;
import org.teet.doko.gateway.v1.SimpleStatusResponse;
import org.teet.doko.util.AuthUtil;
import org.teet.doko.util.PojoUtil;

@RestController
@RequestMapping("/rules/v1")
public class RuleController {
	@Autowired
    private CommunityService communityService;
	
	@Autowired 
	private GameruleService gameruleService;
	
	@Autowired 
	private GameService gameService;
	
	@CrossOrigin
	@RequestMapping(value = "/gamerule/{communityId}", method = RequestMethod.PUT)
	public SimpleStatusResponse addGamerul2Community(@PathVariable("communityId") long communityId,@RequestBody SimpleGamerule game, Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			Communitygamerule c=communityService.addRuleForCommunity(game, communityId,AuthUtil.getSimplePlayerFromPrincipal(principal));
			rc.setId(c.getId());
		} catch (Exception e) {
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
			
			//e.printStackTrace();
		}
		return rc;
	}
	
	@CrossOrigin
	@RequestMapping(value = "gamerule/{ruleId}/gamevariant", method = RequestMethod.PUT)
	public SimpleStatusResponse addGamevariant2Gamerule(@PathVariable("ruleId") long ruleId,@RequestBody SimpleGamerulevariant variant, Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			Gamerulevariant c=gameruleService.addGamevariant2Gamerule(variant, ruleId,AuthUtil.getSimplePlayerFromPrincipal(principal));
			rc.setId(c.getId());
		} catch (Exception e) {
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
			
			//e.printStackTrace();
		}
		return rc;
	}
	
	@CrossOrigin
	@RequestMapping(value = "gamerule/{ruleId}", method = RequestMethod.GET)
	public SimpleGamerule getGameruleById(@PathVariable("ruleId") long ruleId) {
		
		
			SimpleGamerule sc;
			try {
				sc = communityService.getSimpleGameruleByCommunitygameruleId(ruleId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				sc=new SimpleGamerule();
			}
		
		return sc;
	}
	
	@CrossOrigin
	@RequestMapping(value = "game", method = RequestMethod.GET)
	public List<SimpleGame> getGame() {
		
		return gameService.getAllAsSimpleGame();
			
	}
	
	@CrossOrigin
	@RequestMapping(value = "game/{gameId}/item", method = RequestMethod.PUT)
	public SimpleStatusResponse addGameitem(@PathVariable("gameId") long gameId,@RequestBody SimpleGameitem gameitem, Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			
			Gameitem gi=gameService.addSimpleGameitem2Game(gameitem, gameId,AuthUtil.getSimplePlayerFromPrincipal(principal));
			rc.setId(gi.getId());
		} catch (StatusException e) {
			rc.setStatus(e.getErrorCode());
			rc.setMessage(e.getMessage());
		}
		return rc;
	}
	
	
	
	@CrossOrigin
	@RequestMapping(value = "game/{gameId}/itemcategory", method = RequestMethod.PUT)
	public SimpleStatusResponse addGameitemcategory(@PathVariable("gameId") long gameId,@RequestBody SimpleGameitemcategory category, Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			
			Gameitemcategory gi=gameService.addSimpleGameitemcategory2Game(category, gameId,AuthUtil.getSimplePlayerFromPrincipal(principal));
			rc.setId(gi.getId());
		} catch (StatusException e) {
			rc.setStatus(e.getErrorCode());
			rc.setMessage(e.getMessage());
		}
		return rc;
	}
	
	@CrossOrigin
	@RequestMapping(value = "game/{gameId}/itemtype", method = RequestMethod.PUT)
	public SimpleStatusResponse addGameitemtype(@PathVariable("gameId") long gameId,@RequestBody SimpleGameitemtype gameitemtype, Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			
			Gameitemtype gi=gameService.addSimpleGameitemtype2Game(gameitemtype, gameId,AuthUtil.getSimplePlayerFromPrincipal(principal));
			rc.setId(gi.getId());
		} catch (StatusException e) {
			rc.setStatus(e.getErrorCode());
			rc.setMessage(e.getMessage());
		}
		return rc;
	}
	
	@CrossOrigin
	@RequestMapping(value = "game/{gameId}/item/{itemId}", method = RequestMethod.DELETE)
	public SimpleStatusResponse deleteGameitem(@PathVariable("gameId") long gameId,@PathVariable("itemId") long itemId, Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			
			gameService.deleteGameitem(itemId, gameId,AuthUtil.getSimplePlayerFromPrincipal(principal));
			
		} catch (StatusException e) {
			rc.setStatus(e.getErrorCode());
			rc.setMessage(e.getMessage());
		}
		return rc;
	}
	
	@CrossOrigin
	@RequestMapping(value = "game/{gameId}/rules", method = RequestMethod.GET)
	public List<SimpleGamerule> getGamerulesByGame(@PathVariable("gameId") long gameId) {
		
		try {
			return PojoUtil.toSimpleGamerule(gameruleService.findByGame(gameService.findById(gameId)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ArrayList<SimpleGamerule>();
		}
			
	}
	
	@CrossOrigin
	@RequestMapping(value = "game/{gameId}/items/reorder", method = RequestMethod.POST)
	public SimpleStatusResponse reorderGameitems(@PathVariable("gameId") long gameId,@RequestBody ArrayList<SimpleGameitem> gameitems, Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			
			gameService.reorderItemsBySimpleItem(gameitems, gameId,AuthUtil.getSimplePlayerFromPrincipal(principal));
			
		} catch (StatusException e) {
			rc.setStatus(e.getErrorCode());
			rc.setMessage(e.getMessage());
		}
		return rc;
	}
	
	
	
	
}
