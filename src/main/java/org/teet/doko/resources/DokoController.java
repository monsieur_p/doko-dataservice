package org.teet.doko.resources;

import java.security.Principal;
import java.util.Collection;
import java.util.Set;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.teet.doko.Version;
import org.teet.doko.domain.Community;
import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitioncycle;
import org.teet.doko.domain.Competitionlocation;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.service.CommunityService;
import org.teet.doko.domain.service.CommunityplayerService;
import org.teet.doko.domain.service.CompetitionService;
import org.teet.doko.domain.service.CompetitioncycleService;
import org.teet.doko.domain.service.CompetitionlocationService;
import org.teet.doko.domain.service.PlayerService;
import org.teet.doko.domain.service.ValidationService;
import org.teet.doko.exception.CommunityplayerAddException;
import org.teet.doko.gateway.v1.SimpleCommunity;
import org.teet.doko.gateway.v1.SimpleCommunityInfo;
import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleCompetitionInfo;
import org.teet.doko.gateway.v1.SimpleLocation;
import org.teet.doko.gateway.v1.SimpleLocationCycle;
import org.teet.doko.gateway.v1.SimpleMember;
import org.teet.doko.gateway.v1.SimplePlayer;
import org.teet.doko.gateway.v1.SimpleStatusResponse;
import org.teet.doko.util.AuthUtil;
import org.teet.doko.util.PojoUtil;

@RestController
@RequestMapping("/doko/v1")
public class DokoController {
	

	
	@Autowired
    private CommunityService communityService;
	
	@Autowired
    private CompetitionService competitionService;
	
	@Autowired
    private CompetitionlocationService competitionlocationService;
	
	@Autowired
    private CommunityplayerService communityplayerService;
	
	@Autowired
    private CompetitioncycleService competitioncycleService;
	
	@Autowired
    private PlayerService playerService;
	
	@Autowired
    private ValidationService validationService;
	
	@RequestMapping("/version")
	public Version getVersion() {
		return new Version();
	}
	
	/**
	 * Einer Community anhander der Anmeldedaten von Keycloakdaten beitreten
	 * @param communityId
	 * @param principal
	 * @return
	 */
	@CrossOrigin
	@RequestMapping(value = "/community/{communityId}/accede", method = RequestMethod.GET)
	public SimpleStatusResponse accede2Community(@PathVariable("communityId") long communityId, Principal principal) {
		//KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
		SimpleStatusResponse response=new SimpleStatusResponse();
		if (principal instanceof KeycloakPrincipal) {

		    
		      
		    SimplePlayer player=AuthUtil.getSimplePlayerFromPrincipal(principal);
			if(player!=null) {
				
				try {
					Communityplayer pl=validationService.addSimplePlayer2Community(player, communityId);
					response.setStatus(200);
					response.setId(pl.getId());
				}catch(CommunityplayerAddException e) {
					e.printStackTrace();
					response.setStatus(e.getErrorCode());
					response.setMessage(e.getMessage());
				}
			}
		}
		return response;
	}
	
	/**
	 * Gibt die Benutzerdaten des Spielers zurück
	 * @param principal
	 * @return
	 */
	@CrossOrigin
	@RequestMapping(value = "/userinfo", method = RequestMethod.GET)
	public SimplePlayer getPlayerinfo(Principal principal) {
		
		SimplePlayer player=new SimplePlayer();
		if (principal instanceof KeycloakPrincipal) {

		    
		      
		    player=AuthUtil.getSimplePlayerFromPrincipal(principal);;
			if(player!=null) {
				
				try {
					Player pl=playerService.findByUserid(player.getUserid());
					if(pl!=null) {
						player=PojoUtil.toSimplePlayer(pl);
					}else {
						player=new SimplePlayer();
					}
				}catch(Exception e) {
					e.printStackTrace();
					
				}
			}
		}
		return player;
	}
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/community", method = RequestMethod.PUT)
	public SimpleStatusResponse addCommunity(@RequestBody SimpleCommunity simpleCommunity,  Principal principal) {
		
	    
		SimplePlayer player=AuthUtil.getSimplePlayerFromPrincipal(principal);
		
		
		simpleCommunity.setOwnwer(player.getUserid());
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			Community c=communityService.add(simpleCommunity);
			if(player!=null) {
				
				try {
					player.setActive(true);
					validationService.addSimplePlayer2Community(player, c.getId());
				}catch(CommunityplayerAddException e) {
					e.printStackTrace();
				}
			}
			rc.setId(c.getId());
		} catch (Exception e) {
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
			
			e.printStackTrace();
		}
		return rc;
	}
	@CrossOrigin
	@RequestMapping(value = "/communities", method = RequestMethod.GET)
	public Iterable<Community> getCommunity() {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			return communityService.findAll();
			
		} catch (Exception e) {
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
			
			//e.printStackTrace();
		}
		return null;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/community/{communityId}/competition", method = RequestMethod.GET)
	public Iterable<Competition> getCompetitionByCommunity(@PathVariable("communityId") long communityId) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			return competitionService.findByCommunityId(communityId);
			
		} catch (Exception e) {
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
			
			//e.printStackTrace();
		}
		return null;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/community/{communityId}/info", method = RequestMethod.GET)
	public SimpleCommunityInfo getSimpleCommunityInfo(@PathVariable("communityId") long communityId) {
		
		try {
			return communityService.findSimpleinfoById(communityId);
			
		} catch (Exception e) {	
			e.printStackTrace();
		}
		return null;
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/competition", method = RequestMethod.PUT)
	public SimpleStatusResponse addCompetition(@RequestBody SimpleCompetition simpleCompetition,Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			//Competition c=competitionService.add(simpleCompetition);
			Competition c=validationService.addCompetition(AuthUtil.getSimplePlayerFromPrincipal(principal),simpleCompetition);
			rc.setId(c.getId());
			//rc.setId(simpleCompetition.getId());
		} catch (Exception e) {
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
			
			//e.printStackTrace();
		}
		return rc;
	}
	
	
	/*
	@CrossOrigin
	@RequestMapping(value = "/competitionplayer", method = RequestMethod.PUT)
	public SimpleStatusResponse addCompetitionplayer(@RequestBody SimpleMember member, Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
	    SimplePlayer authenticatedUser=addSimplePlayerFromPrincipal(kp);
		try {
			Communityplayer player=communityplayerService.add(member,authenticatedUser);
			rc.setId(player.getId());
		} catch (Exception e) {
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
			
			//e.printStackTrace();
		}
		return rc;
	}
	*/
	@CrossOrigin
	@RequestMapping(value = "/competition/{competitionId}/info", method = RequestMethod.GET)
	public SimpleCompetitionInfo getSimpleCompetitionInfo(@PathVariable("competitionId") long competitionId) {
		
		try {
			return competitionService.findSimpleinfoById(competitionId);
			
		} catch (Exception e) {	
			//e.printStackTrace();
		}
		return null;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/competitionlocation", method = RequestMethod.PUT)
	public SimpleStatusResponse addCompetitionlocation(@RequestBody SimpleLocation location) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			Competitionlocation cl=competitionlocationService.add(location);
			rc.setId(cl.getId());
		} catch (Exception e) {
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
			
			//e.printStackTrace();
		}
		return rc;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/competition/{competitionId}/addroundforall", method = RequestMethod.GET)
	public SimpleStatusResponse addCompetitionroundForAllLocation(@PathVariable("competitionId") long competitionId) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			Competitioncycle sc= competitionService.addCompetitioncycleForAllLocation(competitionId);
			rc.setId(sc.getId());
		} catch (Exception e) {	
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
		}
		return rc;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/competition/{competitionId}/player", method = RequestMethod.POST)
	public SimpleStatusResponse editCompetitionplayer(@PathVariable("competitionId") long competitionId,@RequestBody Set<SimpleMember> setMember,Principal principal) {
		
		//Set<SimpleMember> setMember = new HashSet<>(member);
		SimplePlayer authUser=AuthUtil.getSimplePlayerFromPrincipal(principal);
		//System.out.println("competitionId:"+competitionId+" SET:"+setMember.size());
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		try {
			 Set<Competitionplayer> cpLs=validationService.addCompetitionplayer(authUser,setMember,  competitionId);
			 if(!cpLs.isEmpty()) {
				 if(cpLs.size()==setMember.size()) {
					 rc.setStatus(200);
				 }if(cpLs.size()!=setMember.size()) {
					 rc.setStatus(203); 
				 }
			 }else {
				 rc.setStatus(503);
			 }
			return rc;
		} catch (Exception e) {	
			rc.setStatus(501);
			rc.setMessage(e.getMessage());
		}
		return rc;
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/competition/{competitionId}/round/{roundId}/location", method = RequestMethod.GET)
	public Set<SimpleLocationCycle> getCompetitionlocationByRound(@PathVariable("competitionId") long competitionId,@PathVariable("roundId") long roundId) {
		
		return competitioncycleService.getCompetitionlocationByRoundId(roundId);
			
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/player/search/{text}", method = RequestMethod.GET)
	public Collection<SimplePlayer> findPlayerBySearchText(@PathVariable("text") String seachText){
		return communityService.findBySearchtext(seachText);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/community/{communityId}/player", method = RequestMethod.PUT)
	public SimpleStatusResponse addExistingPlayer2Community(@PathVariable("communityId") long communityId,@RequestBody SimplePlayer player) {
		return communityService.addPlayerToCommunity(player, communityId);
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/community/{communityId}/player/{memberId}", method = RequestMethod.POST)
	public SimpleStatusResponse editCommunityMember(@PathVariable("communityId") long communityId,@PathVariable("memberId") long memberId,@RequestBody SimpleMember member, Principal principal) {
		SimpleStatusResponse rc=new SimpleStatusResponse(200,"");
		
		
	    SimplePlayer authenticatedUser=AuthUtil.getSimplePlayerFromPrincipal(principal);
	    
		try {
			communityplayerService.editMember(member, authenticatedUser);
			rc.setStatus(200);
		} catch (Exception e) {
			
			rc.setStatus(500);
			rc.setMessage(e.getMessage());
		}
		
		
		
		return rc;
		
	}
	
	///competition/'+competitionId+'/round/'+roundId
	
	
	
}
