package org.teet.doko.resources;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.teet.doko.domain.service.CompetitionlocationcycleService;
import org.teet.doko.exception.PlayerincycleDeleteException;
import org.teet.doko.exception.PlayerincycleExistsException;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleStatusResponse;
import org.teet.doko.gateway.v1.SimpleLocationCycleplayer;
import org.teet.doko.gateway.v1.SimpleLocationroundInfo;
import org.teet.doko.gateway.v1.SimpleMatch;

@RestController
@RequestMapping("/game/v1")
public class MatchController {
	
	@Autowired
	CompetitionlocationcycleService competitionlocationcycleService;
	
	@CrossOrigin
	@RequestMapping(value = "/locationround/{locationroundId}", method = RequestMethod.GET)
	public SimpleLocationroundInfo getSimpleLocationcycleInfo(@PathVariable("locationroundId") long locationroundId) {
		
		try {
			return competitionlocationcycleService.getSimpleLocationcycleInfoById(locationroundId);
			
		} catch (Exception e) {	
			//e.printStackTrace();
		}
		return null;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/locationround/{locationroundId}/player", method = RequestMethod.PUT)
	public SimpleStatusResponse addPlayer2Cycle(@PathVariable("locationroundId") long locationroundId,@RequestBody Set<SimpleLocationCycleplayer> setPlayer) {
		SimpleStatusResponse res=new SimpleStatusResponse();
		res.setStatus(200);
		try {
			competitionlocationcycleService.addPlayer2Loctioncycle(setPlayer,locationroundId);
		}catch(PlayerincycleExistsException p){
			res.setStatus(504);
			res.setMessage(p.getMessage());
		} catch (Exception e) {
			res.setStatus(503);
			res.setMessage(e.getMessage());
		} 
		
		return res;
	}
	@CrossOrigin
	@RequestMapping(value = "/locationround/{locationroundId}/player/{cycleplayer}", method = RequestMethod.DELETE)
	public SimpleStatusResponse deletePlayerFromCycle(@PathVariable("locationroundId") long locationroundId,@PathVariable("cycleplayer") long cycleplayerId) {
		SimpleStatusResponse res=new SimpleStatusResponse();
		res.setStatus(200);
		try {
			
			competitionlocationcycleService.deletePlayerFromLocationcycle(cycleplayerId);
		}catch(PlayerincycleDeleteException p){
			res.setStatus(p.getErrorCode());
			res.setMessage(p.getMessage());
		} catch (Exception e) {
			res.setStatus(506);
			res.setMessage(e.getMessage());
		} 
		
		return res;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/locationround/{locationroundId}/match", method = RequestMethod.PUT)
	public SimpleStatusResponse addMatchForCycle(@PathVariable("locationroundId") long locationroundId,@RequestBody SimpleMatch match) {
		SimpleStatusResponse res=new SimpleStatusResponse();
		res.setStatus(200);
		try {
			competitionlocationcycleService.addMatchForCycle(match,locationroundId);
		} catch (Exception e) {
			res.setStatus(503);
			res.setMessage(e.getMessage());
		} 
		
		return res;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/locationround/{locationroundId}/match/{matchid}", method = RequestMethod.DELETE)
	public SimpleStatusResponse deleteMatchFromCycle(@PathVariable("locationroundId") long locationroundId,@PathVariable("matchid") long matchId) {
		SimpleStatusResponse res=new SimpleStatusResponse();
		res.setStatus(200);
		try {
			
			competitionlocationcycleService.deleteMatchByRoundAndId(matchId,locationroundId);
		}catch(StatusException p){
			res.setStatus(p.getErrorCode());
			res.setMessage(p.getMessage());
		} catch (Exception e) {
			res.setStatus(506);
			res.setMessage(e.getMessage());
		} 
		
		return res;
	}
}
