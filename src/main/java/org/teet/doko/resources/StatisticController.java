package org.teet.doko.resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.teet.doko.domain.service.CommunityStatisticService;
import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleGamestatistic;
import org.teet.doko.gateway.v1.SimplePlayercoin;
import org.teet.doko.gateway.v1.SimplePlayercoingroup;

@RestController
@RequestMapping("/statistic/v1")
public class StatisticController {
	@Autowired
	CommunityStatisticService statisticService;
	
	@CrossOrigin
	@RequestMapping(value = "/community/{communityId}/competition", method = RequestMethod.GET)
	public Collection<SimpleCompetition> getCompetitionByComminityId(@PathVariable("communityId") Long communityId) {
		return statisticService.getCompetitionByCommunityId(communityId);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/player/competition/{competitionId}", method = RequestMethod.GET)
	public Set<SimplePlayercoin> getGameruleById(@PathVariable("competitionId") long competitionId) {
		
		
		Set<SimplePlayercoin> coins=null;
			try {
				coins = statisticService.getSimplePlayercoinByCompetitionId(competitionId);
			} catch (Exception e) {
				e.printStackTrace();
				coins=new HashSet<SimplePlayercoin>();
				
			}
		
		return coins;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/player/competition/{competitionId}/statistic", method = RequestMethod.GET)
	public SimpleGamestatistic getPlayerstatisticByCompetition(@PathVariable("competitionId") Long competionId) {
		return statisticService.getPlayerstatisticByCompetition(competionId);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/player/competition/{competitionId}/sum", method = RequestMethod.GET)
	public List<SimplePlayercoin> getPlayersumByCompetition(@PathVariable("competitionId") Long competionId) {
		return statisticService.getPlayercoinSumByCompetition(competionId);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/player/competition/{competitionId}/sumcycle", method = RequestMethod.GET)
	public List<SimplePlayercoingroup> getPlayersumByCompetitionAndCycle(@PathVariable("competitionId") Long competionId) {
		return statisticService.getPlayercoinSumByCompetitionAndCycle(competionId);
		
		
	}
}
