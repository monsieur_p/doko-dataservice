package org.teet.doko.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.teet.doko.domain.Communitygamerule;
import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitioncycle;
import org.teet.doko.domain.Competitiongamerule;
import org.teet.doko.domain.Competitionlocation;
import org.teet.doko.domain.Competitionlocationcycle;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.domain.Competitionplayercycle;
import org.teet.doko.domain.Game;
import org.teet.doko.domain.Gameitem;
import org.teet.doko.domain.Gameitemtype;
import org.teet.doko.domain.Gamerule;
import org.teet.doko.domain.Gameitemcategory;
import org.teet.doko.domain.Gameruleitem;
import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.domain.Gamerulevariantitem;
import org.teet.doko.domain.Gamevariant;
import org.teet.doko.domain.Match;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.Playercoin;
import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleCycle;
import org.teet.doko.gateway.v1.SimpleGame;
import org.teet.doko.gateway.v1.SimpleGameitem;
import org.teet.doko.gateway.v1.SimpleGameitemtype;
import org.teet.doko.gateway.v1.SimpleGamerule;
import org.teet.doko.gateway.v1.SimpleGameitemcategory;
import org.teet.doko.gateway.v1.SimpleGameruleitem;
import org.teet.doko.gateway.v1.SimpleGamerulevariant;
import org.teet.doko.gateway.v1.SimpleGamerulevariantitem;
import org.teet.doko.gateway.v1.SimpleLocation;
import org.teet.doko.gateway.v1.SimpleLocationCycle;
import org.teet.doko.gateway.v1.SimpleLocationCycleplayer;
import org.teet.doko.gateway.v1.SimpleMatch;
import org.teet.doko.gateway.v1.SimpleMember;
import org.teet.doko.gateway.v1.SimplePlayer;
import org.teet.doko.gateway.v1.SimplePlayercoin;

public class PojoUtil {
	public static Set<SimpleCompetition> toSimpleCompetition(Set<Competition> cLs){
		Set<SimpleCompetition> sc= new HashSet<SimpleCompetition>();
		if(cLs!=null && !cLs.isEmpty()) {
			cLs.forEach(cp->{
				SimpleCompetition c=new SimpleCompetition();
				c.setCommunityId(cp.getCommunity().getId());
				c.setDate(cp.getDate());
				c.setTitle(cp.getTitle());
				c.setId(cp.getId());
				if(cp.getOwner()!=null)
					c.setOwner_id(cp.getOwner().getId());
				c.setBegin(cp.getBegin());
				c.setEnd(cp.getEnd());
				sc.add(c);
			});
		}
		return sc;
	}
	
	public static Collection<SimpleCompetition> toSimpleCompetition(Collection<Competition> cLs){
		Collection<SimpleCompetition> sc= new ArrayList<SimpleCompetition>();
		if(cLs!=null && !cLs.isEmpty()) {
			cLs.forEach(cp->{
				SimpleCompetition c=new SimpleCompetition();
				c.setCommunityId(cp.getCommunity().getId());
				c.setDate(cp.getDate());
				c.setTitle(cp.getTitle());
				c.setId(cp.getId());
				sc.add(c);
			});
		}
		return sc;
	}
	public static Set<SimpleMember> toSimpleMember(Set<Communityplayer> cLs){
		 Set<SimpleMember> sm= new HashSet<SimpleMember>();
		 if(cLs!=null && !cLs.isEmpty()) {
			cLs.forEach(cp->{
				SimpleMember m=new SimpleMember();
				m.setId(cp.getId());
				m.setCommunityId(cp.getCommunity().getId());
				m.setFirstname(cp.getPlayer().getFirstname());
				m.setLastname(cp.getPlayer().getLastname());
				m.setUsername(cp.getPlayer().getUsername());
				m.setActive(cp.isActive());
				m.setPlayerId(cp.getPlayer().getId());
				sm.add(m);
			});
		 }
		return sm;
	}
	
	public static Set<SimpleLocation> toSimpleLocation(Set<Competitionlocation> cLs){
		Set<SimpleLocation> sl= new HashSet<SimpleLocation>();
		if(cLs!=null && !cLs.isEmpty()) {
			cLs.forEach(cl->{
				SimpleLocation s=new SimpleLocation();
				s.setId(cl.getId());
				s.setName(cl.getName());
				sl.add(s);
			});
		}
		return sl;
	}
	
	public static Set<SimpleCycle> toSimpleCycle(Set<Competitioncycle> cLs){
		Set<SimpleCycle> sl= new HashSet<SimpleCycle>();
		if(cLs!=null && !cLs.isEmpty()) {
			cLs.forEach(cl->{
				SimpleCycle s=new SimpleCycle();
				s.setId(cl.getId());
				s.setSequence(cl.getSequence());
				s.setTitle(cl.getTitle());
				s.setLocationcycle(PojoUtil.toSimpleLocationCycle(cl.getLocationcycle()));
				sl.add(s);
			});
		}
		return sl;
	}
	public static Collection<SimplePlayer> toSimplePlayerFromPlayer(Collection<Player> cLs){
		Collection<SimplePlayer> sl= new ArrayList<SimplePlayer>();
		if(cLs!=null && !cLs.isEmpty()) {
			cLs.forEach(cl->{
				SimplePlayer s=new SimplePlayer();
				s.setId(cl.getId());
				s.setFirstname(cl.getFirstname());
				s.setLastname(cl.getLastname());
				s.setUsername(cl.getUsername());
				s.setEmail(cl.getEmail());
				s.setActive(cl.isActive());
				sl.add(s);
			});
		}
		return sl;
	}
	public static Set<SimplePlayer> toSimplePlayer(Set<Competitionplayer> cLs){
		Set<SimplePlayer> sl= new HashSet<SimplePlayer>();
		if(cLs!=null && !cLs.isEmpty()) {
			cLs.forEach(cl->{
				SimplePlayer s=toSimplePlayer(cl.getPlayer());
				//!! Überschreibt die Id aus Simpleplayer
				s.setId(cl.getId());
				
				s.setCompetitionId(cl.getCompetition().getId());
				s.setPlayerId(cl.getPlayer().getId());
				
				sl.add(s);
			});
		}
		return sl;
	}
	public static SimplePlayer toSimplePlayer(Player player){
		SimplePlayer s=new SimplePlayer();
		s.setId(player.getId());
		s.setFirstname(player.getFirstname());
		s.setLastname(player.getLastname());
		s.setUsername(player.getUsername());
		s.setEmail(player.getEmail());
		s.setActive(player.isActive());
		return s;
	}
	public static Set<SimpleLocationCycleplayer> toSimpleCycleplayer(Set<Competitionplayercycle> cLs){
		Set<SimpleLocationCycleplayer> sl= new HashSet<SimpleLocationCycleplayer>();
		if(cLs!=null && !cLs.isEmpty()) {
			cLs.forEach(cl->{
				
				SimpleLocationCycleplayer s=new SimpleLocationCycleplayer();
				s.setId(cl.getId());
				Player p=cl.getPlayer();
				s.setFirstname(p.getFirstname());
				s.setLastname(p.getLastname());
				s.setUsername(p.getUsername());
				s.setPlayerId(p.getId());
				s.setSequence(cl.getSequence());
				sl.add(s);
			});
		}
		return sl;
	}
	
	public static  Set<SimpleLocationCycle> toSimpleLocationCycle(Set<Competitionlocationcycle> clcSet) {
		 Set<SimpleLocationCycle> slcSet= new HashSet<SimpleLocationCycle>();
		 if(clcSet!=null && !clcSet.isEmpty()) {
			 clcSet.forEach(cl->{
				 slcSet.add(PojoUtil.toSingleSimpleLocationCycle(cl,false));
			});
		 }
		 return slcSet;
	}
	public static  SimpleLocationCycle toSingleSimpleLocationCycle(Competitionlocationcycle cl) {
		return PojoUtil.toSingleSimpleLocationCycle(cl,true);
	}
	
	public static  SimpleLocationCycle toSingleSimpleLocationCycle(Competitionlocationcycle cl, boolean details) {
			SimpleLocationCycle slc=new SimpleLocationCycle();
		    slc.setCycleId(cl.getCycle().getId());
			slc.setLocationId(cl.getLocation().getId());
			slc.setName(cl.getLocation().getName());
			slc.setSequence(cl.getCycle().getSequence());
			slc.setId(cl.getId());
			if(details) {
				Set<Competitionplayercycle> playerSet= cl.getPlayer();
				playerSet.forEach(p->{
					
				});
			}
				 
			return slc;
	}
	
	public static Set<SimpleMatch> toSimpleMatch(Set<Match> matchSet){
		Set<SimpleMatch> mLs=new HashSet<SimpleMatch>();
		if(matchSet!=null && !matchSet.isEmpty()) {
			matchSet.forEach(m->{
				SimpleMatch sm=new SimpleMatch();
				sm.setId(m.getId());
				sm.setSequence(m.getSequence());
				sm.setVariant(m.getGamerulevariant().getTitle());
				sm.setVariantId(m.getGamerulevariant().getId());
				sm.setCoin(m.getCoin());
				sm.setPlayercoin(toSimplePlayercoin(m.getPlayercoin()));
				sm.setFactor(m.getFactor());
				mLs.add(sm);
			});
		}
		return mLs;
	}
	
	public static Set<SimpleGamerule> toSimpleRuleByCommpetitionrule(Set<Competitiongamerule> cgr){
		Set<SimpleGamerule> sgrLs=new HashSet<SimpleGamerule>();
		if(cgr!=null && !cgr.isEmpty()) {
			cgr.forEach(gr->{
				SimpleGamerule sg=PojoUtil.toSingleSimpleRule(gr.getGamerule());
				sg.setId(gr.getId());
				sgrLs.add(sg);
			});
		}
		return sgrLs;
	}
	
	
	public static Set<SimpleGamerule> toSimpleRuleByCommunityrule(Set<Communitygamerule> cgr){
		Set<SimpleGamerule> sgrLs=new HashSet<SimpleGamerule>();
		if(cgr!=null && !cgr.isEmpty()) {
			cgr.forEach(gr->{
				SimpleGamerule sg=PojoUtil.toSingleSimpleRule(gr.getGamerule());//new SimpleGamerule();
				sg.setId(gr.getId());
				
				
				
				sgrLs.add(sg);
			});
		}
		return sgrLs;
	}
	
	public static SimpleGamerule toSingleSimpleRule(Gamerule gr){
		SimpleGamerule sg=new SimpleGamerule();
		
		sg.setGameTitle(gr.getGame().getTitle());
		sg.setTitle(gr.getTitle());
		sg.setVariant(toSimpleGamerulevariant(gr.getVariant()));
		
		
		return sg;
	}
	
	public static Set<SimpleGamerulevariant> toSimpleVariant(Set<Gamevariant> cgr){
		Set<SimpleGamerulevariant> sgvLs=new HashSet<SimpleGamerulevariant>();
		if(cgr!=null && !cgr.isEmpty()) {
			cgr.forEach(gr->{
				SimpleGamerulevariant sg=new SimpleGamerulevariant();
				sg.setId(gr.getId());
				sg.setTitle(gr.getTitle());
				
				
				
				sgvLs.add(sg);
			});
		}
		return sgvLs;
	}
	public static List<SimpleGameitemcategory> toSimpleCategoryList(List<Gameitemcategory> categoryLs){
		List<SimpleGameitemcategory> scatList=new ArrayList<SimpleGameitemcategory>();
		if(categoryLs!=null) {
			for(Gameitemcategory item:categoryLs) {
				scatList.add(toSimpleCategory(item));
			}
		}
		return scatList;
	}
	public static Set<SimpleGameitemcategory> toSimpleCategory(Set<Gameitemcategory> categoryLs){
		Set<SimpleGameitemcategory> scatList=new HashSet<SimpleGameitemcategory>();
		
		if(categoryLs!=null) {
			categoryLs.forEach(item->{
				scatList.add(toSimpleCategory(item));
			});
		}
		return scatList;
	}
	
	public static SimpleGameitemcategory toSimpleCategory(Gameitemcategory category){
		SimpleGameitemcategory scat=null;
		if(category!=null) {
			scat=new SimpleGameitemcategory();
			if(category.getGame()!=null)
				scat.setGame_id(category.getGame().getId());
			scat.setId(category.getId());
			scat.setPriority(category.getPriority());
			scat.setName(category.getName());
		}
		return scat;
	}
	
	public static List<SimpleGame> toSimpleGameList(List<Game> gameLs){
		List<SimpleGame> gameList=new ArrayList<SimpleGame>();
		if(gameLs!=null) {
		for(Game item:gameLs) {
			SimpleGame g1=new SimpleGame();
			g1.setId(item.getId());
			g1.setTitle(item.getTitle());
			if(item.getOwner()!=null) {
				g1.setOwner_id(item.getOwner().getId());
			}
			g1.setItem(PojoUtil.toSimpleGameitemList(item.getItem()));
			g1.setItemtype(toSimpleGameitemtype(item.getType()));
			g1.setCategory(PojoUtil.toSimpleCategory(item.getCategory()));
			gameList.add(g1);
		}
		}
		return gameList;
	}
	public static Set<SimpleGameitemtype> toSimpleGameitemtype(Set<Gameitemtype> varSet){
		Set<SimpleGameitemtype> sgrvSet=new HashSet<SimpleGameitemtype>();
		if(varSet!=null) {
			varSet.forEach(var->{
				SimpleGameitemtype type=toSimpleGameitemtype(var);
				sgrvSet.add(type);
			});
		}
		return sgrvSet;
	}
	public static SimpleGameitemtype toSimpleGameitemtype(Gameitemtype var){
		SimpleGameitemtype type=null;
		if(var!=null) {
			type=new SimpleGameitemtype();
			if(var.getGame()!=null)
				type.setGame_id(var.getGame().getId());
			
			type.setId(var.getId());
			type.setName(var.getName());
			type.setSequence(var.getSequence());
			type.setIconId(var.getIconId());
		}
		return type;
	}
	public static Set<SimpleGamerulevariant> toSimpleGamerulevariant(Set<Gamerulevariant> varSet){
		Set<SimpleGamerulevariant> sgrvSet=new HashSet<SimpleGamerulevariant>();
		
		varSet.forEach(var->{
			sgrvSet.add(toSingleSimpleGamerulevariant(var));
		});
		
		return sgrvSet;
	}
	
	public static SimpleGamerulevariant toSingleSimpleGamerulevariant(Gamerulevariant var){
		SimpleGamerulevariant sgrv=new SimpleGamerulevariant();
		sgrv.setId(var.getId());
		sgrv.setDescription(var.getDescription());
		sgrv.setTitle(var.getTitle());
		
		return sgrv;
	}
	public static List<SimpleGamerule> toSimpleGamerule(Collection<Gamerule> cLs){
		List<SimpleGamerule> sc= new ArrayList<SimpleGamerule>();
		if(cLs!=null && !cLs.isEmpty()) {
			cLs.forEach(cp->{
				SimpleGamerule c=toSimpleGamerule(cp);
				sc.add(c);
			});
		}
		return sc;
	}
	public static SimpleGamerule toSimpleGamerule(Gamerule var){
		SimpleGamerule sgrv=new SimpleGamerule();
		sgrv.setId(var.getId());
		sgrv.setTitle(var.getTitle());
		if(var.getOwner()!=null)
			sgrv.setOwner_id(var.getOwner().getId());
		sgrv.setGameTitle(var.getGame().getTitle());
		sgrv.setGameId(var.getGame().getId());
		sgrv.setVariant(toSimpleGamerulevariant(var.getVariant()));
		sgrv.setItem(toSimpleGameruleitemList(var.getItem()));
		return sgrv;
	}
	public static Set<SimplePlayercoin> toSimplePlayercoin(Set<Playercoin> cgr){
		Set<SimplePlayercoin> sgvLs=new HashSet<SimplePlayercoin>();
		if(cgr!=null && !cgr.isEmpty()) {
			cgr.forEach(gr->{
				SimplePlayercoin sg=new SimplePlayercoin();
				sg.setId(gr.getId());
				Player pl=gr.getPlayer();
				if(pl!=null) {
					sg.setFirstname(pl.getFirstname());
					sg.setLastname(pl.getLastname());
					sg.setUsername(pl.getUsername());
					sg.setUserId(pl.getId());
				}
				sg.setCoin(gr.getCoin());
				sg.setCyclesequence(gr.getCyclesequence());
				Match m=gr.getMatch();
				if(m!=null)
					sg.setMatchId(m.getId());
				
				
				sgvLs.add(sg);
			});
		}
		return sgvLs;
	}
	
	public static Set<SimpleGameitem> toSimpleGameitemList(Set<Gameitem> item){
		Set<SimpleGameitem> sgriLs=new HashSet<SimpleGameitem>();
		if(item!=null && !item.isEmpty()) {
			item.forEach(gr->{
				SimpleGameitem sg=new SimpleGameitem();
				sg.setId(gr.getId());
				Game pl=gr.getGame();
				if(pl!=null) {
					sg.setGame_id(pl.getId());
				}
				
				sg.setName(gr.getName());
				sg.setPoints(gr.getPoints());
				sg.setCount(gr.getCount());
				sg.setImageId(gr.getImageId());
				sg.setIconId(gr.getIconId());
				sg.setType(toSimpleGameitemtype(gr.getItemtype()));
				sgriLs.add(sg);
			});
		}
		return sgriLs;
	}
	
	public static Set<SimpleGameruleitem> toSimpleGameruleitemList(Set<Gameruleitem> item){
		Set<SimpleGameruleitem> sgriLs=new HashSet<SimpleGameruleitem>();
		if(item!=null && !item.isEmpty()) {
			item.forEach(gr->{
				sgriLs.add(toSimpleGameruleitem(gr));
			});
		}
		return sgriLs;
	}
	public static SimpleGameruleitem toSimpleGameruleitem(Gameruleitem item){
		SimpleGameruleitem sg=null;
		if(item!=null) {
			sg=new SimpleGameruleitem();
			sg.setId(item.getId());
			Gamerule pl=item.getGamerule();
			if(pl!=null) {
				sg.setGamerule_id(pl.getId());
			}
			
			sg.setName(item.getName());
			sg.setPoints(item.getPoints());
			sg.setSequence(item.getSequence());
			
			sg.setCount(item.getCount());
			Gameitem g=item.getGameitem();
			if(g!=null) {
				sg.setGameitem_id(g.getId());
				sg.setItemtype(toSimpleGameitemtype(g.getItemtype()));
			}
			
			
		}
		return sg;
	}
	
	
	public static Set<SimpleGamerulevariantitem> toSimpleGamerulevariantitemList(Set<Gamerulevariantitem> item){
		Set<SimpleGamerulevariantitem> sgriLs=new HashSet<SimpleGamerulevariantitem>();
		if(item!=null && !item.isEmpty()) {
			item.forEach(gr->{
				SimpleGamerulevariantitem sg=new SimpleGamerulevariantitem();
				sg.setId(gr.getId());
				Gamerulevariant pl=gr.getGamerulevariant();
				if(pl!=null) {
					sg.setGamerulevariant_id(pl.getId());
				}
				Gameitemcategory cat=gr.getCategory();
				if(cat!=null) {
					sg.setCategory_id(cat.getId());
					sg.setItemcategorylabel(cat.getName());
				}
				sg.setSequence(gr.getSequence());
				
				sg.setPoints(gr.getPoints());
				
				sgriLs.add(sg);
			});
		}
		return sgriLs;
	}
	
}
