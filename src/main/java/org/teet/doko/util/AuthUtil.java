package org.teet.doko.util;

import java.security.Principal;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.teet.doko.gateway.v1.SimplePlayer;

public class AuthUtil {
	public static SimplePlayer getSimplePlayerFromPrincipal(Principal principal) {
		KeycloakPrincipal<KeycloakSecurityContext> kp = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
		SimplePlayer user =null;
		
		if(kp!=null) {
			AccessToken token = kp.getKeycloakSecurityContext().getToken();
			user =new SimplePlayer();
			user.setFirstname(token.getGivenName());
			user.setLastname(token.getFamilyName());
			user.setEmail(token.getEmail());
			user.setUserid(token.getSubject());
			user.setPhone(token.getPhoneNumber());
			
			
		}
		return user;
	}
}
