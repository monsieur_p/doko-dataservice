package org.teet.doko.gateway.v1;

import java.util.List;

/**
 * Objekt zur, Anfrage eines neuen Spiels in der Runde 
 * @author todhinio
 *
 */
public class SimpleMatchRequest {
	private List<SimplePlayer> player;
	private long locationroundId;
	private boolean showCards;
	public List<SimplePlayer> getPlayer() {
		return player;
	}
	public void setPlayer(List<SimplePlayer> player) {
		this.player = player;
	}
	public long getLocationroundId() {
		return locationroundId;
	}
	public void setLocationroundId(long locationroundId) {
		this.locationroundId = locationroundId;
	}
	public boolean isShowCards() {
		return showCards;
	}
	public void setShowCards(boolean showCards) {
		this.showCards = showCards;
	}
	
	
	
	
}
