package org.teet.doko.gateway.v1;

import java.sql.Timestamp;
import java.util.Collection;

public class SimpleCompetitionInfo {
	private long id;
	private String title;
    private long communityId;
    private Long owner_id;
    private Timestamp begin;
    private Timestamp end;
    private Collection<SimplePlayer> player;
    private Collection<SimpleLocation> location;
    private Collection<SimpleCycle> cycle;
    private Collection<SimpleGamerule> rule;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getCommunityId() {
		return communityId;
	}
	public void setCommunityId(long communityId) {
		this.communityId = communityId;
	}
	public Collection<SimplePlayer> getPlayer() {
		return player;
	}
	public void setPlayer(Collection<SimplePlayer> player) {
		this.player = player;
	}
	public Collection<SimpleLocation> getLocation() {
		return location;
	}
	public void setLocation(Collection<SimpleLocation> location) {
		this.location = location;
	}
	public Collection<SimpleCycle> getCycle() {
		return cycle;
	}
	public void setCycle(Collection<SimpleCycle> cycle) {
		this.cycle = cycle;
	}
	public Collection<SimpleGamerule> getRule() {
		return rule;
	}
	public void setRule(Collection<SimpleGamerule> rule) {
		this.rule = rule;
	}
	public Long getOwner_id() {
		return owner_id;
	}
	public void setOwner_id(Long owner_id) {
		this.owner_id = owner_id;
	}
	public Timestamp getBegin() {
		return begin;
	}
	public void setBegin(Timestamp begin) {
		this.begin = begin;
	}
	public Timestamp getEnd() {
		return end;
	}
	public void setEnd(Timestamp end) {
		this.end = end;
	}
    
    
    
    
    
}
