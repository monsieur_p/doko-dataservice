package org.teet.doko.gateway.v1;

import java.util.List;

public class SimpleStatistic {
	private String title;
	private List<SimpleStatisticItem> item;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<SimpleStatisticItem> getItem() {
		return item;
	}
	public void setItem(List<SimpleStatisticItem> item) {
		this.item = item;
	}
	
	
}
