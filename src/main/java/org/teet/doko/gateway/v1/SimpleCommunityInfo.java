package org.teet.doko.gateway.v1;

import java.util.Collection;

public class SimpleCommunityInfo {
	private long id;
	private String title;
	private Long owner_id;
	private String owner;
	private Collection<SimpleMember> member;
	private Collection<SimpleCompetition> competition;
	private Collection<SimpleGamerule> rule;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	
	public Collection<SimpleMember> getMember() {
		return member;
	}
	public void setMember(Collection<SimpleMember> member) {
		this.member = member;
	}
	public Collection<SimpleCompetition> getCompetition() {
		return competition;
	}
	public void setCompetition(Collection<SimpleCompetition> competition) {
		this.competition = competition;
	}
	public Collection<SimpleGamerule> getRule() {
		return rule;
	}
	public void setRule(Collection<SimpleGamerule> rule) {
		this.rule = rule;
	}
	public Long getOwner_id() {
		return owner_id;
	}
	public void setOwner_id(Long owner_id) {
		this.owner_id = owner_id;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	
	
}
