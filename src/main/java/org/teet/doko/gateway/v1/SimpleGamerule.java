package org.teet.doko.gateway.v1;

import java.util.Set;

public class SimpleGamerule {
	private Long id;
	private Long gameId;
	private String gameTitle;
	private String title;
	private Long owner_id;
	private Set<SimpleGameruleitem>item; 
	private Set<SimpleGamerulevariant>variant; 
	private int playermin;
	private int playermax;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getGameId() {
		return gameId;
	}
	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}
	public String getGameTitle() {
		return gameTitle;
	}
	public void setGameTitle(String gameTitle) {
		this.gameTitle = gameTitle;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Set<SimpleGamerulevariant> getVariant() {
		return variant;
	}
	public void setVariant(Set<SimpleGamerulevariant> variant) {
		this.variant = variant;
	}
	public int getPlayermin() {
		return playermin;
	}
	public void setPlayermin(int playermin) {
		this.playermin = playermin;
	}
	public int getPlayermax() {
		return playermax;
	}
	public void setPlayermax(int playermax) {
		this.playermax = playermax;
	}
	public Set<SimpleGameruleitem> getItem() {
		return item;
	}
	public void setItem(Set<SimpleGameruleitem> item) {
		this.item = item;
	}
	public Long getOwner_id() {
		return owner_id;
	}
	public void setOwner_id(Long owner_id) {
		this.owner_id = owner_id;
	}
	
	
}
