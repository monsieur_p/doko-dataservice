package org.teet.doko.gateway.v1;

public class SimplePlayercoin {
	private long id;
	private int coin;
	private int cyclesequence;
	private long userId;
	private String firstname;
	private String lastname;
	private String username;
	private long matchId;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getCoin() {
		return coin;
	}
	public void setCoin(int coin) {
		this.coin = coin;
	}
	public int getCyclesequence() {
		return cyclesequence;
	}
	public void setCyclesequence(int cyclesequence) {
		this.cyclesequence = cyclesequence;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public long getMatchId() {
		return matchId;
	}
	public void setMatchId(long matchId) {
		this.matchId = matchId;
	}
	
	
}
