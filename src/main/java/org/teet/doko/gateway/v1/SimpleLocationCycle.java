package org.teet.doko.gateway.v1;

import java.util.Set;

public class SimpleLocationCycle {
	private Long id;
	private String name;
	private int sequence;
	private long locationId;
	private long cycleId;
	private Set<SimpleLocationCycleplayer> player;
	private Set<SimpleMatch> match;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public long getLocationId() {
		return locationId;
	}
	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}
	public long getCycleId() {
		return cycleId;
	}
	public void setCycleId(long cycleId) {
		this.cycleId = cycleId;
	}
	public Set<SimpleLocationCycleplayer> getPlayer() {
		return player;
	}
	public void setPlayer(Set<SimpleLocationCycleplayer> player) {
		this.player = player;
	}
	public Set<SimpleMatch> getMatch() {
		return match;
	}
	public void setMatch(Set<SimpleMatch> match) {
		this.match = match;
	}
	
	
}
