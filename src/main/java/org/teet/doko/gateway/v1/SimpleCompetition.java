package org.teet.doko.gateway.v1;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;

public class SimpleCompetition {
	private Long id;
	private String title;
	private Long owner_id;
    private long communityId;
    private Date date;
    private Timestamp begin;
    private Timestamp end;
    private Collection<SimpleGamerule> rule;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public long getCommunityId() {
		return communityId;
	}
	public void setCommunityId(long communityId) {
		this.communityId = communityId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Collection<SimpleGamerule> getRule() {
		return rule;
	}
	public void setRule(Collection<SimpleGamerule> rule) {
		this.rule = rule;
	}
	public Long getOwner_id() {
		return owner_id;
	}
	public void setOwner_id(Long owner_id) {
		this.owner_id = owner_id;
	}
	public Timestamp getBegin() {
		return begin;
	}
	public void setBegin(Timestamp begin) {
		this.begin = begin;
	}
	public Timestamp getEnd() {
		return end;
	}
	public void setEnd(Timestamp end) {
		this.end = end;
	}
	
	
    
    
}
