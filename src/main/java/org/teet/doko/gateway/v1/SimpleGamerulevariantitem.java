package org.teet.doko.gateway.v1;


public class SimpleGamerulevariantitem {
	private Long id;
	private int sequence;
	private long gamerulevariant_id;
	public long gameruleitem_id;
	private String itemlabel;
	private String itemcategorylabel;
	private long itemcategory_id;
	
	private long item_id;
	private int points;
	private long category_id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getItemlabel() {
		return itemlabel;
	}

	public void setItemlabel(String itemlabel) {
		this.itemlabel = itemlabel;
	}

	public long getItem_id() {
		return item_id;
	}

	public void setItem_id(long item_id) {
		this.item_id = item_id;
	}

	public long getCategory_id() {
		return category_id;
	}

	public void setCategory_id(long category_id) {
		this.category_id = category_id;
	}

	public long getItemcategory_id() {
		return itemcategory_id;
	}

	public void setItemcategory_id(long itemcategory_id) {
		this.itemcategory_id = itemcategory_id;
	}

	public String getItemcategorylabel() {
		return itemcategorylabel;
	}

	public void setItemcategorylabel(String itemcategorylabel) {
		this.itemcategorylabel = itemcategorylabel;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public long getGamerulevariant_id() {
		return gamerulevariant_id;
	}

	public void setGamerulevariant_id(long gamerulevariant_id) {
		this.gamerulevariant_id = gamerulevariant_id;
	}

	public long getGameruleitem_id() {
		return gameruleitem_id;
	}

	public void setGameruleitem_id(long gameruleitem_id) {
		this.gameruleitem_id = gameruleitem_id;
	}

	
	
	
}

