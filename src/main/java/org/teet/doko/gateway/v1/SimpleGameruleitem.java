package org.teet.doko.gateway.v1;

public class SimpleGameruleitem {	
	private Long id;
	private String name;
	private long gamerule_id;
	private long gameitem_id;
	private SimpleGameitemtype itemtype;
	private int sequence;
	private int count;
	
	private int points;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getGamerule_id() {
		return gamerule_id;
	}
	public void setGamerule_id(long gamerule_id) {
		this.gamerule_id = gamerule_id;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public long getGameitem_id() {
		return gameitem_id;
	}
	public void setGameitem_id(long gameitem_id) {
		this.gameitem_id = gameitem_id;
	}

	public SimpleGameitemtype getItemtype() {
		return itemtype;
	}
	public void setItemtype(SimpleGameitemtype itemtype) {
		this.itemtype = itemtype;
	}
	
	
	
	
}
