package org.teet.doko.gateway.v1;

import java.util.Set;

public class SimpleGamestatistic {
	private Set<SimpleGamerulevariant> variant;
	private Set<SimplePlayerresult> player;
	public Set<SimpleGamerulevariant> getVariant() {
		return variant;
	}
	public void setVariant(Set<SimpleGamerulevariant> variant) {
		this.variant = variant;
	}
	public Set<SimplePlayerresult> getPlayer() {
		return player;
	}
	public void setPlayer(Set<SimplePlayerresult> player) {
		this.player = player;
	}
	
	
}
