package org.teet.doko.gateway.v1;

public class SimpleLocation {
	private long id;
	private String name;
	private Long competitionId;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCompetitionId() {
		return competitionId;
	}
	public void setCompetitionId(Long competitionId) {
		this.competitionId = competitionId;
	}
	
	
	
}
