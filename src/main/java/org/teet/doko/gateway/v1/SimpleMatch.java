package org.teet.doko.gateway.v1;

import java.util.Set;

public class SimpleMatch {
	private long id;
	private  int sequence;
	private String variant;
	private long cycleId;
	private long variantId;
	private int factor; 
	private int coin;
	private Set<SimplePlayercoin> playercoin;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public int getCoin() {
		return coin;
	}
	public void setCoin(int coin) {
		this.coin = coin;
	}
	public long getCycleId() {
		return cycleId;
	}
	public void setCycleId(long cycleId) {
		this.cycleId = cycleId;
	}
	public long getVariantId() {
		return variantId;
	}
	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}
	
	public int getFactor() {
		return factor;
	}
	public void setFactor(int factor) {
		this.factor = factor;
	}
	public Set<SimplePlayercoin> getPlayercoin() {
		return playercoin;
	}
	public void setPlayercoin(Set<SimplePlayercoin> playercoin) {
		this.playercoin = playercoin;
	}
	
	
}
