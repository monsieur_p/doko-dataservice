package org.teet.doko.gateway.v1;

import java.util.Set;

public class SimplePlayerresult {
	private long id;
	private Set<SimpleGameresult> game;
	private String firstname;
	private String lastname;
	private int result;
	private double average;
	private int matches;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Set<SimpleGameresult> getGame() {
		return game;
	}
	public void setGame(Set<SimpleGameresult> game) {
		this.game = game;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}
	public int getMatches() {
		return matches;
	}
	public void setMatches(int matches) {
		this.matches = matches;
	}
	
	
}
