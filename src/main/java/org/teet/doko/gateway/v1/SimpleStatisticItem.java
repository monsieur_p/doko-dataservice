package org.teet.doko.gateway.v1;

import java.util.List;

public class SimpleStatisticItem {
	private String label;
	private List<SimpleStatisticCoin> coin;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public List<SimpleStatisticCoin> getCoin() {
		return coin;
	}
	public void setCoin(List<SimpleStatisticCoin> coin) {
		this.coin = coin;
	}
	
	
	
}
