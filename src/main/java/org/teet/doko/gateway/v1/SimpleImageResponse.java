package org.teet.doko.gateway.v1;

public class SimpleImageResponse extends SimpleStatusResponse{
	 public SimpleImageResponse() {
		 super();
	 }	
	 
	 private String name;
	 private String type;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	 
	 
}
