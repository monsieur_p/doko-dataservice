package org.teet.doko.gateway.v1;


public class SimpleGameitem {
	private Long id;
	private int sequence;
	private String name;
	
	private long game_id;
	
	private int points;
	private int count;
	private Long iconId;
	private Long imageId;
	
	private SimpleGameitemtype type;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	

	

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getGame_id() {
		return game_id;
	}

	public void setGame_id(long game_id) {
		this.game_id = game_id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Long getIconId() {
		return iconId;
	}

	public void setIconId(Long iconId) {
		this.iconId = iconId;
	}

	public Long getImageId() {
		return imageId;
	}

	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}

	public SimpleGameitemtype getType() {
		return type;
	}

	public void setType(SimpleGameitemtype type) {
		this.type = type;
	}

	
	
	
}

