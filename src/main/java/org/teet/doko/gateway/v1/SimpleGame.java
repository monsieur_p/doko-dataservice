package org.teet.doko.gateway.v1;

import java.util.Set;

public class SimpleGame {
	private long id;
	private String title;
	private Set<SimpleGameitemcategory> category;
	private Set<SimpleGameitem> item;
	private Set<SimpleGameitemtype> itemtype;
	private Long owner_id;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Set<SimpleGameitem> getItem() {
		return item;
	}
	public void setItem(Set<SimpleGameitem> item) {
		this.item = item;
	}
	public Long getOwner_id() {
		return owner_id;
	}
	public void setOwner_id(Long owner_id) {
		this.owner_id = owner_id;
	}
	public Set<SimpleGameitemtype> getItemtype() {
		return itemtype;
	}
	public void setItemtype(Set<SimpleGameitemtype> itemtype) {
		this.itemtype = itemtype;
	}
	public Set<SimpleGameitemcategory> getCategory() {
		return category;
	}
	public void setCategory(Set<SimpleGameitemcategory> category) {
		this.category = category;
	}
	
	
	
	
}
