package org.teet.doko.gateway.v1;

import java.util.Set;

public class SimpleLocationroundInfo {
	private long id;
	private Set<SimpleMatch> match;
	private Set<SimpleLocationCycleplayer> player;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Set<SimpleMatch> getMatch() {
		return match;
	}
	public void setMatch(Set<SimpleMatch> match) {
		this.match = match;
	}
	public Set<SimpleLocationCycleplayer> getPlayer() {
		return player;
	}
	public void setPlayer(Set<SimpleLocationCycleplayer> player) {
		this.player = player;
	}
	
	
	
}
