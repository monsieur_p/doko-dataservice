package org.teet.doko.gateway.v1;

import java.util.Set;



public class SimpleGamerulevariant {
	private Long id;
	private String description;
	private String title;
	private Set<SimpleGamerulevariantitem>item; 
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Set<SimpleGamerulevariantitem> getItem() {
		return item;
	}
	public void setItem(Set<SimpleGamerulevariantitem> item) {
		this.item = item;
	}
	
	
}
