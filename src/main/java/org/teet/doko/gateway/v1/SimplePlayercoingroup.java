package org.teet.doko.gateway.v1;

import java.util.List;

public class SimplePlayercoingroup {
	private int sequence;
	private List<SimplePlayercoin> coin;
	
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public List<SimplePlayercoin> getCoin() {
		return coin;
	}
	public void setCoin(List<SimplePlayercoin> coin) {
		this.coin = coin;
	}
	
	
}
