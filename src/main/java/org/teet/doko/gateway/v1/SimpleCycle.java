package org.teet.doko.gateway.v1;

import java.util.Set;

public class SimpleCycle {
	private long id;
	private String title;
	private long competitionId;
	private int sequence;
	Set<SimpleLocationCycle> locationcycle;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getCompetitionId() {
		return competitionId;
	}
	public void setCompetitionId(long competitionId) {
		this.competitionId = competitionId;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public Set<SimpleLocationCycle> getLocationcycle() {
		return locationcycle;
	}
	public void setLocationcycle(Set<SimpleLocationCycle> locationcycle) {
		this.locationcycle = locationcycle;
	}
	
	
}
