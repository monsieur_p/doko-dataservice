package org.teet.doko.gateway.v1;

public class SimpleStatusResponse {
	public SimpleStatusResponse() {}
	public SimpleStatusResponse(int status,String message) {
		this.setStatus(status);
		this.setMessage(message);
	}
	public SimpleStatusResponse(int status,String message,long id) {
		this.setStatus(status);
		this.setMessage(message);
		this.setId(id);
	}
	private int status;
	private String message;
	private long id;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
}
