package org.teet.doko.domain;

import org.springframework.data.repository.CrudRepository;

public interface GamevariantRepository extends CrudRepository<Gamevariant, Long> {

	Gamevariant findByTitle(String title);
}
