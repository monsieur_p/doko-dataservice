package org.teet.doko.domain;

import org.springframework.data.repository.CrudRepository;

public interface CommunityplayerRepository extends CrudRepository<Communityplayer, Long> {
	
	public Communityplayer findByCommunityAndPlayer(Community community, Player player) throws Exception;
}
