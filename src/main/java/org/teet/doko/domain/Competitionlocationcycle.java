package org.teet.doko.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

/*
 * Tischrunde
 */

@Entity
public class Competitionlocationcycle {
	private long id;
	private boolean closed=false;
	private Competitionlocation location;
	private Competitioncycle cycle;
	private Set<Match> match;
	private Set<Competitionplayercycle> player;
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@ManyToOne(optional = false,cascade = CascadeType.ALL)
	@JoinColumn(name = "competitionlocation_id",nullable=false)
	public Competitionlocation getLocation() {
		return location;
	}
	public void setLocation(Competitionlocation location) {
		this.location = location;
	}
	
	@ManyToOne(optional = false,cascade = CascadeType.ALL)
	@JoinColumn(name = "competitioncycle_id",nullable=false)
	public Competitioncycle getCycle() {
		return cycle;
	}
	
	public void setCycle(Competitioncycle cycle) {
		this.cycle = cycle;
	}
	
	@JsonIgnore
	@OneToMany(
			mappedBy = "competitionlocationcycle",
			orphanRemoval=true,
	        cascade = CascadeType.ALL, 
	        fetch = FetchType.LAZY
	    )
	public Set<Match> getMatch() {
		return match;
	}
	
	public void setMatch(Set<Match> match) {
		this.match = match;
	}
	
	@JsonIgnore
	@OneToMany(
			mappedBy = "locationcycle",
			orphanRemoval=true,
	        cascade = CascadeType.ALL, 
	        fetch = FetchType.LAZY
	    )
	public Set<Competitionplayercycle> getPlayer() {
		return player;
	}
	public void setPlayer(Set<Competitionplayercycle> player) {
		this.player = player;
	}
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}
	
	
	
	
	
}
