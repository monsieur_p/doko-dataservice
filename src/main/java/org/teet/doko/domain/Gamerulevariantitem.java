package org.teet.doko.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * z. B. Reihenfolge und Kategorisierung
 * @author todhinio
 *
 */

@Entity
public class Gamerulevariantitem {
	private long id;
	private int sequence;
	private Gamerulevariant gamerulevariant;
	private Gameruleitem item;
	
	private Gameitemcategory category;
	private int points;
	
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	@ManyToOne(optional = false)
	@JoinColumn(name = "gameruleitem_id",nullable=false)
	public Gameruleitem getItem() {
		return item;
	}
	public void setItem(Gameruleitem item) {
		this.item = item;
	}
	@ManyToOne(optional = false)
	@JoinColumn(name = "gamerulecategory_id",nullable=false)
	public Gameitemcategory getCategory() {
		return category;
	}
	public void setCategory(Gameitemcategory category) {
		this.category = category;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "gamerulevariant_id",nullable=false)
	public Gamerulevariant getGamerulevariant() {
		return gamerulevariant;
	}
	public void setGamerulevariant(Gamerulevariant gamerulevariant) {
		this.gamerulevariant = gamerulevariant;
	}
	
	
	
}
