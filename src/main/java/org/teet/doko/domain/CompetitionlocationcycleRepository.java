package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;


public interface CompetitionlocationcycleRepository extends CrudRepository<Competitionlocationcycle, Long> {
	public Collection<Competitionlocationcycle> findByCycle(Competitioncycle cycle);
}
