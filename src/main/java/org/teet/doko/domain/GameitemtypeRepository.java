package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface GameitemtypeRepository extends CrudRepository<Gameitemtype, Long> {
	Collection<Gameitemtype> findByGame(Game game);
}
