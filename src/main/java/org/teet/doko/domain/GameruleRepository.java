package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface GameruleRepository extends CrudRepository<Gamerule, Long> {

	Collection<Gamerule> findByTitle(String title);
	
	Collection<Gamerule> findByGame(Game game);
}
