package org.teet.doko.domain;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Competition {
    
	private long id;
	private String title;
    private Community community;
    private Date date;
    private Timestamp begin;
    private Timestamp end;
    private Set <Competitionplayer> player;
    private Set <Competitionlocation> location;
    private Set <Competitioncycle> cycle;
    private Set <Competitiongamerule> gamerule;
    private Player owner;
    
    @Id
    @GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@JsonIgnore
	@ManyToOne(optional = false,fetch = FetchType.LAZY)
	@JoinColumn(name = "community_id",nullable=false)
	public Community getCommunity() {
		return community;
	}
	public void setCommunity(Community community) {
		this.community = community;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
    
	@OneToMany(
			mappedBy = "competition",
	        cascade = CascadeType.MERGE, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Competitionplayer> getPlayer() {
		return player;
	}
	public void setPlayer(Set<Competitionplayer> player) {
		this.player = player;
	}
	
	@OneToMany(
			mappedBy = "competition",
	        cascade = CascadeType.MERGE, 
	        orphanRemoval = true,
	        fetch = FetchType.EAGER
	    )
	public Set<Competitionlocation> getLocation() {
		return location;
	}
	public void setLocation(Set<Competitionlocation> location) {
		this.location = location;
	}
	
	
	@OneToMany(
			mappedBy = "competition",
	        cascade = CascadeType.MERGE, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Competitioncycle> getCycle() {
		return cycle;
	}
	public void setCycle(Set<Competitioncycle> cycle) {
		this.cycle = cycle;
	}
	@OneToMany(
			mappedBy = "competition",
	        cascade = CascadeType.REFRESH, 
	        orphanRemoval = true,
	        fetch = FetchType.EAGER
	    )
	public Set<Competitiongamerule> getGamerule() {
		return gamerule;
	}
	public void setGamerule(Set<Competitiongamerule> gamerule) {
		this.gamerule = gamerule;
	}
	
	
	@Override
	public boolean equals(Object ob) {
	   if (ob == null) {
	       return false;
	   }
	   if (this == ob) {
	       return true;
	   }
	   if (ob instanceof Competition) {
		   Competition other = (Competition) ob;
	       return this.id==other.getId();
	   }
	   return false;
	}
	public Timestamp getBegin() {
		return begin;
	}
	public void setBegin(Timestamp begin) {
		this.begin = begin;
	}
	public Timestamp getEnd() {
		return end;
	}
	public void setEnd(Timestamp end) {
		this.end = end;
	}
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "owner_id",nullable=true)
	public Player getOwner() {
		return owner;
	}
	public void setOwner(Player owner) {
		this.owner = owner;
	}
	
	
    
}
