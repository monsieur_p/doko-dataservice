package org.teet.doko.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Communitygamerule {
	private long id;
	private Community community;
	
	Gamerule gamerule;
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@JsonIgnore
	@ManyToOne(optional = false,fetch = FetchType.EAGER)
	@JoinColumn(name = "community_id",nullable=false)
	public Community getCommunity() {
		return community;
	}
	public void setCommunity(Community community) {
		this.community = community;
	}
	@JsonIgnore
	@ManyToOne(optional = false,fetch = FetchType.EAGER)
	@JoinColumn(name = "gamerule_id",nullable=false)
	public Gamerule getGamerule() {
		return gamerule;
	}
	public void setGamerule(Gamerule gamerule) {
		this.gamerule = gamerule;
	}
	
	
}
