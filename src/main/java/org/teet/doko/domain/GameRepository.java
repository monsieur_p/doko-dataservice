package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface GameRepository extends CrudRepository<Game, Long> {

	Collection<Game> findByTitle(String title);
	
}
