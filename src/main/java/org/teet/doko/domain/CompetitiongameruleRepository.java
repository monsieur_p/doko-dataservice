package org.teet.doko.domain;

import org.springframework.data.repository.CrudRepository;

public interface CompetitiongameruleRepository extends CrudRepository<Competitiongamerule, Long> {

}
