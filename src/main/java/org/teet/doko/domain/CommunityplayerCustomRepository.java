package org.teet.doko.domain;

import java.util.List;

public interface CommunityplayerCustomRepository {
	public List<Community> getCommunityByPlayer(Player player);
	public List<Player> getPlayerByCommunity(Community community);
	
	public List<Player> getPlayerBySearchword(String searchText);
	
}
