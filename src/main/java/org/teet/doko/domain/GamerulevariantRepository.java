package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface GamerulevariantRepository  extends CrudRepository<Gamerulevariant, Long>{
	Collection<Gamerulevariant> findByGamerule(Gamerule gamerule);
}
