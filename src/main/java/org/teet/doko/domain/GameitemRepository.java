package org.teet.doko.domain;

import org.springframework.data.repository.CrudRepository;

public interface GameitemRepository extends CrudRepository<Gameitem, Long> {

}
