package org.teet.doko.domain;

import org.springframework.data.repository.CrudRepository;

public interface CompetitioncycleRepository extends CrudRepository<Competitioncycle, Long> {

}
