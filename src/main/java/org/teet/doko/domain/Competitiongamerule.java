package org.teet.doko.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Competitiongamerule {
	private long id;
	private Competition competition;
	private Gamerule gamerule;
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@ManyToOne(optional = false)
	@JoinColumn(name = "competition_id",nullable=false)
	public Competition getCompetition() {
		return competition;
	}
	public void setCompetition(Competition competition) {
		this.competition = competition;
	}
	@ManyToOne(optional = false)
	@JoinColumn(name = "gamerule_id",nullable=false)
	public Gamerule getGamerule() {
		return gamerule;
	}
	public void setGamerule(Gamerule gamerule) {
		this.gamerule = gamerule;
	}
	
}
