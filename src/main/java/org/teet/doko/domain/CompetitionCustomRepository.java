package org.teet.doko.domain;

import java.util.Collection;
import java.util.List;

import org.teet.doko.gateway.v1.SimplePlayercoin;

public interface CompetitionCustomRepository {
	
	
	public List<Playercoin> getPlayercoinByCommunity(long communityId);
	
	public List<Playercoin> getPlayercoinByCompetition(long competitionId);
	
	public List<SimplePlayercoin> getPlayercoinSumByCompetition(long competitionId);
	
	public Collection<Competition> getCompetitionByCommunityId(long communityId);
	
	public List<SimplePlayercoin> getPlayercoinSumByCompetitionAndCycle(long competitionId);
	
}
