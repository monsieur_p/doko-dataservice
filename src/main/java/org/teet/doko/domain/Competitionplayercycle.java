package org.teet.doko.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Competitionplayercycle {
    private long id;
    private Competitionlocationcycle locationcycle ;
    private Player player;
    private int sequence;
    private Set<Playercoin> coin;
      
    @Id
    @GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "competionlocationcycle_id",nullable=false)
	public Competitionlocationcycle getLocationcycle() {
		return locationcycle;
	}

	public void setLocationcycle(Competitionlocationcycle locationcycle) {
		this.locationcycle = locationcycle;
	}

	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "player_id",nullable=false)
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	@JsonIgnore
	@OneToMany(
			mappedBy = "competitionplayercycle",
	        cascade = CascadeType.ALL, 
	        fetch = FetchType.LAZY
	    )
	public Set<Playercoin> getCoin() {
		return coin;
	}

	public void setCoin(Set<Playercoin> coin) {
		this.coin = coin;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	
	

	
	
}
