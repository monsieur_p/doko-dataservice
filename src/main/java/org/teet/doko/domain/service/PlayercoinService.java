package org.teet.doko.domain.service;

import org.teet.doko.domain.Playercoin;

public interface PlayercoinService {
	
	public void add(Playercoin playercoin) throws Exception;
}
