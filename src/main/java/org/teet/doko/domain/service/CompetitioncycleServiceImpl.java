package org.teet.doko.domain.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Competitioncycle;
import org.teet.doko.domain.CompetitioncycleRepository;
import org.teet.doko.gateway.v1.SimpleLocationCycle;
import org.teet.doko.util.PojoUtil;

@Service("competitioncycleService")
public class CompetitioncycleServiceImpl implements CompetitioncycleService {
	
	@Autowired
	CompetitioncycleRepository competitioncycleRepository; 
	
	@Override
	public void add(Competitioncycle competitioncycle) throws Exception {
		competitioncycleRepository.save(competitioncycle);
		
	}

	@Override
	public Set<SimpleLocationCycle> getCompetitionlocationByRoundId(long roundId) {
		Set<SimpleLocationCycle> lcSet=new HashSet<SimpleLocationCycle>();
		Competitioncycle cycle=competitioncycleRepository.findById(roundId).orElse(null);
		if(cycle!=null) {
			lcSet=PojoUtil.toSimpleLocationCycle(cycle.getLocationcycle());
		}
		
		return lcSet;
	}

}
