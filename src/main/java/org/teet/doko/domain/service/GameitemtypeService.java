package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.teet.doko.domain.Gameitemtype;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGameitemtype;


public interface GameitemtypeService {
public Iterable<Gameitemtype> findAll() throws Exception;
	
	public Gameitemtype add(SimpleGameitemtype item) throws StatusException;
	
	 /**
	  * @param id
	  * @return
	  * @throws Exception
	  */
	 public Gameitemtype findById(Long id) throws Exception;
	   
	   
	   /**
	   *
	   * @param gameruleId
	   * @return
	   * @throws Exception
	   */
	  public  Collection<Gameitemtype> findByGameId(long gameruleId) throws Exception;
	  
	
	   /**
	    *
	    * @param name
	    * @return
	    * @throws Exception
	    */
	   public Collection<Gameitemtype> findByCriteria(Map<String, ArrayList<String>> name) throws Exception;
	   
	   public  Collection<SimpleGameitemtype> findSimpleByGameId(long gameruleId) throws Exception;
}
