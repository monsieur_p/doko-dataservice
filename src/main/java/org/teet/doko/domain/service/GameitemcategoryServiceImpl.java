package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Game;
import org.teet.doko.domain.Gameitemcategory;
import org.teet.doko.domain.GameitemcategoryRepository;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGameitemcategory;

@Service("gamerulecategoryService")
public class GameitemcategoryServiceImpl implements GameitemcategoryService {
	
	@Autowired
	private GameitemcategoryRepository gamerulecategoryRepository;
	
	@Autowired
	private GameService gameService;
	
	@Override
	public Iterable<Gameitemcategory> findAll() throws Exception {
		
		return gamerulecategoryRepository.findAll();
	}
	
	@Override
	public Gameitemcategory add(SimpleGameitemcategory simpleCategory) throws StatusException {
		Game game=null;
		try {
			game = gameService.findById(simpleCategory.getGame_id());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new StatusException("Game not requestable",501);
		}
		if(game==null) 
			throw new StatusException("Game is null",502);
		
		Gameitemcategory cat=null;
		if(simpleCategory.getId()!=null) {
			try {
				cat=findById(simpleCategory.getId());
				if(cat==null) {
					throw new StatusException("Gameitemcategory with id "+simpleCategory.getId()+" not found!");
				}
				
			}catch (StatusException e) {
				
				throw new StatusException(e.getMessage(),e.getErrorCode());
			} catch (Exception e) {
				
				throw new StatusException("Gameitemcategory not requestable",501);
			}
		}else {
			cat=new Gameitemcategory();
		}
		cat.setName(simpleCategory.getName());
		cat.setPriority(simpleCategory.getPriority());
		cat.setGame(game);
		gamerulecategoryRepository.save(cat);
			
			
		return cat;
	}
	
	@Override
	public Gameitemcategory findById(Long id) throws Exception {
		
		return gamerulecategoryRepository.findById(id).orElse(null);
	}

	

	@Override
	public Collection<Gameitemcategory> findByCriteria(Map<String, ArrayList<String>> name) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	

}
