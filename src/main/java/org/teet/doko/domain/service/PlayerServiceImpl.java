package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.PlayerRepository;

@Service("playerService")
public class PlayerServiceImpl implements PlayerService {
	
	@Autowired
    private PlayerRepository playerRepository;

	@Override
	public Player findById(Long playerId) throws Exception {
		// 
		return this.playerRepository.findById(playerId).orElse(null);
	}

	@Override
	public Player findByUsername(String username) throws Exception {
		
		return this.playerRepository.findByUsername(username);
	}
	
	@Override
	public Collection<Player> findByCriteria(Map<String, ArrayList<String>> name) throws Exception {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void add(Player player) throws Exception {
		playerRepository.save(player);
		
	}

	@Override
	public Player findByUserid(String userid) throws Exception {
		
		return playerRepository.findByUserid(userid);
	}
	
	
}
