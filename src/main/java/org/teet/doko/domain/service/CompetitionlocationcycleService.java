package org.teet.doko.domain.service;

import java.util.Collection;
import java.util.Set;

import org.teet.doko.domain.Competitioncycle;
import org.teet.doko.domain.Competitionlocationcycle;
import org.teet.doko.exception.PlayerincycleDeleteException;
import org.teet.doko.exception.PlayerincycleExistsException;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleLocationCycleplayer;
import org.teet.doko.gateway.v1.SimpleLocationroundInfo;
import org.teet.doko.gateway.v1.SimpleMatch;
import org.teet.doko.gateway.v1.SimpleMatchRequest;

public interface CompetitionlocationcycleService {
		public SimpleLocationroundInfo getSimpleLocationcycleInfoById(long competitionlocationcycleId);
		public void add(Competitionlocationcycle competitionlocationcycle) throws Exception;
		public Competitionlocationcycle fincdById(long id) throws Exception;
		public void addPlayer2Loctioncycle(Set<SimpleLocationCycleplayer> setPlayer,long competitionlocationcycleId) throws PlayerincycleExistsException,Exception ;
		
		
		public void addMatchForCycle(SimpleMatch match,long locationroundId) throws Exception;
		
		public void getNextMatchForCycle(SimpleMatchRequest matchRequest, String username) throws Exception;
		
		public void deletePlayerFromLocationcycle(long competitionlocationcycleId) throws PlayerincycleDeleteException;
		
		public Collection<Competitionlocationcycle> findByCompetitioncycle(Competitioncycle cycle);
		
		public void deleteMatchByRoundAndId(long matchId,long locationroundId) throws StatusException;

}
