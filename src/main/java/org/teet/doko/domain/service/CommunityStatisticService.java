package org.teet.doko.domain.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleGamestatistic;
import org.teet.doko.gateway.v1.SimplePlayercoin;
import org.teet.doko.gateway.v1.SimplePlayercoingroup;
import org.teet.doko.gateway.v1.SimpleStatistic;

public interface CommunityStatisticService {
	public Set<SimplePlayercoin> getSimplePlayercoinSumByCompetitionId(Long competionId);
	
	public Set<SimplePlayercoin> getSimplePlayercoinByCompetitionId(Long competionId);
	
	public Set<SimplePlayercoin> getSimplePlayercoinByCommunityId(Long communityId);
	
	public SimpleGamestatistic getPlayerstatisticByCompetition(Long competionId);
	
	public List<SimplePlayercoin> getPlayercoinSumByCompetition(Long competionId);
	
	public List<SimplePlayercoingroup> getPlayercoinSumByCompetitionAndCycle(Long competionId);
	
	public SimpleStatistic getPlayercoinStatisticSumByCompetition(Long competionId);
	
	public Collection<SimpleCompetition> getCompetitionByCommunityId(long communityId);
	
	
}
