package org.teet.doko.domain.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Community;
import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.domain.Player;
import org.teet.doko.exception.CommunityplayerAddException;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleMember;
import org.teet.doko.gateway.v1.SimplePlayer;

@Service("validationService")
public class ValidationServiceImpl implements ValidationService {
	@Autowired
	private PlayerService playerService;
	
	@Autowired
	private CommunityService communityService;
	
	@Autowired
	private CommunityplayerService communityplayerService;
	
	@Autowired
	private CompetitionService competitionService;
	
	@Override
	public Communityplayer addSimplePlayer2Community(SimplePlayer authenticatedUser, long communityId)
			throws CommunityplayerAddException {
		
		Player player;
		try {
			player = playerService.findByUserid(authenticatedUser.getUserid());
		} catch (Exception e) {
			throw new CommunityplayerAddException("Player not found",507);
		}
		Community community;
		try {
			community = communityService.findById(communityId);
		} catch (Exception e) {
			throw new CommunityplayerAddException("Community not found",508);
		}
		
		if(player==null) {
			player=new Player();
			player.setFirstname(authenticatedUser.getFirstname());
			player.setLastname(authenticatedUser.getLastname());
			player.setUsername(authenticatedUser.getUsername());
			player.setEmail(authenticatedUser.getEmail());
			player.setUserid(authenticatedUser.getUserid());
			player.setPhone(authenticatedUser.getPhone());
			
			
			try {
				playerService.add(player);
			} catch (Exception e) {
				throw new CommunityplayerAddException("Player could not be added.",509);
			}
		}
		Communityplayer cp=communityplayerService.addPlayer2Community(player,community);
			
		
		
		return cp;
	}

	@Override
	public Communityplayer addMember2Community(SimplePlayer authenticatedUser,SimpleMember member) throws StatusException {
		Community co;
		 try {
				co = communityService.findById(member.getCommunityId());
				isOwner(authenticatedUser,co.getOwner());
					
				
		} catch (Exception e) {
			throw new StatusException("Community with id "+member.getCommunityId()+" not found!",504);
			
		}
		 
		Player player;
		try {
			player = playerService.findByUsername(member.getUsername());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			throw new StatusException("Player with username "+member.getUsername()+" not found!",505);
		}
		
		if(player==null) {
			player=new Player();
			player.setFirstname(member.getFirstname());
			player.setLastname(member.getLastname());
			player.setUsername(member.getUsername());
			try {
				playerService.add(player);
			} catch (Exception e) {
				e.printStackTrace();
				throw new StatusException("Player could not be added!",501);
			}
		}else {
			throw new StatusException("Player with username "+member.getUsername()+" exist!",502);
		}


		Communityplayer cPlayer;
		try {
			cPlayer = communityplayerService.addPlayer2Community(player,co);
		} catch (CommunityplayerAddException e) {
			throw new StatusException("Could not add Player 2 Community with username "+member.getUsername()+" !",506);
		}
			
		return cPlayer;
	}
	
	private boolean isOwner(SimplePlayer authenticatedUser,Player player) throws StatusException  {
		if(player!=null && authenticatedUser!=null) {
			if(player.getUserid().equals(authenticatedUser.getUserid())) {
				return true;
			}
		}
		throw new StatusException("No edit access!",500);
	}
	
	@Override
	public Set<Competitionplayer> addCompetitionplayer(SimplePlayer authenticatedUser, Set<SimpleMember> memberList,
			long competitionId) throws StatusException {
		Set<Competitionplayer> cpLs = new HashSet<Competitionplayer>();
		
		
		
		Competition c;
		try {
			c = competitionService.findById(competitionId);
			
			isOwner(authenticatedUser,c.getOwner());
		} catch (StatusException e1) {
			throw new StatusException(e1.getMessage(),e1.getErrorCode());
		}catch (Exception e2) {
			throw new StatusException("Error finding competion "+competitionId,501);
		}
		
		memberList.forEach(sp -> {
			try {
				cpLs.add(competitionService.addCompetitionplayer(sp, c));
			} catch (Exception e) {
				System.out.println(e.getMessage());
				

			}
		});

		return cpLs;
	}

	@Override
	public Competition addCompetition(SimplePlayer authenticatedUser, SimpleCompetition simpleCompetition)
			throws StatusException {
		try {
			Community co=communityService.findById(simpleCompetition.getCommunityId());
			isOwner(authenticatedUser,co.getOwner());
			//Setzen des Owners
			//TODO Owner m
			Player player = playerService.findByUserid(authenticatedUser.getUserid());
			simpleCompetition.setOwner_id(player.getId());
		} catch (StatusException e1) {
			throw new StatusException(e1.getMessage(),e1.getErrorCode());
		}catch (Exception e2) {
			// TODO Auto-generated catch block
			throw new StatusException("Error finding community "+simpleCompetition.getCommunityId(),501);
		}
		
		
		Competition c=null;
		try {
			c=competitionService.add(simpleCompetition);
		} catch (Exception e) {
			throw new StatusException("Error adding competion ",502);
		}
		return c;
	}

}
