package org.teet.doko.domain.service;

import java.util.Collection;

import org.teet.doko.domain.Competitionlocationcycle;
import org.teet.doko.domain.Competitionplayercycle;

public interface CompetitionplayercycleService {
	public Collection<Competitionplayercycle> findByLocationcycle(Competitionlocationcycle locationcycle);
	public Competitionplayercycle findById(long id);
	public void save(Competitionplayercycle cycle) throws Exception;
	public void delete(Competitionplayercycle cycle) throws Exception;
}
