package org.teet.doko.domain.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Competitionlocationcycle;
import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.domain.Match;
import org.teet.doko.domain.MatchRepository;
import org.teet.doko.exception.StatusException;

@Service("matchService")
public class MatchServiceImpl implements MatchService {
	
	@Autowired
	MatchRepository matchRepository;
	
	@Autowired
	CompetitionlocationcycleService competitionlocationcycleService;
	
	@Override
	public void add(Match match) throws Exception {
		matchRepository.save(match);
	}


	@Override
	public void deleteById(long id) throws Exception {
		Match match= matchRepository.findById(id).orElse(null);
		if(match==null)
			throw new StatusException("Match with id "+id+" not found!",200);
		
		matchRepository.delete(match);
	}
	
	public Collection<Match> findByCompetitionlocationcycleId(long competitionlocationcycleId){
		Competitionlocationcycle competitionlocationcycle;
		try {
			competitionlocationcycle = competitionlocationcycleService.fincdById(competitionlocationcycleId);
			if(competitionlocationcycle!=null) {
				return matchRepository.findByCompetitionlocationcycle(competitionlocationcycle);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Collection<Match> findByCompetitionlocationcycleIdAndGamerulevariant(long competitionlocationcycleId,
			Gamerulevariant gamerulevariant) {
		Competitionlocationcycle competitionlocationcycle;
		try {
			competitionlocationcycle = competitionlocationcycleService.fincdById(competitionlocationcycleId);
			if(competitionlocationcycle!=null) {
				return matchRepository.findByCompetitionlocationcycleAndGamerulevariant(competitionlocationcycle,gamerulevariant);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
