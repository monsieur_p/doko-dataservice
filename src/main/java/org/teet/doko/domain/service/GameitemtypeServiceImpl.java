package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Game;
import org.teet.doko.domain.Gameitemtype;
import org.teet.doko.domain.GameitemtypeRepository;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGameitemtype;

@Service("gameitemtypeService")
public class GameitemtypeServiceImpl implements GameitemtypeService {
	@Autowired
	private GameitemtypeRepository gameitemtypeRepository;
	
	@Autowired
	private GameService gameService;
	
	@Override
	public Iterable<Gameitemtype> findAll() throws Exception {
		
		return gameitemtypeRepository.findAll();
	}

	@Override
	public Gameitemtype add(SimpleGameitemtype item) throws StatusException {
		Gameitemtype itemtype;
		if(item.getId()!=null) {
			itemtype=gameitemtypeRepository.findById(item.getId()).orElse(null);
			if(itemtype==null)
				throw new StatusException("Gameitemtype with id "+item.getId()+" not found!",500);
		}else {
			itemtype=new Gameitemtype();
		}
		try {
			Game game=null;
			if(item.getGame_id()!=null)
				game= gameService.findById(item.getGame_id());
			if(game==null)
				throw new StatusException("Game ",501);
			
			itemtype.setSequence(item.getSequence());
			//TODO ggf. Abgleich ob Spiel identisch, falls nicht neu
			itemtype.setGame(game);
			itemtype.setName(item.getName());
			itemtype.setIconId(item.getIconId());
			gameitemtypeRepository.save(itemtype);
		} catch (StatusException e) {
			// TODO Auto-generated catch block
			throw new StatusException(e.getMessage(),e.getErrorCode());
		}catch (Exception e) {
			
			throw new StatusException(e.getMessage(),502);
		}

		return itemtype;
	}

	@Override
	public Gameitemtype findById(Long id) throws Exception {
		
		return gameitemtypeRepository.findById(id).orElse(null);
	}

	@Override
	public Collection<Gameitemtype> findByGameId(long gameruleId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Gameitemtype> findByCriteria(Map<String, ArrayList<String>> name) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<SimpleGameitemtype> findSimpleByGameId(long gameruleId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
