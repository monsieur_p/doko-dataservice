package org.teet.doko.domain.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Competitionlocationcycle;
import org.teet.doko.domain.Competitionplayercycle;
import org.teet.doko.domain.CompetitionplayercycleRepository;

@Service("competitionplayercycleService")
public class CompetitionplayercycleServiceImpl implements CompetitionplayercycleService{
	
	@Autowired
	CompetitionplayercycleRepository competitionplayercycleRepository;
	
	@Override
	public Collection<Competitionplayercycle> findByLocationcycle(Competitionlocationcycle locationcycle) {
		
		return competitionplayercycleRepository.findByLocationcycle(locationcycle);
	}

	@Override
	public Competitionplayercycle findById(long id) {
		
		return competitionplayercycleRepository.findById(id).orElse(null);
	}

	@Override
	public void save(Competitionplayercycle cycle) throws Exception {
		competitionplayercycleRepository.save(cycle);
		
	}

	@Override
	public void delete(Competitionplayercycle cycle) throws Exception {
		competitionplayercycleRepository.delete(cycle);
		
	}

}
