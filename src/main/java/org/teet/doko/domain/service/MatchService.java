package org.teet.doko.domain.service;

import java.util.Collection;

import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.domain.Match;

public interface MatchService {
	/**
    *
    * @param match
    * @throws Exception
    */
   public void add(Match match) throws Exception;
   
   public void deleteById(long id) throws Exception;
   
   public Collection<Match> findByCompetitionlocationcycleId(long competitionlocationcycleId);
   
   public Collection<Match> findByCompetitionlocationcycleIdAndGamerulevariant(long competitionlocationcycleId,Gamerulevariant gamerulevariant);
}
