package org.teet.doko.domain.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.teet.doko.domain.Matchplayeritem;
import org.teet.doko.domain.MatchplayeritemRepository;
import org.teet.doko.exception.StatusException;


@Service("matchplayeritemService")
public class MatchplayeritemServiceImpl implements MatchplayeritemService{
	
	@Autowired
	MatchplayeritemRepository matchplayeritemRepository;
	
	@Override
	public void add(Matchplayeritem matchplayeritem) throws Exception {
		matchplayeritemRepository.save(matchplayeritem);
		
	}

	@Override
	public void deleteById(long id) throws Exception {
		Matchplayeritem matchplayer= matchplayeritemRepository.findById(id).orElse(null);
		if(matchplayer==null)
			throw new StatusException("Match with id "+id+" not found!",200);
		matchplayeritemRepository.delete(matchplayer);
	}

	@Override
	public Collection<Matchplayeritem> findByMatchId(long matchId) {
		// TODO Auto-generated method stub
		return null;
	}

}
