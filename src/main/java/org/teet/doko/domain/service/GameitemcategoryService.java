package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.teet.doko.domain.Gameitemcategory;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGameitemcategory;

public interface GameitemcategoryService {
	
	public Iterable<Gameitemcategory> findAll() throws Exception;
	
	public Gameitemcategory add(SimpleGameitemcategory simpleCategory) throws StatusException;
	
	 /**
	  * @param id
	  * @return
	  * @throws Exception
	  */
	 public Gameitemcategory findById(Long id) throws Exception;
	   
	   
	   
	  
	
	   /**
	    *
	    * @param name
	    * @return
	    * @throws Exception
	    */
	   public Collection<Gameitemcategory> findByCriteria(Map<String, ArrayList<String>> name) throws Exception;
	   
	   
	   
}
