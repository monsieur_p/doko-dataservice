package org.teet.doko.domain.service;

import java.util.Set;

import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.gateway.v1.SimplePlayer;

public interface CompetitionplayerService {
	/**
    *
    * @param cashdesk
    * @throws Exception
    */
   public void add(Competitionplayer communityplayer) throws Exception;
   
   /**
   *
   * @param cashdesk
   * @throws Exception
   */
  public Competitionplayer add(SimplePlayer player) throws Exception;
	/**
    *
    * @param productorderId
    * @return
    * @throws Exception
    */
   public Competitionplayer findById(long communityplayerId) throws Exception;

   public Set<Competitionplayer> findByCompetitionId(long competitionId) throws Exception;
   
}
