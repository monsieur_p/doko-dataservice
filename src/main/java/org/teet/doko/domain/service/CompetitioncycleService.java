package org.teet.doko.domain.service;

import java.util.Set;

import org.teet.doko.domain.Competitioncycle;
import org.teet.doko.gateway.v1.SimpleLocationCycle;

public interface CompetitioncycleService {
	/**
    *
    * @param cashdesk
    * @throws Exception
    */
   public void add(Competitioncycle competitioncycle) throws Exception;
   
   public Set<SimpleLocationCycle> getCompetitionlocationByRoundId(long roundId);
   
   
   
   


   
   
   

}
