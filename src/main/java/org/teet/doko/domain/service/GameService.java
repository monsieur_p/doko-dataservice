package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.List;

import org.teet.doko.domain.Game;
import org.teet.doko.domain.Gameitem;
import org.teet.doko.domain.Gameitemcategory;
import org.teet.doko.domain.Gameitemtype;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGame;
import org.teet.doko.gateway.v1.SimpleGameitem;
import org.teet.doko.gateway.v1.SimpleGameitemcategory;
import org.teet.doko.gateway.v1.SimpleGameitemtype;
import org.teet.doko.gateway.v1.SimplePlayer;

public interface GameService {
	
	public Iterable<Game> findAll() throws Exception;
	
	public Game findById(long id) throws Exception;
	
	public Gameitem findItemById(long id) throws Exception;
	
	public List<SimpleGame> getAllAsSimpleGame();
	
	public Gameitem addSimpleGameitem2Game(SimpleGameitem gameitem,long gameId,SimplePlayer player)throws StatusException;
	
	public Gameitemcategory addSimpleGameitemcategory2Game(SimpleGameitemcategory category,long gameId,SimplePlayer player)throws StatusException;
	
	public Gameitemtype addSimpleGameitemtype2Game(SimpleGameitemtype gameitemtype,long gameId,SimplePlayer player)throws StatusException;
	
	public void reorderItemsBySimpleItem(ArrayList<SimpleGameitem> gameitem,long gameId,SimplePlayer player)throws StatusException;
	
	public void deleteGameitem(long itemId,long gameId,SimplePlayer player)throws StatusException;

}
