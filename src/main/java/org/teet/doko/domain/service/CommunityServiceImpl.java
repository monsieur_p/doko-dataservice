package org.teet.doko.domain.service;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Community;
import org.teet.doko.domain.CommunityRepository;
import org.teet.doko.domain.Communitygamerule;
import org.teet.doko.domain.CommunitygameruleRepository;
import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.CommunityplayerCustomRepository;
import org.teet.doko.domain.Game;
import org.teet.doko.domain.GameRepository;
import org.teet.doko.domain.Gamerule;
import org.teet.doko.domain.GameruleRepository;
import org.teet.doko.domain.Player;
import org.teet.doko.exception.PlayerExistsInCommunityException;
import org.teet.doko.gateway.v1.SimpleCommunity;
import org.teet.doko.gateway.v1.SimpleCommunityInfo;
import org.teet.doko.gateway.v1.SimpleGamerule;
import org.teet.doko.gateway.v1.SimpleGameruleitem;
import org.teet.doko.gateway.v1.SimplePlayer;
import org.teet.doko.gateway.v1.SimpleStatusResponse;
import org.teet.doko.util.PojoUtil;

@Service("communityService")
public class CommunityServiceImpl implements CommunityService {
	
	@Autowired
	CommunityplayerCustomRepository communityplayerCustomRepository;
	
	@Autowired
    private CommunityRepository communityRepository;

	@Autowired
    private GameRepository gameRepository;
	
	@Autowired
    private GameruleRepository gameruleRepository;
	
	@Autowired
	private CommunitygameruleRepository communitygameruleRepository;
	
	@Autowired
    private GameruleService gameruleService;
	
	@Autowired
    private PlayerService playerService;
	
	@Autowired
	CommunityplayerService communityplayerService;
	
	@Autowired
    private GameruleitemService gameruleitemService;
	
	@Override
	public Community findById(Long communityId) throws Exception {
		// 
		return this.communityRepository.findById(communityId).orElse(null);
	}

	@Override
	public Community findByTitle(String title) throws Exception {
		Community c=this.communityRepository.findByTitle(title);
		if(c!=null) {
			c.getCompetition();
			c.getOwner();
			c.getMember();
			c.getRule();
		}
		
		return c;
	}
	
	

	@Override
	public void add(Community community) throws Exception {
		communityRepository.save(community);
		
	}

	@Override
	public Iterable<Community> findAll() throws Exception {

		return communityRepository.findAll();
	}

	@Override
	public Community add(SimpleCommunity simpleCommunity) throws Exception {
		if(simpleCommunity.getOwnwer()!=null) {
			Player player=playerService.findByUsername(simpleCommunity.getOwnwer());
			if(player==null) {
				//Vornamen künstlich erzeugen
				String fistname=simpleCommunity.getOwnwer();
				Player p=new Player();
				int hasI=simpleCommunity.getOwnwer().indexOf("@");
				if(hasI>0)fistname=simpleCommunity.getOwnwer().substring(0,hasI);
				
				p.setFirstname(fistname);
				p.setUsername(simpleCommunity.getOwnwer());
				playerService.add(p);
				player=p;
			}
			
			Community c=new Community();
			if(simpleCommunity.getId()!=null) {
				c.setId(simpleCommunity.getId());
				Community cCheck=communityRepository.findById(simpleCommunity.getId()).orElse(null);
				if(cCheck!=null && cCheck.getOwner()!=null && cCheck.getOwner().getId() != player.getId()) {
					throw new Exception("Owner not vaild to Edit");
				}
			}
			c.setTitle(simpleCommunity.getTitle());
			c.setDescription(simpleCommunity.getDescription());
			c.setOwner(player);
			communityRepository.save(c);
			
			//this.addPlayerToCommunity(player, c);
			
			return c;
		}else {
			throw new Exception("Owner not set");
		}
		
		
	}

	@Override
	public SimpleCommunityInfo findSimpleinfoById(long id) throws Exception {
		SimpleCommunityInfo info=new SimpleCommunityInfo();;
		Optional<Community> co=this.communityRepository.findById(id);
		if(co!=null) {
			Community c=co.get();
			info.setId(c.getId());
			info.setTitle(c.getTitle());
			if(c.getOwner()!=null) {
				info.setOwner(c.getOwner().getFirstname()+" "+c.getOwner().getLastname());
				info.setOwner_id(c.getOwner().getId());
			}
			info.setCompetition(PojoUtil.toSimpleCompetition(c.getCompetition()));
			info.setMember(PojoUtil.toSimpleMember(c.getMember()));
			info.setRule(PojoUtil.toSimpleRuleByCommunityrule(c.getRule()));
		}
		return info;
	}

	@Override
	public Communitygamerule addRuleForCommunity(SimpleGamerule game, long communityId,SimplePlayer player) throws Exception {
		
		Optional<Community> co=this.communityRepository.findById(communityId);
		if(co!=null) {
			Game g=null;
			if(game.getGameId()==null) {
				Collection<Game> gCol=gameRepository.findByTitle(game.getGameTitle());
				if(gCol.isEmpty()) {
					g =new Game();
					g.setTitle(game.getGameTitle());
					
					gameRepository.save(g);
				}else {
					throw new Exception("Game Exist");
				}
			}else {
				g=gameRepository.findById(game.getGameId()).orElse(null);
			}
			if(g==null)
				throw new Exception("Game Exist");
			
			Gamerule gr=null;
			if(game.getId()==null) {
				gr=new Gamerule();
				gr.setGame(g);
				gr.setTitle(game.getTitle());
				gr.setPlayermax(game.getPlayermax());
				gr.setPlayermin(game.getPlayermin());
				gr.setOwner(playerService.findByUserid(player.getUserid()));
				gameruleRepository.save(gr);
				if(game.getItem()!=null) {
					for(SimpleGameruleitem item:game.getItem()) {
						item.setGamerule_id(gr.getId());
						gameruleitemService.add(item, player);
					}
				}
				
			}else {
				gr=gameruleRepository.findById(game.getId()).orElse(null);
			}
			
			if(gr==null) {
				throw new Exception("Gamerule invalid");
			}
			
			Communitygamerule cr=new Communitygamerule();
			cr.setGamerule(gr);
			cr.setCommunity(co.get());
			
			communitygameruleRepository.save(cr);
			
			return cr;
		}
		throw new Exception("Community invalid");
	}
	
	public SimpleGamerule getSimpleGameruleByCommunitygameruleId(long communitygameruleId) {
		Communitygamerule cgr=communitygameruleRepository.findById(communitygameruleId).orElse(null);
		if(cgr!=null) {
			try {
				return gameruleService.getSimpleGameruleById(cgr.getGamerule().getId());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return new SimpleGamerule();
	}

	@Override
	public Collection<SimplePlayer> findBySearchtext(String searchtext) {
		Collection<Player> pList= communityplayerCustomRepository.getPlayerBySearchword(searchtext);
		
		return PojoUtil.toSimplePlayerFromPlayer(pList);
	}
	
	public SimpleStatusResponse addPlayerToCommunity(SimplePlayer player,long communityId) {
		SimpleStatusResponse resp=new SimpleStatusResponse();
		try {
			Player playerFound=playerService.findById(player.getId());
			Community com1=communityRepository.findById(communityId).orElse(null);
			
			addPlayerToCommunity(playerFound,com1);
			resp.setStatus(200);
		}catch (PlayerExistsInCommunityException e) {
			resp.setStatus(e.getErrorCode());
			resp.setMessage(e.getMessage());
		}catch (Exception e) {
			resp.setStatus(503);
			resp.setMessage("Player with id "+player.getId()+" not found!");
		}
		
		return resp;
	}
	
	private void addPlayerToCommunity(Player player,Community com1) throws PlayerExistsInCommunityException,Exception {
		
		if(player!=null && com1!=null) {
			Set<Communityplayer> cmLs=com1.getMember();
			boolean isMember=false;
			if(cmLs!=null) {
				for(Communityplayer cm:cmLs) {
					if(cm.getPlayer().getId()==player.getId()) {
						isMember=true;
						break;
					}
				}
			}
			if(!isMember) {
				Communityplayer cp1=new Communityplayer();
				cp1.setCommunity(com1);
				cp1.setPlayer(player);
				communityplayerService.add(cp1,null);
				
			}else {
				throw new PlayerExistsInCommunityException("Player "+player.getId()+" exists in Community",501);
			}
				
		}else {
			throw new Exception("addPlayerToCommunity Community or Player does not exists");
		}
		
	}
	
	
}
