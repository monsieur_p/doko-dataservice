package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.teet.doko.domain.Gameruleitem;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGameruleitem;
import org.teet.doko.gateway.v1.SimplePlayer;

public interface GameruleitemService {
public Iterable<Gameruleitem> findAll() throws Exception;
	
	public Gameruleitem add(SimpleGameruleitem simpleGamerulitem,SimplePlayer player) throws StatusException;
	
	 /**
	  * @param id
	  * @return
	  * @throws Exception
	  */
	 public Gameruleitem findById(Long id) throws Exception;
	   
	   
	   /**
	   *
	   * @param gameruleId
	   * @return
	   * @throws Exception
	   */
	  public  Collection<Gameruleitem> findByGameruleId(long gameruleId) throws Exception;
	  
	
	   /**
	    *
	    * @param name
	    * @return
	    * @throws Exception
	    */
	   public Collection<Gameruleitem> findByCriteria(Map<String, ArrayList<String>> name) throws Exception;
	   
	   public SimpleGameruleitem findSimpleGameruleitemById(long id) throws Exception;
}
