package org.teet.doko.domain.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitionlocation;
import org.teet.doko.domain.CompetitionlocationRepository;
import org.teet.doko.gateway.v1.SimpleLocation;

@Service("competitionlocationService")
public class CompetitionlocationServiceImpl implements CompetitionlocationService {
	@Autowired
	CompetitionlocationRepository competitionlocationRepository;
	
	@Autowired
	CompetitionService competitionService;
	
	@Override
	public Iterable<Competitionlocation> findAll() throws Exception {
		
		return competitionlocationRepository.findAll();
	}
	
	@Override
	public Competitionlocation findByNameAndCompetition(String name, Competition competition) throws Exception {
		if( name==null && competition==null)
			return null;
		return competitionlocationRepository.findByNameAndCompetition(name,competition);
	}
	
	@Override
	public Collection<Competitionlocation> findByCompetition( Competition competition) throws Exception {
		if( competition==null)
			return null;
		return competitionlocationRepository.findByCompetition(competition);
	}
	
	@Override
	public void add(Competitionlocation competitionlocation) throws Exception {
		competitionlocationRepository.save(competitionlocation);
		
	}

	@Override
	public Competitionlocation add(SimpleLocation simpleLocation) throws Exception {
		Competition c=competitionService.findById(simpleLocation.getCompetitionId());
		if(c==null) 
			throw new Exception("Competition with id "+simpleLocation.getCompetitionId()+" does not exist!");
		
		Competitionlocation c0=findByNameAndCompetition(simpleLocation.getName(),c);
		
		if(c0!=null)
			throw new Exception("Competitionlocation with name "+simpleLocation.getName()+" exist!");
		
		
		Competitionlocation cl=new Competitionlocation();
		cl.setName(simpleLocation.getName());
		cl.setCompetition(c);
		add(cl);
		
		return cl;
		
	}

	@Override
	public Competitionlocation findById(Long competitionlocationId) throws Exception {
		
		return competitionlocationRepository.findById(competitionlocationId).orElse(null);
	}

}
