package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Game;
import org.teet.doko.domain.GameRepository;
import org.teet.doko.domain.Gameitem;
import org.teet.doko.domain.GameitemRepository;
import org.teet.doko.domain.Gameitemcategory;
import org.teet.doko.domain.Gameitemtype;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGame;
import org.teet.doko.gateway.v1.SimpleGameitem;
import org.teet.doko.gateway.v1.SimpleGameitemcategory;
import org.teet.doko.gateway.v1.SimpleGameitemtype;
import org.teet.doko.gateway.v1.SimplePlayer;
import org.teet.doko.util.PojoUtil;

@Service("gameService")
public class GameServiceImpl implements GameService {
	@Autowired
    private GameRepository gameRepository;
	
	
	@Autowired
    private GameitemRepository gameitemRepository;
	
	@Autowired
    private GameitemtypeService gameitemtypeService;
	
	@Autowired
    private GameitemcategoryService gameitemcategoryService;
	
	@Override
	public Iterable<Game> findAll() throws Exception {
		return gameRepository.findAll();
	}
	
	@Override
	public List<SimpleGame> getAllAsSimpleGame(){
		List<SimpleGame> gameList=new ArrayList<SimpleGame>();
		
		List<Game> gameLs=new ArrayList<Game>();
		
		try {
			findAll().forEach(gameLs::add);
			gameList=PojoUtil.toSimpleGameList(gameLs);
			/*
			findAll().forEach(item->{
				SimpleGame g1=new SimpleGame();
				g1.setId(item.getId());
				g1.setTitle(item.getTitle());
				if(item.getOwner()!=null) {
					g1.setOwner_id(item.getOwner().getId());
				}
				g1.setItem(PojoUtil.toSimpleGameitemList(item.getItem()));
				gameList.add(g1);
			});*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return gameList;
		
		
	}

	@Override
	public Game findById(long id) throws Exception {
		Game game =gameRepository.findById(id).orElse(null);
		if(game!=null)
			game.getOwner();
		return game; 
	}
	
	@Override
	public Gameitem findItemById(long id) throws Exception {
		Gameitem gameitem =gameitemRepository.findById(id).orElse(null);
		if(gameitem!=null)
			gameitem.getItemtype();
		return gameitem; 
	}
	
	private Game findByIdChecked(long id,SimplePlayer player)throws StatusException {
		Game game;
		try {
			game = findById(id);

			if(game==null)
				throw new StatusException("Game not found",501);
			
			if(game.getOwner()==null|| player==null) {
				String msg =player==null ? "no user authenticated" : "rootgame";
				throw new StatusException("Editing not allowed "+msg,502);
			}else if(!game.getOwner().getUserid().equals(player.getUserid())){
				throw new StatusException("User not valid "+player.getUserid(),503);
			}
			return game;
		}catch (StatusException e) {
			// TODO Auto-generated catch block
			throw new StatusException(e.getMessage(),e.getErrorCode());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new StatusException("Error ask game "+id+" not found "+e.getMessage(),504);
		}
	}
	
	@Override
	public Gameitemcategory addSimpleGameitemcategory2Game(SimpleGameitemcategory category, long gameId,
			SimplePlayer player) throws StatusException {
		//Ob Berechtigung besteht das Spiel zu editieren
		Game game = findByIdChecked(gameId,player);
		
		
		return gameitemcategoryService.add(category);
	}
	
	@Override
	public Gameitem addSimpleGameitem2Game(SimpleGameitem gameitem, long gameId, SimplePlayer player) throws StatusException {
		Game game = findByIdChecked(gameId,player);
		
		Gameitem item=new Gameitem();
		
		if(gameitem.getId()!=null) {
			item=gameitemRepository.findById(gameitem.getId()).orElse(null);
			if(item==null)
				throw new StatusException("Item does not exist",505);
		}
		
			try {
				item.setItemtype(gameitemtypeService.findById(gameitem.getType().getId()));
			} catch (Exception e) {
				throw new StatusException("Gameitemtype  not found",506);
			}
		
		item.setPoints(gameitem.getPoints());
		item.setGame(game);
		item.setSequence(gameitem.getSequence());
		item.setCount(gameitem.getCount());
		item.setName(gameitem.getName());
		item.setIconId(gameitem.getIconId());
		item.setImageId(gameitem.getImageId());
		gameitemRepository.save(item);
		
		return item;
	}
	
	@Override
	public Gameitemtype addSimpleGameitemtype2Game(SimpleGameitemtype gameitemtype, long gameId, SimplePlayer player)
			throws StatusException {
		Game game = findByIdChecked(gameId,player);
		Gameitemtype itemtype=null;
		if(game.getId()==gameitemtype.getGame_id()) {
			itemtype=gameitemtypeService.add(gameitemtype);
		}else {
			throw new StatusException("Game is not valid ",504);
		}
		
		return itemtype;
	}

	@Override
	public void reorderItemsBySimpleItem(ArrayList<SimpleGameitem> gameitem, long gameId, SimplePlayer player)
			throws StatusException {
		findByIdChecked(gameId,player);
		if(gameitem!=null) {
			for(SimpleGameitem g : gameitem) {
				//if(g.getGame_id()==game.getId()) {
					Gameitem i=gameitemRepository.findById(g.getId()).orElse(null);
					if(i!=null) {
						if(i.getGame().getId()==gameId) {
							i.setSequence(g.getSequence());
							gameitemRepository.save(i);
						}
					}
				//}
				
			}
		}
		
	}

	@Override
	public void deleteGameitem(long itemId, long gameId, SimplePlayer player) throws StatusException {
		Game game = findByIdChecked(gameId,player);
		Gameitem gi=gameitemRepository.findById(itemId).orElse(null);
		if(gi!=null && gameId==game.getId()) {
			try {
			gameitemRepository.delete(gi);
			}catch(Exception e) {
				throw new StatusException("Delete item failed "+e.getMessage(),505);
			}
		}else {
			throw new StatusException("Item to delelet is not item from game",506);
		}
		
		
	}

	

	
}
