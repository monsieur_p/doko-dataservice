package org.teet.doko.domain.service;

import java.util.Collection;

import org.teet.doko.domain.Matchplayeritem;

public interface MatchplayeritemService {
	/**
    *
    * @param match
    * @throws Exception
    */
   public void add(Matchplayeritem matchplayeritem) throws Exception;
   
   public void deleteById(long id) throws Exception;
   
   public Collection<Matchplayeritem> findByMatchId(long matchId);
   
   
}
