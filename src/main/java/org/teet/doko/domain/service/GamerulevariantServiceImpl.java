package org.teet.doko.domain.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Gamerule;
import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.domain.GamerulevariantRepository;
@Service("gamerulevariantService")
public class GamerulevariantServiceImpl implements GamerulevariantService{
	@Autowired
	GamerulevariantRepository gamerulevariantRepository; 
	
	@Autowired
	GameruleService gameruleService; 
	
	@Override
	public Collection<Gamerulevariant> getGamerulevariantByGameId(long id) {
		Gamerule gamerule;
		try {
			gamerule = gameruleService.findById(id);
			return gamerulevariantRepository.findByGamerule(gamerule);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}

	@Override
	public Gamerulevariant findById(long id) throws Exception {
		return gamerulevariantRepository.findById(id).orElse(null);
	}

}
