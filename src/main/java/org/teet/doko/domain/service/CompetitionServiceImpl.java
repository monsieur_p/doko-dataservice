package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Community;
import org.teet.doko.domain.CommunityRepository;
import org.teet.doko.domain.Communitygamerule;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.CompetitionRepository;
import org.teet.doko.domain.Competitioncycle;
import org.teet.doko.domain.Competitiongamerule;
import org.teet.doko.domain.CompetitiongameruleRepository;
import org.teet.doko.domain.Competitionlocation;
import org.teet.doko.domain.Competitionlocationcycle;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.domain.CompetitionplayerRepository;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.PlayerRepository;
import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleCompetitionInfo;
import org.teet.doko.gateway.v1.SimpleGamerule;
import org.teet.doko.gateway.v1.SimpleMember;
import org.teet.doko.util.PojoUtil;

@Service("competitionService")
public class CompetitionServiceImpl implements CompetitionService {

	@Autowired
	private CommunityRepository communityRepository;

	@Autowired
	private CompetitionRepository competitionRepository;
	@Autowired
	CompetitionService competitionService;

	//@Autowired
	//private CompetitioncycleRepository competitioncycleRepository;
	
	@Autowired
	private CompetitioncycleService competitioncycleService;

	
	@Autowired
	private CompetitionlocationcycleService competitionlocationcycleService ;
	
	@Autowired
	private CompetitionplayerRepository competitionplayerRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private CompetitiongameruleRepository competitiongameruleRepository;

	@Autowired
	private CompetitionlocationService competitionlocationService;
	
	@Override
	public Competition findById(Long competitionId) throws Exception {
		//
		return this.competitionRepository.findById(competitionId).orElse(null);
	}

	@Override
	public Collection<Competition> findByCommunity(Community community) throws Exception {

		return this.competitionRepository.findByCommunity(community);
	}

	@Override
	public Collection<Competition> findByCriteria(Map<String, ArrayList<String>> name) throws Exception {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void add(Competition competition) throws Exception {
		Community co = competition.getCommunity();

		Optional<Community> optinalEntity = communityRepository.findById(co.getId());
		competition.setCommunity(optinalEntity.get());

		competitionRepository.save(competition);

	}

	@Override
	public Iterable<Competition> findAll() throws Exception {

		return competitionRepository.findAll();
	}

	@Override
	public Collection<Competition> findByCommunityId(long id) throws Exception {
		Optional<Community> optinalEntity = communityRepository.findById(id);

		return this.findByCommunity(optinalEntity.get());
	}

	@Override
	public Competition add(SimpleCompetition simpleCompetition) throws Exception {
		Optional<Community> optinalEntity = communityRepository.findById(simpleCompetition.getCommunityId());
		if (optinalEntity != null) {
			Community co = optinalEntity.get();
			Competition c = new Competition();
			c.setDate(simpleCompetition.getDate());
			c.setCommunity(co);
			c.setTitle(simpleCompetition.getTitle());
			
			if(simpleCompetition.getOwner_id()!=null) {
				c.setOwner(playerRepository.findById(simpleCompetition.getOwner_id()).orElse(null));
			}

			competitionRepository.save(c);

			Set<Communitygamerule> sCgr = co.getRule();

			Collection<SimpleGamerule> srLs = simpleCompetition.getRule();

			Collection<Communitygamerule> grSel = new ArrayList<Communitygamerule>();
			if (srLs != null) {
				for (SimpleGamerule sgr : srLs) {
					for (Communitygamerule tg : sCgr) {
						if (tg.getId() == sgr.getId()) {
							grSel.add(tg);
						}
					}
				}
			}
			if (grSel != null && !grSel.isEmpty()) {
				for (Communitygamerule sgr : grSel) {
					Competitiongamerule ncgr = new Competitiongamerule();
					ncgr.setCompetition(c);
					ncgr.setGamerule(sgr.getGamerule());
					competitiongameruleRepository.save(ncgr);
				}
			}
			/*
			 * //TODO so lange keine AUswahl dar ist wird der erste Eintrag aus
			 * Communityregeln genommen
			 * 
			 * if(sCgr!=null && !sCgr.isEmpty()) { Communitygamerule
			 * cgr=sCgr.iterator().next(); Competitiongamerule ncgr=new
			 * Competitiongamerule(); ncgr.setCompetition(c);
			 * ncgr.setGamerule(cgr.getGamerule());
			 * competitiongameruleRepository.save(ncgr); }
			 */

			return c;
		} else {
			throw new Exception("Community NOT FOUND");
		}

	}

	@Override
	public SimpleCompetitionInfo findSimpleinfoById(long communityId) throws Exception {
		SimpleCompetitionInfo info = new SimpleCompetitionInfo();
		Optional<Competition> optinalEntity = competitionRepository.findById(communityId);
		if (optinalEntity != null) {
			Competition c = optinalEntity.get();
			info.setId(c.getId());
			info.setTitle(c.getTitle());
			if(c.getOwner()!=null)
				info.setOwner_id(c.getOwner().getId());
			info.setBegin(c.getBegin());
			info.setEnd(c.getEnd());
			info.setLocation(PojoUtil.toSimpleLocation(c.getLocation()));
			info.setCycle(PojoUtil.toSimpleCycle(c.getCycle()));
			info.setPlayer(PojoUtil.toSimplePlayer(c.getPlayer()));
			info.setRule(PojoUtil.toSimpleRuleByCommpetitionrule(c.getGamerule()));

		}
		return info;
	}

	@Override
	public Competitioncycle addCompetitioncycleForAllLocation(long competitionId) {
		Competitioncycle nCc = null;
		// SimpleCycle sCc=null;
		Competition c;
		try {
			c = findById(competitionId);

			if (c != null) {
				Set<Competitioncycle> cC = c.getCycle();
				Collection<Competitionlocation> cL = competitionlocationService.findByCompetition(c);
				int cycleSize = 0;
				if (cC != null) {
					cycleSize = cC.size();
				}
				if (cL != null) {
					int roundSeq = cycleSize + 1;

					Competitioncycle tCc = new Competitioncycle();
					tCc.setTitle("Runde " + roundSeq);
					tCc.setSequence(roundSeq);
					tCc.setCompetition(c);

					competitioncycleService.add(tCc);
					//System.out.println("ADD CYCLE "+tCc.getId());
					//cL.forEach(l -> {
					for(Competitionlocation l : cL) {
						Competitionlocationcycle cy = new Competitionlocationcycle();
						cy.setCycle(tCc);

						cy.setLocation(l);
						competitionlocationcycleService.add(cy);
						//System.out.println("ADD LOCATIONCYCLE "+cy.getId());
					}
						//});
					nCc = tCc;
					/*
					 * sCc=new SimpleCycle(); sCc.setCompetitionId(competitionId);
					 * sCc.setTitle(nCc.getTitle()); sCc.setSequence(nCc.getSequence());
					 */
				} else {
					System.out.println("NO LOCATIONS");
				}

			}else {
				System.out.println("NO COMPETITION");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nCc;
	}

	@Override
	public Set<Competitionplayer> addCompetitionplayer(Set<SimpleMember> memberList, long competitionId) {
		Set<Competitionplayer> cpLs = new HashSet<Competitionplayer>();

		Competition c = competitionRepository.findById(competitionId).orElse(null);

		memberList.forEach(sp -> {
			try {
				cpLs.add(addCompetitionplayer(sp, c));
			} catch (Exception e) {
				System.out.println(e.getMessage());

			}
		});

		return cpLs;
	}

	@Override
	public Competitionplayer addCompetitionplayer(SimpleMember player, Competition competition) throws Exception {
		Competitionplayer cp = new Competitionplayer();

		Player pl = playerRepository.findByUsername(player.getUsername());

		cp.setCompetition(competition);
		cp.setPlayer(pl);
		Competitionplayer cpl = competitionplayerRepository.findByCompetitionAndPlayer(competition, pl);
		if (cpl == null) {
			competitionplayerRepository.save(cp);
			return cp;
		} else {
			throw new Exception("Competitionplayer exist!");
		}

	}

}
