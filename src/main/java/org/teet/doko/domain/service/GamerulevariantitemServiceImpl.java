package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Gamerulevariantitem;
import org.teet.doko.domain.GamerulevariantitemRepository;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGamerulevariantitem;
@Service("gamerulevariantitemService")
public class GamerulevariantitemServiceImpl implements GamerulevariantitemService{
	
	@Autowired
	private GamerulevariantitemRepository gamerulevariantitemRepository;
	
	
	@Autowired
	private GamerulevariantService gamerulevariantService;
	
	@Autowired
	private GameitemcategoryService gameitemcategoryService;
	
	@Autowired
	private GameruleitemService gameruleitemService;
	
	@Override
	public Gamerulevariantitem add(SimpleGamerulevariantitem vItem) throws Exception {
		//Gamerule gamerule = gameruleService.findById();
		
		Gamerulevariantitem item=new Gamerulevariantitem();
		
		if(vItem.getId()!=null) {
			item=gamerulevariantitemRepository.findById(vItem.getId()).orElse(null);
			if(item==null)
				throw new StatusException("Item does not exist",505);
		}
		
		try {
			
			item.setGamerulevariant(gamerulevariantService.findById(vItem.getGamerulevariant_id()));
		} catch (Exception e) {
			throw new StatusException("Gameitemtype  not found",506);
		}
		
		try {
			item.setItem(gameruleitemService.findById(vItem.getItem_id()));
		} catch (Exception e) {
			throw new StatusException("Gameruleitem with id "+vItem.getItem_id()+" not found",507);
		}
		
		item.setCategory(gameitemcategoryService.findById(vItem.getCategory_id()));
		
		item.setPoints(vItem.getPoints());
		
		
		item.setSequence(vItem.getSequence());
		
		
		
		gamerulevariantitemRepository.save(item);
		
		return item;
	}

	@Override
	public Gamerulevariantitem findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Gamerulevariantitem> findByGameruleId(long gameruleId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Gamerulevariantitem> findByCriteria(Map<String, ArrayList<String>> name) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SimpleGamerulevariantitem findSimpleGamerulevariantitemById(long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Gamerulevariantitem> findAll() throws Exception {
		
		return gamerulevariantitemRepository.findAll();
	}
	
	
}
