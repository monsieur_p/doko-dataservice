package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Gamerule;
import org.teet.doko.domain.Gameruleitem;
import org.teet.doko.domain.GameruleitemRepository;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGameruleitem;
import org.teet.doko.gateway.v1.SimplePlayer;
import org.teet.doko.util.PojoUtil;

@Service("gameruleitemService")
public class GameruleitemServiceImpl implements GameruleitemService{
	
	@Autowired
	private GameruleitemRepository gameruleitemRepository;
	
	@Autowired
	private GameService gameService;
	
	@Autowired
	private GameruleService gameruleService;
	
	@Override
	public Iterable<Gameruleitem> findAll() throws Exception {
		
		return gameruleitemRepository.findAll();
	}

	@Override
	public Gameruleitem add(SimpleGameruleitem simpleGamerulitem,SimplePlayer player) throws StatusException {
		Gamerule gamerule = gameruleService.findByIdChecked(simpleGamerulitem.getGamerule_id(),player);
		
		Gameruleitem item=new Gameruleitem();
		
		if(simpleGamerulitem.getId()!=null) {
			item=gameruleitemRepository.findById(simpleGamerulitem.getId()).orElse(null);
			if(item==null)
				throw new StatusException("Item does not exist",505);
		}
		
			try {
				item.setGameitem(gameService.findItemById(simpleGamerulitem.getGameitem_id()));
			} catch (Exception e) {
				throw new StatusException("Gameitemtype  not found",506);
			}
		
		item.setPoints(simpleGamerulitem.getPoints());
		try {
			item.setGamerule(gameruleService.findById(simpleGamerulitem.getGamerule_id()));
		} catch (Exception e) {
			throw new StatusException("gamerule not found",502);
		}
		
		item.setSequence(simpleGamerulitem.getSequence());
		item.setCount(simpleGamerulitem.getCount());
		item.setName(simpleGamerulitem.getName());
		item.setGamerule(gamerule);
		item.setPoints(simpleGamerulitem.getPoints());
		gameruleitemRepository.save(item);
		
		return item;
	}

	@Override
	public Gameruleitem findById(Long id) throws Exception {
		
		return gameruleitemRepository.findById(id).orElse(null);
	}

	@Override
	public Collection<Gameruleitem> findByGameruleId(long gameruleId) throws Exception {
		
		return gameruleitemRepository.findByGamerule(gameruleService.findById(gameruleId));
	}

	@Override
	public Collection<Gameruleitem> findByCriteria(Map<String, ArrayList<String>> name) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SimpleGameruleitem findSimpleGameruleitemById(long id) throws Exception {
		Gameruleitem item=gameruleitemRepository.findById(id).orElse(null);
		
		return PojoUtil.toSimpleGameruleitem(item);
	}

}
