package org.teet.doko.domain.service;

import java.util.List;

import org.teet.doko.domain.Community;
import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.Player;
import org.teet.doko.exception.CommunityplayerAddException;
import org.teet.doko.gateway.v1.SimpleMember;
import org.teet.doko.gateway.v1.SimplePlayer;


public interface CommunityplayerService {
	
	/**
    *
    * @param cashdesk
    * @throws Exception
    */
   public void add(Communityplayer communityplayer,SimplePlayer authenticatedUser) throws Exception;
   
   /**
   *
   * @param cashdesk
   * @throws Exception
   */
  public Communityplayer add(SimpleMember member,SimplePlayer authenticatedUser) throws Exception;
  
  
  //public Communityplayer addSimplePlayer2Community(SimplePlayer player,long communityId) throws CommunityplayerAddException;
  public Communityplayer addPlayer2Community(Player player, Community community) throws CommunityplayerAddException;
  /**
    *
    * @param productorderId
    * @return
    * @throws Exception
    */
   public List<Community> findByPlayer(Player player) throws Exception;
   
   /**
    * Setzt ein Communitymitglied aktiv
    * @param member
    * @throws Exception
    */
   public Communityplayer editMember(SimpleMember member,SimplePlayer authenticatedUser) throws Exception; 
   
}
