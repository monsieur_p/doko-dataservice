package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.teet.doko.domain.Gameitemcategory;
import org.teet.doko.domain.Gamerulevariantitem;
import org.teet.doko.gateway.v1.SimpleGamerulevariantitem;

public interface GamerulevariantitemService {
	public Iterable<Gamerulevariantitem> findAll() throws Exception;
	
	public Gamerulevariantitem add(SimpleGamerulevariantitem simpleCategory) throws Exception;
	
	 /**
	  * @param id
	  * @return
	  * @throws Exception
	  */
	 public Gamerulevariantitem findById(Long id) throws Exception;
	   
	   
	   /**
	   *
	   * @param gameruleId
	   * @return
	   * @throws Exception
	   */
	  public  Collection<Gamerulevariantitem> findByGameruleId(long gameruleId) throws Exception;
	  
	
	   /**
	    *
	    * @param name
	    * @return
	    * @throws Exception
	    */
	   public Collection<Gamerulevariantitem> findByCriteria(Map<String, ArrayList<String>> name) throws Exception;
	   
	   public SimpleGamerulevariantitem findSimpleGamerulevariantitemById(long id) throws Exception;
}
