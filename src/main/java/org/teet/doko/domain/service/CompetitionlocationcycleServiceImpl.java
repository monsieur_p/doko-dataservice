package org.teet.doko.domain.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Competitioncycle;
import org.teet.doko.domain.Competitiongamerule;
import org.teet.doko.domain.Competitionlocationcycle;
import org.teet.doko.domain.CompetitionlocationcycleRepository;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.domain.Competitionplayercycle;
import org.teet.doko.domain.Gameruleitem;
import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.domain.Match;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.Playercoin;
import org.teet.doko.exception.PlayerincycleDeleteException;
import org.teet.doko.exception.PlayerincycleExistsException;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleLocationCycleplayer;
import org.teet.doko.gateway.v1.SimpleLocationroundInfo;
import org.teet.doko.gateway.v1.SimpleMatch;
import org.teet.doko.gateway.v1.SimpleMatchRequest;
import org.teet.doko.util.PojoUtil;

@Service("competitionlocationcycleService")
public class CompetitionlocationcycleServiceImpl implements CompetitionlocationcycleService {
	@Autowired
	CompetitionlocationcycleRepository competitionlocationcycleRepository;

	@Autowired
	CompetitionplayerService competitionplayerService;
	
	@Autowired
	PlayerService playerService;
	
	
	@Autowired
	CompetitionplayercycleService competitionplayercycleService;
	
	@Autowired 
	GameruleService gameruleService;
	
	@Autowired 
	MatchService matchService;
	
	@Autowired
	PlayercoinService playercoinService;

	@Override
	public SimpleLocationroundInfo getSimpleLocationcycleInfoById(long competitionlocationcycleId) {
		SimpleLocationroundInfo rI = new SimpleLocationroundInfo();
		Competitionlocationcycle clc = competitionlocationcycleRepository.findById(competitionlocationcycleId)
				.orElse(null);
		if (clc != null) {
			rI.setMatch(PojoUtil.toSimpleMatch(clc.getMatch()));
			rI.setPlayer(PojoUtil.toSimpleCycleplayer(clc.getPlayer()));
		}

		return rI;
	}

	@Override
	public void addPlayer2Loctioncycle(Set<SimpleLocationCycleplayer> setPlayer, long competitionlocationcycleId) throws PlayerincycleExistsException,Exception {

		Competitionlocationcycle clc = competitionlocationcycleRepository.findById(competitionlocationcycleId)
				.orElse(null);
		
		if (clc != null) {
			//Runde holen, um überprüfen zu können, ob der Spieler bereits dort eingesetzt wurde
			Collection<Competitionplayercycle> cpcLs=competitionplayercycleService.findByLocationcycle(clc);
			System.out.println("Cycleplayer "+setPlayer.size());
			Set<String> errorCreated = new HashSet<String>();
			for(SimpleLocationCycleplayer sp : setPlayer) {
			//setPlayer.forEach(sp -> {
				
				Competitionplayer competitionPlayer;

				try {
					//competitionPlayer = competitionplayerService.findById(sp.getPlayerId());
					
					//boolean isInCycle = false;
					Player player=playerService.findById(sp.getPlayerId());//competitionPlayer.getPlayer();
					// TODO Überprüfen, ob bereits im Cycle
					
					//AtomicReference<Boolean> isInCycle = new AtomicReference<>();
					//isInCycle.set(false);
					Competitionplayercycle playerExits=null;
					if(cpcLs!=null && !cpcLs.isEmpty()) {
						//System.out.println("competitionplayercycle FOUND "+cpcLs.size());
						for(Competitionplayercycle item:cpcLs) {
							if(item.getPlayer().getId()==player.getId()) {
								//isInCycle.set(true);
								playerExits=item;
							}
						}
					}
					
					if (playerExits==null) {
						// Player finden und Competionplayercycle erzeugen
						Competitionplayercycle cpc = new Competitionplayercycle();
						cpc.setLocationcycle(clc);
						cpc.setSequence(sp.getSequence());
						cpc.setPlayer(player);
						
						competitionplayercycleService.save(cpc);
					}else {
						playerExits.setSequence(sp.getSequence());
						competitionplayercycleService.save(playerExits);
						//throw new PlayerincycleExistsException("Player with id "+player.getId()+" exist in Competitionplayercycle",sp);
					}
				}  catch (Exception e) {
					e.printStackTrace();
					errorCreated.add("ERROR creating playercycle " + sp.getId() + " message:" + e.getMessage());
				}
			}
				//});

			if (!errorCreated.isEmpty())
				throw new Exception(errorCreated.toString());

		}else {
			System.out.println("Competitionlocationcycle with id  "+competitionlocationcycleId+" is NULL");
		}

	}

	@Override
	public void addMatchForCycle(SimpleMatch match, long locationroundId) throws Exception {
		
		Competitionlocationcycle clc = competitionlocationcycleRepository.findById(locationroundId).orElse(null);
		
		if (clc == null) 
			throw new Exception("Cycle wicth id "+locationroundId+" not valid!");
		
		
		Set<Match> mSet=clc.getMatch();
		int mSize=mSet ==null ? 0 : mSet.size();
		
		if(match.getPlayercoin()==null)
			throw new Exception("Playercoin for match not found!");
		
		
			Gamerulevariant grv=gameruleService.getGamerulevariantById(match.getVariantId());
			if(grv!=null) {
				Match nMatch=new Match();
				nMatch.setCoin(match.getCoin());
				nMatch.setSequence(mSize+1);
				nMatch.setTime(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
				nMatch.setGamerulevariant(grv);
				nMatch.setCompetitionlocationcycle(clc);
				nMatch.setFactor(match.getFactor()<=0 ? 1: match.getFactor());
				matchService.add(nMatch);
				
				
				match.getPlayercoin().forEach(pc->{
					Competitionplayercycle player=competitionplayercycleService.findById(pc.getUserId());
					try {
						if(player!=null) {
							Playercoin coin =new Playercoin(); 
							coin.setCoin(pc.getCoin());
							coin.setMatch(nMatch);
							coin.setPlayer(player.getPlayer());
							coin.setCompetitionplayercycle(player);
							coin.setCyclesequence(mSize+1);
						
							playercoinService.add(coin);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				})  ;
			}else {
				throw new Exception("Gamerulevariant "+match.getVariantId()+" not found!");
			}
		
	}

	@Override
	public void deletePlayerFromLocationcycle(long clcId) throws PlayerincycleDeleteException {
		
		Competitionplayercycle player=competitionplayercycleService.findById(clcId);
		if(player!=null) {
			player.getLocationcycle();
			player.getPlayer();
			if(player.getCoin()== null || player.getCoin().isEmpty()) {
				try {
					competitionplayercycleService.delete(player);
				} catch (Exception e) {
					throw new PlayerincycleDeleteException("Competitionplayercycle with id "+clcId+" could not be deleted", 502);
				}	
			}else {
				throw new PlayerincycleDeleteException("Competitionplayercycle with id "+clcId+" has matches ("+player.getLocationcycle().getMatch().size()+")!", 501);
			}
		}else {
			throw new PlayerincycleDeleteException("Competitionplayercycle with id "+clcId+" does not exist!", 500);
		}
		
	}

	@Override
	public void add(Competitionlocationcycle competitionlocationcycle) throws Exception {
		competitionlocationcycleRepository.save(competitionlocationcycle);
		
	}

	@Override
	public Collection<Competitionlocationcycle> findByCompetitioncycle(Competitioncycle cycle) {
		
		return competitionlocationcycleRepository.findByCycle(cycle);
	}

	@Override
	public void deleteMatchByRoundAndId( long matchId,long locationroundId) throws StatusException {
		// TODO Es sollte überprüft werden ob das Spiel in der entsprechenden Runden enthalten ist
		Collection<Match> matchLs=matchService.findByCompetitionlocationcycleId(locationroundId);
		if(matchLs!=null) {
			int maxSeq=0;
			Match found=null;
			for(Match m:matchLs) {
				int tmpSeq=m.getSequence();
				if(maxSeq<tmpSeq){
					maxSeq=tmpSeq;
					
				}
				if(m.getId()==matchId) {
					found=m;
				}
			}
			if(found==null)
				throw new StatusException("Unknown Error",501);
			else if(found.getSequence()<maxSeq) {
				throw new StatusException("Unknown Error",502);
			}
			
			try {
				matchService.deleteById(matchId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new StatusException("Unknown Error",599);
			}
		}
		
	}

	@Override
	public Competitionlocationcycle fincdById(long id) throws Exception {
		
		return competitionlocationcycleRepository.findById(id).orElse(null);
	}

	@Override
	public void getNextMatchForCycle(SimpleMatchRequest matchRequest, String username) throws Exception {
		Competitionlocationcycle clc = competitionlocationcycleRepository.findById(matchRequest.getLocationroundId()).orElse(null);
		
		if (clc == null) 
			throw new Exception("Cycle wicth id "+matchRequest.getLocationroundId()+" not valid!");
		
		Match matchOpen=null;
		//Suchen eines offens Matches (Datum nicht gesetzt)
		Collection<Match> matchOpenCol=matchService.findByCompetitionlocationcycleIdAndGamerulevariant(clc.getId(), null);
		if(matchOpenCol!=null && matchOpenCol.size()==1) {
			matchOpen=matchOpenCol.iterator().next();
		}
		
		//Wenn kein Match vorhanden ein neues anlegen
		
		Set<Match> mSet=clc.getMatch();
		int mSize=mSet ==null ? 0 : mSet.size();
		
		
			
		if(matchOpen==null) {
			Match nMatch=new Match();
			nMatch.setSequence(mSize+1);
			//nMatch.setTime();
			nMatch.setCompetitionlocationcycle(clc);
			
			matchService.add(nMatch);
			
			matchRequest.getPlayer().forEach(pc->{
				Competitionplayercycle player=competitionplayercycleService.findById(pc.getId());
				try {
					if(player!=null) {
						Playercoin coin =new Playercoin(); 
						coin.setCoin(0);
						coin.setMatch(nMatch);
						coin.setPlayer(player.getPlayer());
						coin.setCompetitionplayercycle(player);
						coin.setCyclesequence(mSize+1);
					
						playercoinService.add(coin);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			})  ;
			
			Set<Competitiongamerule> ruleSet=clc.getLocation().getCompetition().getGamerule();
			
			//TODO ggf. muss gamerule auch übergeben werden, bei mehreren Regeln
			Competitiongamerule rule=ruleSet.iterator().next();
			Set<Gameruleitem> cardsSet=rule.getGamerule().getItem();
			
			List<Gameruleitem> cards=cardsSet.stream().collect(Collectors.toList());
			//Karten mischen
			Collections.shuffle(cards);
			//Karten noch mal mischen
			Collections.shuffle(cards);
			
			
			
			
			
			
		}
		
		
	}

}
