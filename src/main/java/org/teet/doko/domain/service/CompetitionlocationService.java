package org.teet.doko.domain.service;

import java.util.Collection;

import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitionlocation;
import org.teet.doko.gateway.v1.SimpleLocation;


public interface CompetitionlocationService {
	public Iterable<Competitionlocation> findAll() throws Exception;
	/**
    *
    * @param community
    * @throws Exception
    */
   public void add(Competitionlocation community) throws Exception;
   
   public Collection<Competitionlocation>  findByCompetition( Competition competition) throws Exception ;
   
   public Competitionlocation findByNameAndCompetition(String name, Competition competition) throws Exception;
   /**
   *
   * @param simpleCompetitionlocation
   * @throws Exception
   */
   public Competitionlocation add(SimpleLocation simpleLocation) throws Exception;
   
	/**
    *
    * @param communityId
    * @return
    * @throws Exception
    */
   public Competitionlocation findById(Long communityId) throws Exception;

   
}
