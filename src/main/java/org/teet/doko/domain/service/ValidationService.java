package org.teet.doko.domain.service;

import java.util.Set;

import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.exception.CommunityplayerAddException;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleMember;
import org.teet.doko.gateway.v1.SimplePlayer;

public interface ValidationService {
	public Communityplayer addSimplePlayer2Community(SimplePlayer player,long communityId) throws CommunityplayerAddException;
	
	public Communityplayer addMember2Community(SimplePlayer authenticatedUser,SimpleMember member) throws StatusException; 
	
	public Set<Competitionplayer> addCompetitionplayer(SimplePlayer authenticatedUser,Set<SimpleMember> meberList, long competitionId) throws StatusException;
	
	public Competition addCompetition(SimplePlayer authenticatedUser,SimpleCompetition simpleCompetition) throws StatusException;
}
