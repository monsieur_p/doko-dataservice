package org.teet.doko.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Community;
import org.teet.doko.domain.CommunityRepository;
import org.teet.doko.domain.Communityplayer;
import org.teet.doko.domain.CommunityplayerCustomRepository;
import org.teet.doko.domain.CommunityplayerRepository;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.PlayerRepository;
import org.teet.doko.exception.CommunityplayerAddException;
import org.teet.doko.gateway.v1.SimpleMember;
import org.teet.doko.gateway.v1.SimplePlayer;

@Service("communityplayerService")
public class CommunityplayerServiceImpl implements CommunityplayerService {
	
	@Autowired
	private PlayerRepository playerRepository;
	
	@Autowired
    private CommunityRepository communityRepository;
	
	@Autowired
    private CommunityplayerRepository communityplayerRepository;
	
	@Autowired
    private CommunityplayerCustomRepository communityplayerCustomRepository;
	
	
	@Override
	public void add(Communityplayer communityplayer,SimplePlayer authenticatedUser) throws Exception {
		communityplayerRepository.save(communityplayer);
		
	}

	@Override
	public List<Community> findByPlayer(Player player) throws Exception {
		
		return communityplayerCustomRepository.getCommunityByPlayer(player);
	}

	@Override
	public Communityplayer add(SimpleMember member,SimplePlayer authenticatedUser) throws Exception {
		//TODO CHECKEN OB Username von Player exist
		Player player=playerRepository.findByUsername(member.getUsername());
		if(player==null) {
			player=new Player();
			player.setFirstname(member.getFirstname());
			player.setLastname(member.getLastname());
			player.setUsername(member.getUsername());
			playerRepository.save(player);
		}else {
			throw new Exception("Player with username "+member.getUsername()+" exist!");
		}
		
		 Optional<Community> co = communityRepository.findById(member.getCommunityId());
		if(co!=null) {
			Communityplayer cPlayer=addPlayer2Community(player,co.get());
			
			return cPlayer;
		}else {
			throw new Exception("Community with id "+member.getId()+" does not exist!");
		}
	}
	
	@Override
	public Communityplayer editMember(SimpleMember member,SimplePlayer authenticatedUser) throws Exception {
		
		Communityplayer player=communityplayerRepository.findById(member.getId()).orElse(null);
		if(player!=null && authenticatedUser!=null) {
			
			Player pOwner=player.getCommunity().getOwner();
			if(pOwner.getUserid().equals(authenticatedUser.getUserid())) {
				player.setActive(member.isActive());
				communityplayerRepository.save(player);
			}
			
		}else {
			throw new Exception("Communityplayer with username "+member.getUsername()+" exist!");
		}
		
		return player;
	}
	
	public Communityplayer addPlayer2Community(Player player, Community community) throws CommunityplayerAddException {
		if(community==null)
			throw new CommunityplayerAddException("Community not found",501);
		if(player==null)
			throw new CommunityplayerAddException("Player not found",502);
		
		Communityplayer cPlayer=null;
		try {
			cPlayer = communityplayerRepository.findByCommunityAndPlayer(community, player);
		} catch (Exception e) {
			throw new CommunityplayerAddException("Error querieng Communityplayer!",503);
		}
		if(cPlayer!=null) 
			throw new CommunityplayerAddException("Communityplayer exists!",504);
		
		Communityplayer comPlayer=new Communityplayer();
		comPlayer.setPlayer(player);
		comPlayer.setCommunity(community);
		comPlayer.setActive(false);
		communityplayerRepository.save(comPlayer);
			
		return comPlayer;
	}
	/*
	@Override
	public Communityplayer addSimplePlayer2Community(SimplePlayer sPlayer, long communityId) throws CommunityplayerAddException {
		Player player=playerRepository.findByUserid(sPlayer.getUserid());
		Community community = communityRepository.findById(communityId).orElse(null);
		
		if(player==null) {
			player=new Player();
			player.setFirstname(sPlayer.getFirstname());
			player.setLastname(sPlayer.getLastname());
			player.setUsername(sPlayer.getUsername());
			player.setEmail(sPlayer.getEmail());
			player.setUserid(sPlayer.getUserid());
			player.setPhone(sPlayer.getPhone());
			
			
			playerRepository.save(player);
		}
		Communityplayer cp=addPlayer2Community(player,community);
			
		
		
		return cp;
	}
	*/
/*
	@Override
	public void setPlayerActive(long id,SimplePlayer authenticatedUser) throws Exception {
		Communityplayer cp=communityplayerRepository.findById(id).orElse(null);
		
		if(cp!=null&& authenticatedUser!=null) {
			Player pOwner=cp.getCommunity().getOwner();
			if(pOwner.getUserid().equals(authenticatedUser.getUserid())) {
				cp.setActive(true);
				communityplayerRepository.save(cp);
			}else {
				throw new Exception("Authenticated "+authenticatedUser.getUsername()+" user is not allowed to activate Communityplayer");
			}
			
		}else {
			throw new Exception("Communityplayer or authenticated user not found");
		}
		
	}*/


	
	
}
