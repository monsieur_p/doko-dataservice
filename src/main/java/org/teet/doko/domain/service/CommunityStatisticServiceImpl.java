package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.CompetitionCustomRepository;
import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.domain.Player;
import org.teet.doko.domain.Playercoin;
import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleGameresult;
import org.teet.doko.gateway.v1.SimpleGamerulevariant;
import org.teet.doko.gateway.v1.SimpleGamestatistic;
import org.teet.doko.gateway.v1.SimplePlayercoin;
import org.teet.doko.gateway.v1.SimplePlayercoingroup;
import org.teet.doko.gateway.v1.SimplePlayerresult;
import org.teet.doko.gateway.v1.SimpleStatistic;
import org.teet.doko.util.PojoUtil;

@Service("communitystatisticService")
public class CommunityStatisticServiceImpl implements CommunityStatisticService {
	@Autowired
	private CompetitionCustomRepository competitionCustomRepository;
	@Override
	public Set<SimplePlayercoin> getSimplePlayercoinByCompetitionId(Long competionId) {
		return PojoUtil.toSimplePlayercoin(getPlayercoinByCompetitionId(competionId)); 
	}
	
	
	private Set<Playercoin> getPlayercoinByCompetitionId(Long competionId) {
		return  new HashSet<Playercoin>(competitionCustomRepository.getPlayercoinByCompetition(competionId));
	}
	
	public SimpleGamestatistic sumPlayercoinByCompetitionId(Long competionId) {
		SimpleGamestatistic statistic=new SimpleGamestatistic();
		Set<Playercoin> coins=getPlayercoinByCompetitionId(competionId);
		/*
		List<Playercoin> playerCoins=competitionCustomRepository.getPlayercoinByCompetition(competionId);
		if(playerCoins!=null && !playerCoins.isEmpty()) {
			coins=new HashSet<Playercoin>(playerCoins);
		}
		*/
		
		Map<Long,SimpleGamerulevariant> variant =new HashMap<Long,SimpleGamerulevariant>();
		Map<Long,SimplePlayerresult> result =new HashMap<Long,SimplePlayerresult>();
		
		coins.forEach(sp -> {
			Player player=sp.getPlayer();
			long userId=player.getId();
			SimplePlayerresult uResult;
			if(!result.containsKey(userId)) {
				uResult=new SimplePlayerresult();
				uResult.setFirstname(player.getFirstname());
				uResult.setLastname(player.getLastname());
				uResult.setId(player.getId());
				uResult.setGame(new HashSet<SimpleGameresult>());
				result.put(player.getId(),uResult);
			}else {
				uResult=result.get(userId);
			}
			
			uResult.setMatches(uResult.getMatches()+1);
			uResult.setResult(uResult.getResult()+sp.getCoin());
			uResult.setAverage(uResult.getResult()/uResult.getMatches());
			
			Gamerulevariant grv=sp.getMatch().getGamerulevariant();
			
			AtomicReference<Boolean> value = new AtomicReference<>();
			value.set(false);
			uResult.getGame().forEach(gv ->{
				if(grv.getId()==gv.getId()) {
					value.set(true);
					gv.setCount(gv.getCount()+1);
					gv.setResult(gv.getResult()+sp.getCoin());
					
				}
			});
			
			if(!value.get()) {
				SimpleGameresult sgr=new SimpleGameresult();
				sgr.setId(grv.getId());
				sgr.setCount(1);
				sgr.setResult(sp.getCoin());	
				uResult.getGame().add(sgr);
				
				if(!variant.containsKey(grv.getId())) {
					SimpleGamerulevariant sgrv =new SimpleGamerulevariant();
					sgrv.setId(grv.getId());
					sgrv.setTitle(grv.getTitle());
					//sgrv.setTitle(grv.getGamevariant().getTitle());
					variant.put(grv.getId(), sgrv);
				}
			}
		});
		statistic.setVariant(new HashSet<>(variant.values()));
		statistic.setPlayer(new HashSet<>(result.values()));
		
		return statistic;
	}
	
	@Override
	public Set<SimplePlayercoin> getSimplePlayercoinByCommunityId(Long communityId) {
		Set<Playercoin> coinLs= new HashSet<Playercoin>(competitionCustomRepository.getPlayercoinByCommunity(communityId));
		
		return PojoUtil.toSimplePlayercoin(coinLs); 
	}


	@Override
	public SimpleGamestatistic getPlayerstatisticByCompetition(Long competionId) {
		return  sumPlayercoinByCompetitionId(competionId);
	}


	@Override
	public Set<SimplePlayercoin> getSimplePlayercoinSumByCompetitionId(Long competionId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<SimplePlayercoin> getPlayercoinSumByCompetition(Long competionId) {
		
		return competitionCustomRepository.getPlayercoinSumByCompetition(competionId);
	}


	@Override
	public Collection<SimpleCompetition> getCompetitionByCommunityId(long communityId) {
		Collection<Competition> comList=competitionCustomRepository.getCompetitionByCommunityId(communityId);
		
		return PojoUtil.toSimpleCompetition(comList);
	}


	@Override
	public SimpleStatistic getPlayercoinStatisticSumByCompetition(Long competionId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<SimplePlayercoingroup> getPlayercoinSumByCompetitionAndCycle(Long competionId) {
		List<SimplePlayercoingroup> coinggroups = new ArrayList<SimplePlayercoingroup>();
		List<SimplePlayercoin> coins=competitionCustomRepository.getPlayercoinSumByCompetitionAndCycle(competionId);
		Map<Integer,List<SimplePlayercoin>> groupMap=new HashMap<Integer,List<SimplePlayercoin>>();
		for(SimplePlayercoin c:coins) {
			if(groupMap.containsKey(c.getCyclesequence())) {
				groupMap.get(c.getCyclesequence()).add(c);
			}else {
				List<SimplePlayercoin> cLs=new ArrayList<SimplePlayercoin>();
				cLs.add(c);
				groupMap.put(c.getCyclesequence(), cLs);
			}
		}
		for(Map.Entry<Integer,List<SimplePlayercoin>> entry:groupMap.entrySet()) {
			SimplePlayercoingroup gr=new SimplePlayercoingroup();
			gr.setSequence(entry.getKey());
			gr.setCoin(entry.getValue());
			coinggroups.add(gr);
		}
		return coinggroups;
	}

}
