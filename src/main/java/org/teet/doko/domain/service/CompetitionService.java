package org.teet.doko.domain.service;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.teet.doko.domain.Community;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.Competitioncycle;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.gateway.v1.SimpleCompetition;
import org.teet.doko.gateway.v1.SimpleCompetitionInfo;
import org.teet.doko.gateway.v1.SimpleMember;

public interface CompetitionService {
	public Iterable<Competition> findAll() throws Exception;
	/**
    *
    * @param community
    * @throws Exception
    */
   public void add(Competition competition) throws Exception;
	
   
   public Competition add(SimpleCompetition simpleCompetition) throws Exception;
   /**
    *
    * @param community
    * @return
    * @throws Exception
    */
   public Competition findById(Long competition) throws Exception;
   
   
   /**
   *
   * @param communityId
   * @return
   * @throws Exception
   */
  public  Collection<Competition> findByCommunityId(long competitionId) throws Exception;
  
  
   /**
    *
    * @param community
    * @return
    * @throws Exception
    */
   public  Collection<Competition> findByCommunity(Community competitionId) throws Exception;

   
   
   /**
    *
    * @param name
    * @return
    * @throws Exception
    */
   public Collection<Competition> findByCriteria(Map<String, ArrayList<String>> name) throws Exception;
   
   public SimpleCompetitionInfo findSimpleinfoById(long competitionId) throws Exception;
   
   public Competitioncycle addCompetitioncycleForAllLocation(long competitionId);
   
   public Set<Competitionplayer> addCompetitionplayer(Set<SimpleMember> meberList, long competitionId);
   
   public Competitionplayer addCompetitionplayer(SimpleMember member, Competition competition) throws Exception;
}

