package org.teet.doko.domain.service;

import java.util.Collection;

import org.teet.doko.domain.Community;
import org.teet.doko.domain.Communitygamerule;
import org.teet.doko.gateway.v1.SimpleCommunity;
import org.teet.doko.gateway.v1.SimpleCommunityInfo;
import org.teet.doko.gateway.v1.SimpleGamerule;
import org.teet.doko.gateway.v1.SimplePlayer;
import org.teet.doko.gateway.v1.SimpleStatusResponse;

public interface CommunityService {
	public Iterable<Community> findAll() throws Exception;
	/**
    *
    * @param community
    * @throws Exception
    */
   public void add(Community community) throws Exception;
   /**
   *
   * @param simpleCommunity
   * @throws Exception
   */
   public Community add(SimpleCommunity simpleCommunity) throws Exception;
   
	/**
    *
    * @param communityId
    * @return
    * @throws Exception
    */
   public Community findById(Long communityId) throws Exception;

   /**
    *
    * @param title
    * @return
    * @throws Exception
    */
   public Community findByTitle(String title) throws Exception;

   public SimpleCommunityInfo findSimpleinfoById(long id) throws Exception;
   
   public Communitygamerule addRuleForCommunity(SimpleGamerule game, long communityId,SimplePlayer player) throws Exception;
   
   public SimpleGamerule getSimpleGameruleByCommunitygameruleId(long communitygameruleId);
   
   public Collection<SimplePlayer> findBySearchtext(String searchtext);
   
   public SimpleStatusResponse addPlayerToCommunity(SimplePlayer player,long communityId);
   
}
