package org.teet.doko.domain.service;

import java.util.Collection;

import org.teet.doko.domain.Game;
import org.teet.doko.domain.Gamerule;
import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGamerule;
import org.teet.doko.gateway.v1.SimpleGamerulevariant;
import org.teet.doko.gateway.v1.SimplePlayer;

public interface GameruleService {
	
	/**
    *
    * @param cashdesk
    * @throws Exception
    */
   public void add(Gamerule gamerule) throws Exception;
	/**
    *
    * @param productorderId
    * @return
    * @throws Exception
    */
   public Collection<Gamerule> findByTitle(String title) throws Exception;
   
   public Gamerule findById(long id) throws Exception;
   
   public Collection<Gamerule> findByGame(Game game) throws Exception;
   
   public Collection<SimpleGamerule> findByGameId(long id) throws Exception;
   
   public Gamerulevariant addGamevariant2Gamerule(SimpleGamerulevariant variant, long ruleId, SimplePlayer player) throws Exception;

   public SimpleGamerule getSimpleGameruleById(long ruleId)  throws Exception;
   
   public Gamerulevariant getGamerulevariantById(long gamerulevariantId)  throws Exception;
   
   public Gamerule findByIdChecked(long id,SimplePlayer player)throws StatusException;
   
}
