package org.teet.doko.domain.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Competition;
import org.teet.doko.domain.CompetitionRepository;
import org.teet.doko.domain.Competitionplayer;
import org.teet.doko.domain.CompetitionplayerRepository;
import org.teet.doko.gateway.v1.SimplePlayer;

@Service("competitionplayerService")
public class CompetitionplayerServiceImpl implements CompetitionplayerService {
	
	@Autowired
	CompetitionplayerRepository competitionplayerRepository;
	
	@Autowired
	CompetitionRepository competitionRepository;
	
	@Override
	public void add(Competitionplayer communityplayer) throws Exception {
		
		competitionplayerRepository.save(communityplayer);
		
	}

	@Override
	public Competitionplayer add(SimplePlayer player) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Competitionplayer findById(long communityplayerId) throws Exception {
		
		return competitionplayerRepository.findById(communityplayerId).orElse(null);
	}
	
	@Override
	public Set<Competitionplayer> findByCompetitionId(long competitionId) throws Exception {
		Competition c1=competitionRepository.findById(competitionId).orElse(null);
		return competitionplayerRepository.findByCompetition(c1);
	}
	
}
