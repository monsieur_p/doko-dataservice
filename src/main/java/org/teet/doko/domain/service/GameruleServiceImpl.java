package org.teet.doko.domain.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Game;
import org.teet.doko.domain.Gamerule;
import org.teet.doko.domain.GameruleRepository;
import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.domain.GamerulevariantRepository;
import org.teet.doko.exception.StatusException;
import org.teet.doko.gateway.v1.SimpleGamerule;
import org.teet.doko.gateway.v1.SimpleGamerulevariant;
import org.teet.doko.gateway.v1.SimpleGamerulevariantitem;
import org.teet.doko.gateway.v1.SimplePlayer;
import org.teet.doko.util.PojoUtil;
@Service("gameruleService")
public class GameruleServiceImpl implements GameruleService{
	
	@Autowired
    private GameruleRepository gameruleRepository;
	
	//@Autowired
	//private GamevariantRepository gamevariantRepository;
	
	@Autowired
    private GamerulevariantRepository gamerulevariantRepository;
	
	@Autowired
    private GameService gameService;
	
	@Autowired
    private GameruleService gameruleService;
	
	@Autowired
	private GamerulevariantitemService gamerulevariantitemService;
	
	@Override
	public void add(Gamerule gamerule) throws Exception {
		gameruleRepository.save(gamerule);
		
	}

	@Override
	public Collection<Gamerule> findByTitle(String title) throws Exception {
		
		return gameruleRepository.findByTitle(title);
	}


	@Override
	public Collection<Gamerule> findByGame(Game game) throws Exception {
		Collection<Gamerule> ruleCol=gameruleRepository.findByGame(game);
			if(ruleCol!=null) {
				ruleCol.forEach(item->{
					item.getItem();
					item.getVariant();
				});
			}
		return gameruleRepository.findByGame(game);
	}
	
	@Override
	public Gamerulevariant addGamevariant2Gamerule(SimpleGamerulevariant variant, long ruleId, SimplePlayer player)  throws Exception{
		Gamerule gr=gameruleService.findByIdChecked(ruleId,player);
		if(gr!=null) {
			/*
			//TODO Checken ob Varinate bereits existiert
			Gamevariant gv=new Gamevariant(); 
			gv.setTitle(variant.getTitle());
			gv.setDescription(variant.getDescription());
			gv.setGame(gr.getGame());
			
			gamevariantRepository.save(gv);
			*/
			Gamerulevariant var=new Gamerulevariant();
			var.setGamerule(gr);
			var.setTitle(variant.getTitle());
			var.setDescription(variant.getDescription());
			//var.setGamevariant(gv);
			var.setSequence(gr.getVariant().size()+1);
			gamerulevariantRepository.save(var);
			if(variant.getItem()!=null) {
				for(SimpleGamerulevariantitem item:variant.getItem()) {
					item.setGamerulevariant_id(var.getId());
					gamerulevariantitemService.add(item);
				}
			}
			
			
			
			return var;
		}
		// TODO Auto-generated method stub
		throw new Exception("Gaemrule invalid for adding Gamevariant");
	}

	@Override
	public SimpleGamerule getSimpleGameruleById(long ruleId)  throws Exception {
		Gamerule gr=gameruleRepository.findById(ruleId).orElse(null);
		gr.getItem();
		gr.getVariant();
		if(gr!=null) {
			gr.getVariant();
			
			return PojoUtil.toSimpleGamerule(gr);
			
		}
		throw new Exception("Gaemrule invalid!");
	}

	@Override
	public Gamerulevariant getGamerulevariantById(long gamerulevariantId) throws Exception {
		
		return gamerulevariantRepository.findById(gamerulevariantId).orElse(null);
	}

	@Override
	public Gamerule findById(long id) throws Exception {
		
		return gameruleRepository.findById(id).orElse(null);
	}

	@Override
	public Collection<SimpleGamerule> findByGameId(long id) throws Exception {
		Game game=gameService.findById(id);
		Collection<Gamerule> ruleLs=this.findByGame(game);
		if(ruleLs!=null) {
			return PojoUtil.toSimpleGamerule(ruleLs);
		}
		// TODO Auto-generated method stub
		return null;
	}

	public Gamerule findByIdChecked(long id,SimplePlayer player)throws StatusException {
		Gamerule gamerule;
		try {
			gamerule = findById(id);

			if(gamerule==null)
				throw new StatusException("Game not found",501);
			
			if(gamerule.getOwner()==null|| player==null) {
				String msg =player==null ? "no user authenticated" : "rootgame";
				throw new StatusException("Editing not allowed "+msg,502);
			}else if(!gamerule.getOwner().getUserid().equals(player.getUserid())){
				throw new StatusException("User not valid "+player.getUserid(),503);
			}
			return gamerule;
		}catch (StatusException e) {
			// TODO Auto-generated catch block
			throw new StatusException(e.getMessage(),e.getErrorCode());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new StatusException("Error ask game "+id+" not found "+e.getMessage(),504);
		}
	}
	
	

}
