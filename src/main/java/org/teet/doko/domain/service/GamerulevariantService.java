package org.teet.doko.domain.service;

import java.util.Collection;

import org.teet.doko.domain.Gamerulevariant;
import org.teet.doko.domain.Gamerulevariantitem;

public interface GamerulevariantService {
	public Collection<Gamerulevariant> getGamerulevariantByGameId(long id); 
	
	public Gamerulevariant findById(long id) throws Exception;
}
