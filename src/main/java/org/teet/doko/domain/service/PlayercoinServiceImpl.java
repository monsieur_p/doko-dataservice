package org.teet.doko.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.teet.doko.domain.Playercoin;
import org.teet.doko.domain.PlayercoinRepository;

@Service("playercoinService")
public class PlayercoinServiceImpl implements PlayercoinService{
	
	@Autowired
	PlayercoinRepository playercoinRepository;
	
	@Override
	public void add(Playercoin playercoin) throws Exception {
		
		playercoinRepository.save(playercoin);
	}

}
