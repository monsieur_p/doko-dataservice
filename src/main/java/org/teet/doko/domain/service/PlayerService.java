package org.teet.doko.domain.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.teet.doko.domain.Player;

public interface PlayerService {
	
	/**
    *
    * @param cashdesk
    * @throws Exception
    */
   public void add(Player player) throws Exception;
	/**
    *
    * @param productorderId
    * @return
    * @throws Exception
    */
   public Player findById(Long playerId) throws Exception;

   /**
    *
    * @param deviceid
    * @return
    * @throws Exception
    */
   public Player findByUsername(String username) throws Exception;
   
   /**
    * 
    * @param username
    * @return
    * @throws Exception
    */
   public Player findByUserid(String userid) throws Exception;
   
   
   /**
    *
    * @param name
    * @return
    * @throws Exception
    */
   public Collection<Player> findByCriteria(Map<String, ArrayList<String>> name) throws Exception;
}
