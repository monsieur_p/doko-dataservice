package org.teet.doko.domain;

import org.springframework.data.repository.CrudRepository;

public interface GamerulevariantitemRepository extends CrudRepository<Gamerulevariantitem, Long>  {

}
