package org.teet.doko.domain;

import org.springframework.data.repository.CrudRepository;


public interface CommunityRepository extends CrudRepository<Community, Long> {

	Community findByTitle(String title);
	
	
}
