package org.teet.doko.domain;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/**
 * Die konkrete Karte in einem Spiel eines Spielers
 * @author todhinio
 *
 */

@Entity
@Table(name="matchplayeritem")
public class Matchplayeritem {
	private long id;
	private Match match;
	private Player player;
	private Gameruleitem item;
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "match_id",nullable=false)
	public Match getMatch() {
		return match;
	}
	public void setMatch(Match match) {
		this.match = match;
	}
	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "player_id",nullable=false)
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "gameruleitem_id",nullable=false)
	public Gameruleitem getItem() {
		return item;
	}
	public void setItem(Gameruleitem item) {
		this.item = item;
	}
	
	
}
