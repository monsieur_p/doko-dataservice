package org.teet.doko.domain;

import org.springframework.data.repository.CrudRepository;

public interface PlayercoinRepository extends CrudRepository<Playercoin, Long> {

}
