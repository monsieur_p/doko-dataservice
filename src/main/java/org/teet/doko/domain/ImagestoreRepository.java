package org.teet.doko.domain;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImagestoreRepository extends JpaRepository<Imagestore, Long> {

    Optional<Imagestore> findByName(String name);

}
