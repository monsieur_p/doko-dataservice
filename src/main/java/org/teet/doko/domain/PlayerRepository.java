package org.teet.doko.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends CrudRepository<Player, Long> {
	
	Player findByUsername(String username);
	Player findByUserid(String userid);
	
	
}
