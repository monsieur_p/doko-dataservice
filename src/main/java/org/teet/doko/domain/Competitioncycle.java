package org.teet.doko.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * Anzahl der Runden, die es in einem Turnier gegeben hat
 * @author todhinio
 *
 */
@Entity
public class Competitioncycle {
	private long id;
	private Competition competition;
	private int sequence;
	private String title;
	
	private Set<Competitionlocationcycle> locationcycle;
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@ManyToOne(optional = false,cascade = CascadeType.ALL)
	@JoinColumn(name = "competition_id",nullable=false)
	public Competition getCompetition() {
		return competition;
	}
	public void setCompetition(Competition competition) {
		this.competition = competition;
	}
	
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@JsonIgnore
	@OneToMany(
			mappedBy = "cycle",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.EAGER
	    )
	public Set<Competitionlocationcycle> getLocationcycle() {
		return locationcycle;
	}
	public void setLocationcycle(Set<Competitionlocationcycle> locationcycle) {
		this.locationcycle = locationcycle;
	}
	
	
	
}
