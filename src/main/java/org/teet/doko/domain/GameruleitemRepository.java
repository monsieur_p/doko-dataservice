package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface GameruleitemRepository extends CrudRepository<Gameruleitem, Long> {
	public Collection<Gameruleitem> findByGamerule(Gamerule gamerule) throws Exception;
}
