package org.teet.doko.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Gamerulevariant {
	private long id;
	private int sequence;
	private String title;
	private String description;
	
	private Gamerule gamerule;
	public Set<Gamerulevariantitem> item;
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	@ManyToOne(optional = false)
	@JoinColumn(name = "gamerule_id",nullable=false)
	public Gamerule getGamerule() {
		return gamerule;
	}
	public void setGamerule(Gamerule gamerule) {
		this.gamerule = gamerule;
	}
	

	@OneToMany(
			mappedBy = "gamerulevariant",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Gamerulevariantitem> getItem() {
		return item;
	}
	public void setItem(Set<Gamerulevariantitem> item) {
		this.item = item;
	}
	
	
	
	
}
