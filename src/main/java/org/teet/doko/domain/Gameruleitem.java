package org.teet.doko.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * z. B. konkrete Karten
 * @author todhinio
 *
 */

@Entity
public class Gameruleitem {
	private long id;
	private String name;
	private Gamerule gamerule;
	private Gameitem gameitem;
	private int sequence;
	private int points;
	private int count;
	
	
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@ManyToOne(optional = false)
	@JoinColumn(name = "gamerule_id",nullable=false)
	public Gamerule getGamerule() {
		return gamerule;
	}
	public void setGamerule(Gamerule gamerule) {
		this.gamerule = gamerule;
	}
	
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "gameitem_id",nullable=false)
	public Gameitem getGameitem() {
		return gameitem;
	}
	public void setGameitem(Gameitem gameitem) {
		this.gameitem = gameitem;
	}
	
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	

	
	
}
