package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface MatchplayeritemRepository extends CrudRepository<Matchplayeritem, Long> { 
	Collection<Matchplayeritem> findByMatch(Match match);
}
