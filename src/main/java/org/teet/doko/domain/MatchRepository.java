package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface MatchRepository extends CrudRepository<Match, Long> {
	public Collection<Match> findByCompetitionlocationcycle(Competitionlocationcycle competitionlocationcycle);
	
	public Collection<Match> findByCompetitionlocationcycleAndGamerulevariant(Competitionlocationcycle competitionlocationcycle,Gamerulevariant gamerulevariant);
}
