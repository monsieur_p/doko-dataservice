package org.teet.doko.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.teet.doko.gateway.v1.SimplePlayercoin;

@Service
public class CompetitionCustomRepositoryImpl implements CompetitionCustomRepository{
	private static final String QUERY_COMPETITION_BY_COMMUNITY = "SELECT c FROM competition c LEFT JOIN c.community co WHERE co.id = :communityId ORDER BY c.date";
	private static final String QUERY_PLAYERCPOINSUM_BY_MATCHES = "SELECT SUM(pc.coin),pc.player FROM Playercoin pc LEFT JOIN pc.player p LEFT JOIN pc.match m WHERE m.id IN(:matchIds) GROUP BY pc.player";
	private static final String QUERY_PLAYERCPOINSUM_BY_MATCHES_AND_CYCLE = "SELECT SUM(pc.coin),pc.player,c.sequence FROM Playercoin pc LEFT JOIN pc.player p LEFT JOIN pc.match m LEFT JOIN m.competitionlocationcycle cl LEFT JOIN cl.cycle c WHERE m.id IN(:matchIds) GROUP BY pc.player,cl.id ORDER BY c.sequence,p.id";
	
	private static final String QUERY_PLAYERCPOIN_BY_MATCHES = "SELECT pc FROM Playercoin pc LEFT JOIN pc.player p LEFT JOIN pc.match m  WHERE m.id IN(:matchIds)";
	private static final String QUERY_MATCHES_BY_COMPETITION = "SELECT m.id FROM Competition co LEFT JOIN co.cycle cy LEFT JOIN cy.locationcycle lc LEFT JOIN lc.match m WHERE co.id >= :competitionId";
	
	private static final String QUERY_MATCHES_BY_COMMUNITY = "SELECT m.id FROM Community cm LEFT JOIN cm.competition co LEFT JOIN co.cycle cy LEFT JOIN cy.locationcycle lc LEFT JOIN lc.match m WHERE cm.id >= :communityId";
	
	@PersistenceContext
    private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Playercoin> getPlayercoinByCommunity(long communityId) {
		Community community=em.find(Community.class, communityId);
		Query q1=em.createQuery(QUERY_MATCHES_BY_COMMUNITY);
		q1.setParameter("communityId", community.getId());
		
		List<Long> midList=q1.getResultList();
		
		Query q2=em.createQuery(QUERY_PLAYERCPOIN_BY_MATCHES);
		q2.setParameter("matchIds", midList);
		
		
		return q2.getResultList();
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Playercoin> getPlayercoinByCompetition(long competitionId) {
		Competition competition=em.find(Competition.class, competitionId);
		Query q1=em.createQuery(QUERY_MATCHES_BY_COMPETITION);
		q1.setParameter("competitionId", competition.getId());
		
		List<Long> midList=q1.getResultList();
		Query q2=em.createQuery(QUERY_PLAYERCPOIN_BY_MATCHES);
		q2.setParameter("matchIds", midList);
		
		return q2.getResultList();
	}


	@Override
	public List<SimplePlayercoin> getPlayercoinSumByCompetitionAndCycle(long competitionId) {
		List<SimplePlayercoin> sumCoins=new ArrayList<SimplePlayercoin>();
		Competition competition=em.find(Competition.class, competitionId);
		Query q1=em.createQuery(QUERY_MATCHES_BY_COMPETITION);
		q1.setParameter("competitionId", competition.getId());
		
		@SuppressWarnings("unchecked")
		List<Long> midList=q1.getResultList();
		Query q2=em.createQuery(QUERY_PLAYERCPOINSUM_BY_MATCHES_AND_CYCLE);
		q2.setParameter("matchIds", midList);
		
		for (Object result : q2.getResultList()){
			if (result == null) {
			      System.out.print("NULL");
			    } else if (result instanceof Object[]) {
			      Object[] row = (Object[]) result;
			      
			      
			    	  SimplePlayercoin coin=new SimplePlayercoin();
			    	  if(row[0] instanceof Long) {
			    		  Long val=(Long)row[0];
			    		  coin.setCoin(val!=null ? val.intValue() :0);
			    	  }else if(row[0] instanceof Integer) {
			    		  coin.setCoin((Integer)row[0]);
			    	  }
			    	  
			    	  Player p=(Player)row[1];
			    	  coin.setUserId(p.getId());
			    	  coin.setUsername(p.getUsername());
			    	  coin.setFirstname(p.getFirstname());
			    	  coin.setLastname(p.getLastname());
			    	  sumCoins.add(coin);
			    	  
			    	  if(row[2] instanceof Long) {
			    		  Long val=(Long)row[2];
			    		  coin.setCyclesequence(val!=null ? val.intValue() :0);
			    	  }else if(row[2] instanceof Integer) {
			    		  Integer val=(Integer)row[2];
			    		  coin.setCyclesequence(val!=null ? val :0);
			    	  }
			    }
		}
		return sumCoins;
	}
	
	
	@Override
	public List<SimplePlayercoin> getPlayercoinSumByCompetition(long competitionId) {
		List<SimplePlayercoin> sumCoins=new ArrayList<SimplePlayercoin>();
		Competition competition=em.find(Competition.class, competitionId);
		Query q1=em.createQuery(QUERY_MATCHES_BY_COMPETITION);
		q1.setParameter("competitionId", competition.getId());
		
		@SuppressWarnings("unchecked")
		List<Long> midList=q1.getResultList();
		Query q2=em.createQuery(QUERY_PLAYERCPOINSUM_BY_MATCHES);
		q2.setParameter("matchIds", midList);
		
		for (Object result : q2.getResultList()){
			if (result == null) {
			      System.out.print("NULL");
			    } else if (result instanceof Object[]) {
			      Object[] row = (Object[]) result;
			      
			      
			    	  SimplePlayercoin coin=new SimplePlayercoin();
			    	  if(row[0] instanceof Long) {
			    		  Long val=(Long)row[0];
			    		  coin.setCoin(val!=null ? val.intValue() :0);
			    	  }else if(row[0] instanceof Integer) {
			    		  coin.setCoin((Integer)row[0]);
			    	  }
			    	  
			    	  Player p=(Player)row[1];
			    	  coin.setUserId(p.getId());
			    	  coin.setUsername(p.getUsername());
			    	  coin.setFirstname(p.getFirstname());
			    	  coin.setLastname(p.getLastname());
			    	  sumCoins.add(coin);
			      
			    }
		}
		return sumCoins;
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Competition> getCompetitionByCommunityId(long communityId){
		Query q1=em.createQuery(QUERY_COMPETITION_BY_COMMUNITY);
		q1.setParameter("communityId", communityId);
		
		return q1.getResultList();
	}

}
