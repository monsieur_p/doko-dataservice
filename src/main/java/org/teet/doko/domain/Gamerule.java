package org.teet.doko.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Gamerule {
	private long id;
	private int playermin;
	private int playermax;
	public String title;
	public Game game;
	public Set<Gameruleitem> item;
	public Set<Gamerulevariant> variant;
	private Player owner;
	
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@ManyToOne(optional = false,cascade = CascadeType.ALL)
	@JoinColumn(name = "game_id",nullable=false)
	public Game getGame() {
		return game;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	@OneToMany(
			mappedBy = "gamerule",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Gamerulevariant> getVariant() {
		return variant;
	}
	public void setVariant(Set<Gamerulevariant> variant) {
		this.variant = variant;
	}
	public int getPlayermin() {
		return playermin;
	}
	public void setPlayermin(int playermin) {
		this.playermin = playermin;
	}
	public int getPlayermax() {
		return playermax;
	}
	public void setPlayermax(int playermax) {
		this.playermax = playermax;
	}
	@OneToMany(
			mappedBy = "gamerule",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Gameruleitem> getItem() {
		return item;
	}
	public void setItem(Set<Gameruleitem> item) {
		this.item = item;
	}
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "owner_id",nullable=true)
	public Player getOwner() {
		return owner;
	}
	public void setOwner(Player owner) {
		this.owner = owner;
	}
	
	
}
