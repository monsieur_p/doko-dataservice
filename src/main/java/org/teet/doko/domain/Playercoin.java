package org.teet.doko.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Playercoin {
	private long id;
	private int coin;
	private int cyclesequence;
	private Competitionplayercycle competitionplayercycle;
	private Player player;
	private Match match;
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getCyclesequence() {
		return cyclesequence;
	}
	public void setCyclesequence(int cyclesequence) {
		this.cyclesequence = cyclesequence;
	}
	
	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "player_id",nullable=false)
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public int getCoin() {
		return coin;
	}
	public void setCoin(int coin) {
		this.coin = coin;
	}
	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "match_id",nullable=false)
	public Match getMatch() {
		return match;
	}
	public void setMatch(Match match) {
		this.match = match;
	}
	
	
	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "competitionplayercycle_id",nullable=false)
	public Competitionplayercycle getCompetitionplayercycle() {
		return competitionplayercycle;
	}
	public void setCompetitionplayercycle(Competitionplayercycle competitionplayercycle) {
		this.competitionplayercycle = competitionplayercycle;
	}
	
	
	
}
