package org.teet.doko.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Competitionlocation {
	private long id;
	private String name;
	private Competition competition;	               
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	//CascadeType.ALL führt zu Fehler
	@ManyToOne(optional = false,cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
	@JoinColumn(name = "competition_id",nullable=false)
	public Competition getCompetition() {
		return competition;
	}
	public void setCompetition(Competition competition) {
		this.competition = competition;
	}
	
	
}
