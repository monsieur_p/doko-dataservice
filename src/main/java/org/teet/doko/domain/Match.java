package org.teet.doko.domain;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="competitionmatch")
public class Match {
	private long id;
	private Date time;
	private int sequence;
	private Competitionlocationcycle competitionlocationcycle;
	private Gamerulevariant gamerulevariant;
	private int coin;
	private int factor;
	private Set<Playercoin> playercoin;
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "competitionlocationcycle_id",nullable=false)
	public Competitionlocationcycle getCompetitionlocationcycle() {
		return competitionlocationcycle;
	}

	public void setCompetitionlocationcycle(Competitionlocationcycle competitionlocationcycle) {
		this.competitionlocationcycle = competitionlocationcycle;
	}

	public int getCoin() {
		return coin;
	}

	public void setCoin(int coin) {
		this.coin = coin;
	}
	@ManyToOne(optional = false,cascade = CascadeType.REFRESH)
	@JoinColumn(name = "gamerulevariant_id",nullable=false)
	public Gamerulevariant getGamerulevariant() {
		return gamerulevariant;
	}

	public void setGamerulevariant(Gamerulevariant gamerulevariant) {
		this.gamerulevariant = gamerulevariant;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@JsonIgnore
	@OneToMany(
			mappedBy = "match",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Playercoin> getPlayercoin() {
		return playercoin;
	}

	public void setPlayercoin(Set<Playercoin> playercoin) {
		this.playercoin = playercoin;
	}

	public int getFactor() {
		return factor;
	}

	public void setFactor(int factor) {
		this.factor = factor;
	}	
	
	
	
}
