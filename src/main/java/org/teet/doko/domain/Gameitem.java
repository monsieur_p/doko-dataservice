package org.teet.doko.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Gameitem {
	private long id;
	private String name;
	private Game game;
	private int sequence;
	private int points;
	private int count;
	private Long iconId;
	private Long imageId;
	private Gameitemtype itemtype; 
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@ManyToOne(optional = false,fetch = FetchType.LAZY)
	@JoinColumn(name = "game_id",nullable=false)
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	
	
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Long getIconId() {
		return iconId;
	}
	public void setIconId(Long iconId) {
		this.iconId = iconId;
	}
	public Long getImageId() {
		return imageId;
	}
	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}
	@ManyToOne(optional = true,fetch = FetchType.LAZY)
	@JoinColumn(name = "gameitemtype_id",nullable=true)
	public Gameitemtype getItemtype() {
		return itemtype;
	}
	public void setItemtype(Gameitemtype itemtype) {
		this.itemtype = itemtype;
	}
	
	
	
	
}
