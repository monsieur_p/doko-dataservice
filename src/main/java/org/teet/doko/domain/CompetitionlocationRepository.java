package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitionlocationRepository extends CrudRepository<Competitionlocation, Long>  {
	public Competitionlocation findByNameAndCompetition(String name,Competition competition);
	public Collection<Competitionlocation> findByCompetition(Competition competition);
}
