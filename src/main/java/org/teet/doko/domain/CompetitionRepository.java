package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitionRepository extends CrudRepository<Competition, Long>{
	 Collection<Competition> findByCommunity(Community community);
}
