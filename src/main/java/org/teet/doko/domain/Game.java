package org.teet.doko.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Game {
	private long id;
	private String title;
	private Player owner;
	private boolean open;
	public Set<Gameitemcategory> category;
	public Set<Gameitem> item;
	public Set<Gameitemtype> type;
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@OneToMany(
			mappedBy = "game",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Gameitem> getItem() {
		return item;
	}
	public void setItem(Set<Gameitem> item) {
		this.item = item;
	}
	@ManyToOne(optional = true)
	@JoinColumn(name = "owner_id",nullable=true)
	public Player getOwner() {
		return owner;
	}
	public void setOwner(Player owner) {
		this.owner = owner;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	@OneToMany(
			mappedBy = "game",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Gameitemtype> getType() {
		return type;
	}
	public void setType(Set<Gameitemtype> type) {
		this.type = type;
	}
	@OneToMany(
			mappedBy = "game",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Gameitemcategory> getCategory() {
		return category;
	}
	public void setCategory(Set<Gameitemcategory> category) {
		this.category = category;
	}
	
	
	
	
}
