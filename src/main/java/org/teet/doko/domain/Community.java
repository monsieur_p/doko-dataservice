package org.teet.doko.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Community {
	
	private Long id;
	private String title;
	private String description;
	private Player owner;
	private Set<Competition> competition;
	private Set<Communityplayer> member;
	private Set<Communitygamerule> rule;
	
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(nullable=false,length=50)
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Column(length=200)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@JsonIgnore
	@OneToMany(
			mappedBy = "community",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Competition> getCompetition() {
		return competition;
	}
	public void setCompetition(Set<Competition> competition) {
		this.competition = competition;
	}
	
	@JsonIgnore
	@OneToMany(
			mappedBy = "community",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Communityplayer> getMember() {
		return member;
	}
	public void setMember(Set<Communityplayer> member) {
		this.member = member;
	}
	
	@Override
	public boolean equals(Object ob) {
	   if (ob == null) {
	       return false;
	   }
	   if (this == ob) {
	       return true;
	   }
	   if (ob instanceof Competition) {
		   Competition other = (Competition) ob;
	       return this.id==other.getId();
	   }
	   return false;
	}
	@JsonIgnore
	@OneToMany(
			mappedBy = "community",
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        fetch = FetchType.LAZY
	    )
	public Set<Communitygamerule> getRule() {
		return rule;
	}
	public void setRule(Set<Communitygamerule> rule) {
		this.rule = rule;
	}
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "owner_id",nullable=true)
	public Player getOwner() {
		return owner;
	}
	public void setOwner(Player owner) {
		this.owner = owner;
	}
	
	
	
	
	
	
	
	

}
