package org.teet.doko.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

@Service
public class CommunityplayerCustomRepositoryImpl implements CommunityplayerCustomRepository{
	private static final String QUERY_COMMUNITY_BY_PLAYER = "SELECT c FROM Communityplayer cp LEFT JOIN cp.player p LEFT JOIN cp.community c WHERE p.id >= :playerId";
	private static final String QUERY_PLAYER_BY_COMMUNITY = "SELECT p FROM Communityplayer cp LEFT JOIN cp.player p LEFT JOIN cp.community c WHERE c.id >= :communityId";
	private static final String QUERY_PLAYER_BY_USERNAME = "SELECT p FROM Player p WHERE UPPER(p.username) LIKE UPPER(:searchText)";
	//private static final String QUERY_PLAYER_BY_EMAIL = "SELECT p FROM Player p WHERE UPPER(p.email) LIKE UPPER(:searchText)";
	
	@PersistenceContext
    private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Community> getCommunityByPlayer(Player player) {
		Query q=em.createQuery(QUERY_COMMUNITY_BY_PLAYER);
		q.setParameter("playerId", player.getId());
		
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Player> getPlayerByCommunity(Community community) {
		Query q=em.createQuery(QUERY_PLAYER_BY_COMMUNITY);
		q.setParameter("communityId", community.getId());
		
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Player> getPlayerBySearchword(String searchText) {
		if(searchText.length()>2) {
			Query q=em.createQuery(QUERY_PLAYER_BY_USERNAME);
			q.setParameter("searchText", searchText.toUpperCase()+"%");
		
			return q.getResultList();
		}else {
			return new ArrayList<Player>();
		}
	}

}
