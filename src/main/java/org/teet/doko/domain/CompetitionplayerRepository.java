package org.teet.doko.domain;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitionplayerRepository extends CrudRepository<Competitionplayer, Long> {
	public Competitionplayer findByCompetitionAndPlayer(Competition competition,Player player);
	public Set<Competitionplayer> findByCompetition(Competition competition);
}
