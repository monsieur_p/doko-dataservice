package org.teet.doko.domain;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

public interface CompetitionplayercycleRepository extends CrudRepository<Competitionplayercycle, Long>  {
	Collection<Competitionplayercycle> findByLocationcycle(Competitionlocationcycle competitionlocationcycle);
}
