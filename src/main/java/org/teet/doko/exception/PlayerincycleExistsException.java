package org.teet.doko.exception;

import org.teet.doko.gateway.v1.SimpleLocationCycleplayer;

public class PlayerincycleExistsException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SimpleLocationCycleplayer player;
	
	public PlayerincycleExistsException() {
		super();
	}
	public PlayerincycleExistsException(String msg) {
		super(msg);
	}
	public PlayerincycleExistsException(String msg,SimpleLocationCycleplayer player) {
		super(msg);
		this.player=player;
	}
	public SimpleLocationCycleplayer getPlayer() {
		return player;
	}
	public void setPlayer(SimpleLocationCycleplayer player) {
		this.player = player;
	}
	
}	
