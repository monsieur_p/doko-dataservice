package org.teet.doko.exception;

public class PlayerincycleDeleteException extends Exception{
	private int errorCode=0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public PlayerincycleDeleteException() {
		super();
	}
	public PlayerincycleDeleteException(String msg) {
		super(msg);
	
	}
	public PlayerincycleDeleteException(String msg, int errorCode) {
		super(msg);
		this.errorCode=errorCode;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}	
