package org.teet.doko.exception;

public class CommunityplayerAddException extends Exception{
	private int errorCode=0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public CommunityplayerAddException() {
		super();
	}
	public CommunityplayerAddException(String msg) {
		super(msg);
	
	}
	public CommunityplayerAddException(String msg, int errorCode) {
		super(msg);
		this.errorCode=errorCode;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}	
