package org.teet.doko.exception;

public class PlayerExistsInCommunityException extends Exception{
	private int errorCode=0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public PlayerExistsInCommunityException() {
		super();
	}
	public PlayerExistsInCommunityException(String msg) {
		super(msg);
	
	}
	public PlayerExistsInCommunityException(String msg, int errorCode) {
		super(msg);
		this.errorCode=errorCode;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}	
