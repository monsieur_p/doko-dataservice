package org.teet.doko.exception;

public class StatusException extends Exception{
	private int errorCode=0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public StatusException() {
		super();
	}
	public StatusException(String msg) {
		super(msg);
	
	}
	public StatusException(String msg, int errorCode) {
		super(msg);
		this.errorCode=errorCode;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}	
