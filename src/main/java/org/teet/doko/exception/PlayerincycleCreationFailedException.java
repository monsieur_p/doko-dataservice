package org.teet.doko.exception;

public class PlayerincycleCreationFailedException extends Exception{
	private int errorCode=0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public PlayerincycleCreationFailedException() {
		super();
	}
	public PlayerincycleCreationFailedException(String msg) {
		super(msg);
	
	}
	public PlayerincycleCreationFailedException(String msg, int errorCode) {
		super(msg);
		this.errorCode=errorCode;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}	
