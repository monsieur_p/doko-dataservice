package org.teet.doko.exception;

public class CompetitionplayerNotExistException extends Exception{
	private int errorCode=0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public CompetitionplayerNotExistException() {
		super();
	}
	public CompetitionplayerNotExistException(String msg) {
		super(msg);
	
	}
	public CompetitionplayerNotExistException(String msg, int errorCode) {
		super(msg);
		this.errorCode=errorCode;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}	
