package org.teet.doko;

import javax.servlet.http.HttpServletRequest;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
/**
*
* @author todhinio
*/
@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages="org.teet.doko.domain")
@ComponentScan("org.teet.doko")
public class DokoApp {
	/**
    *
    * @param args
    */
   public static void main(String[] args) {
       SpringApplication.run(DokoApp.class, args);
       
       
   }
   /*
   @Bean
   @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
   public AccessToken getAccessToken() {
       return ((KeycloakPrincipal) getRequest().getUserPrincipal()).getKeycloakSecurityContext().getToken();
   }

   @Bean
   @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
   public KeycloakSecurityContext getKeycloakSecurityContext() {
       return ((KeycloakPrincipal) getRequest().getUserPrincipal()).getKeycloakSecurityContext();
   }
   
   private HttpServletRequest getRequest() {
       return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
   }*/

}
